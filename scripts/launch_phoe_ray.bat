@echo off

set width=1280
set height=720
set spp=16
set executable=phoe_ray_cpu.exe
set scenePath=res/meshes/
set scene=material_test.obj

:menu
echo.
echo --------------------------
echo phoe_ray path tracer alpha
echo --------------------------

echo Choose resolution
echo 1 - 1280 x 720
echo 2 - 1024 x 576
echo q - Quit
echo.

choice /C 12q
echo.
if errorlevel 1 set width=1280 & set height=720
if errorlevel 2 set width=1024 & set height=576
if errorlevel 3 goto end

echo Choose device
echo 1 - CPU
echo 2 - GPU
echo q - Quit
echo.

choice /C 12q
echo.
if errorlevel 1 set executable=phoe_ray_cpu.exe
if errorlevel 2 set executable=phoe_ray_gpu.exe
if errorlevel 3 goto end

:choose_scene
echo Choose scene
echo 1 - Home
echo 2 - Monkey
echo 3 - City
echo 4 - Texture
echo 5 - Cornell
echo q - Quit
echo.

choice /C 12345q
echo.
if errorlevel 1 set scene=nice_home.obj
if errorlevel 2 set scene=material_test.obj
if errorlevel 3 set scene=city.obj
if errorlevel 4 set scene=texture_test.obj
if errorlevel 5 set scene=room.obj
if errorlevel 6 goto end

echo.
%executable% -w %width% -h %height% -s %spp% -f %scenePath%%scene%
goto choose_scene

:end
echo bye