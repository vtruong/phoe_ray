projectname = "phoe_ray"

workspace (projectname)
    configurations { "debug", "release" }
    location "build"

project (projectname)
    kind "ConsoleApp"
    language "C++"
    location ("build/" .. (projectname))
    targetdir "bin/%{cfg.buildcfg}"
    files { "src/**.cpp", "src/**.h",
            "extlibs/imgui-1.47/imgui.cpp",
            "extlibs/imgui-1.47/imgui_draw.cpp",
            "extlibs/imgui-1.47/*.h" }
    defines { "CPU_PATH_TRACE" }

    -- 3rd party libraries
    -- TODO(vinht): Other platforms
    includedirs { "extlibs/SDL2-2.0.4/include" }
    libdirs { "extlibs/SDL2-2.0.4/lib/x86" }
    links { "SDL2" }

    includedirs { "extlibs/glew-1.13.0/include" }
    libdirs { "extlibs/glew-1.13.0/lib/Release/Win32" }
    links { "glew32" }

    includedirs { "extlibs/glm-0.9.7.4" }
    includedirs { "extlibs/stb" }
    includedirs { "extlibs/imgui-1.47" }

    links { "opengl32" }

    configuration { "vs2015" }
        buildoptions { "/wd4996 /wd4577" }

    filter "configurations:debug"
        defines { "DEBUG" }
        flags { "Symbols" }

    filter "configurations:release"
        defines { "NDEBUG" }
        optimize "On"

    os.mkdir("bin/debug")
    os.mkdir("bin/release")

-- Project directory layout setup
os.mkdir("bin/debug")
os.mkdir("bin/release")
os.copyfile("extlibs/glew-1.13.0/bin/Release/Win32/glew32.dll", "bin/debug/glew32.dll")
os.copyfile("extlibs/glew-1.13.0/bin/Release/Win32/glew32.dll", "bin/release/glew32.dll")
os.copyfile("extlibs/SDL2-2.0.4/lib/x86/SDL2.dll", "bin/debug/SDL2.dll")
os.copyfile("extlibs/SDL2-2.0.4/lib/x86/SDL2.dll", "bin/release/SDL2.dll")
