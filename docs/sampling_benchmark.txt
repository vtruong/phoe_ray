Setup
-----

    if (depth < 4 && bvhIntersect(ray, scene, &hit))

    core.sampleCount = 64;

    core.screen.width = 320;
    core.screen.height = 320;

    core.camera.origin = v3(14.892697f, 5.396818f, -11.101116f);
    core.camera.theta = 0.549999f;
    core.camera.phi = 0.601389f;
    
    buildScene(&core.scene, "res/meshes/nice_home.obj", "res/meshes/nice_home.mtl");

Baseline
--------

# 1
Mean: 83.030101
Variance: 1629.517652

# 2
Mean: 82.987142
Variance: 1631.611241

# 3
Mean: 82.989554
Variance: 1623.595662

Always use same stratum
-----------------------

# 1
Mean: 88.696162
Variance: 1544.717842

# 2
Mean: 88.660107
Variance: 1543.466527

# 3
Mean: 88.635163
Variance: 1544.264420

Only use stratifying on initial hit
-----------------------------------

# 1
Mean: 81.855967
Variance: 1375.495006

# 2
Mean: 81.951143
Variance: 1378.997434

# 3
Mean: 81.987523
Variance: 1377.557901