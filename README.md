# phoe_ray #

phoe_ray is a simple path tracer with OpenGL visualization written in C++. 

Libraries used: [GLFW](http://www.glfw.org/), [GLEW](http://glew.sourceforge.net/), [GLM](http://glm.g-truc.net/0.9.6/index.html), OpenCL, [stb_image, stb_image_write](https://github.com/nothings/stb), [imgui](https://github.com/ocornut/imgui)

![072.jpg](https://bitbucket.org/repo/xjgRRE/images/398692055-072.jpg)
![044.jpg](https://bitbucket.org/repo/xjgRRE/images/792268524-044.jpg)
![038.png](https://bitbucket.org/repo/xjgRRE/images/1252775257-038.png)
![031.jpg](https://bitbucket.org/repo/xjgRRE/images/48576930-031.jpg)
![021.jpg](https://bitbucket.org/repo/xjgRRE/images/3432103191-021.jpg)

## OpenGL View ##

![024.jpg](https://bitbucket.org/repo/xjgRRE/images/2293379805-024.jpg)