/*
* The MIT License (MIT)
*
* Copyright (c) 2015 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

typedef struct
{
    // 16 x 2
    float3 origin, direction;
} ray_t;

float randf(uint* seed)
{
    *seed = (214013 * *seed + 2531011);
    return (float) ((*seed >> 16) & 0x7FFF) / 32768.0f;
}

kernel void launchKernel(global const int* screenWidth,
                         global const int* screenHeight,
                         global const float* aspectRatio,
                         global const float* tanFov,
                         global const float4* cameraOrigin,
                         global const float4* cameraToWorld,
                         global ray_t* rays,
                         global uint* seeds)
{
    int gid = get_global_id(0);

    uint seed = seeds[gid];

    float x = (float) (gid % (*screenWidth));
    float y = (float) (gid / (*screenWidth));

    float w = (float) (*screenWidth);
    float h = (float) (*screenHeight);

    float e1 = randf(&seed) - 0.5f;
    float e2 = randf(&seed) - 0.5f;

    float ndcx = (x + 0.5f + e1) / w;
    float ndcy = (y + 0.5f + e2) / h;

    float px = (2.0f * ndcx - 1.0f);
    float py = (1.0f - 2.0f * ndcy);

    px *= (*aspectRatio);

    px *= (*tanFov);
    py *= (*tanFov);

    float4 cameraPosition;

    float4 t = (float4) (px, py, -1.0f, 1.0f);

    global const float4* c = cameraToWorld;
    cameraPosition.x = dot((float4) (c[0].x, c[1].x, c[2].x, c[3].x), t);
    cameraPosition.y = dot((float4) (c[0].y, c[1].y, c[2].y, c[3].y), t);
    cameraPosition.z = dot((float4) (c[0].z, c[1].z, c[2].z, c[3].z), t);
    cameraPosition.w = dot((float4) (c[0].w, c[1].w, c[2].w, c[3].w), t);

    rays[gid].origin = (*cameraOrigin).xyz;
    rays[gid].direction = normalize(cameraPosition.xyz - rays[gid].origin);

    seeds[gid] = seed;
}