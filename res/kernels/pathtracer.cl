/*
* The MIT License (MIT)
*
* Copyright (c) 2015 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

// NOTE(vinht): Inbound structs. has to perfectly align with host.
typedef enum
{
    SURFACE_DIFFUSE = (1 << 0),
    SURFACE_GLASS = (1 << 1),
    SURFACE_GLOSSY = (1 << 2),
    SURFACE_EMISSION = (1 << 3)
} SurfaceType;

typedef enum
{
    TEXTURE_FLAT_COLOR,
    TEXTURE_CHECKERBOARD,
    TEXTURE_IMAGE
} TextureType;

typedef struct
{
    // 16 16 16
    float3 pa, pb, pc;

    // 16 16 16
    float3 na, nb, nc;

    // 8 8 8
    float2 ua, ub, uc;

    // 4 4
    uint materialId;
    uint pad;
} triangle_t;

typedef struct
{
    // 16
    float3 color;

    // 4 4 4
    uint textureId;
    uint surfaceType;
    uint textureType;

    // 4 4 4
    float roughness;
    float emission;
    float ior;

    uint pad[2];

} material_t;

typedef struct
{
    // 16 x 2
    float3 origin, direction;
} ray_t;

typedef struct
{
    // 16 x 2
    float3 corners[2];
} bbox_t;

typedef struct
{
    // 32
    bbox_t bounds;

    // 4 x 3
    uint offset;
    uint primitiveCount;
    uint axis;
} bvh_node_t;

typedef struct
{
    int width;
    int height;
    int offset;
} texture_t;

// NOTE(vinht): Local structs, no need to worry about alignment too much.
typedef struct
{
    float3 position;
    float3 normal;
    float2 uv;
    uint materialId;
} ray_hit_t;

constant const float tMinRay = 1e-4f;
constant const float tMaxRay = 1e+4f;
constant const float pi = M_PI_F;
constant const float twopi = 2.0f * M_PI_F;
constant const float epsilon = 0.00001f;

float3 uniformSampleHemisphere(float u1, float u2)
{
    float y = u1;
    float r = sqrt(max(0.0f, 1.0f - y * y));
    float phi = twopi * u2;
    float x = r * cos(phi);
    float z = r * sin(phi);
    return (float3)(x, y, z);
}

float2 concentricSampleDisk(float u1, float u2)
{
    float r, theta;

    float sx = 2.0f * u1 - 1.0f;
    float sy = 2.0f * u2 - 1.0f;

    if (sx == 0.0f && sy == 0.0f)
    {
        return (float2)(0.0f);
    }

    if (sx >= -sy)
    {
        if (sx > sy)
        {
            r = sx;
            theta = (sy > 0.0f) ? sy / r : 8.0f + sy / r;
        }
        else
        {
            r = sy;
            theta = 2.0f - sx / r;
        }
    }
    else
    {
        if (sx <= sy)
        {
            r = -sx;
            theta = 4.0f - sy / r;
        }
        else
        {
            r = -sy;
            theta = 6.0f + sx / r;
        }
    }

    theta *= pi / 4.0f;

    return r * (float2)(cos(theta), sin(theta));
}

float3 cosineSampleHemisphere(float u1, float u2)
{
    float2 point = concentricSampleDisk(u1, u2);

    float3 d;

    d.x = point.x;
    d.z = point.y;

    d.y = sqrt(max(0.0f, 1.0f - d.x * d.x - d.z * d.z));

    return d;
}

void coordinateSystem(float3 i, float3* j, float3* k)
{
    if (fabs(i.x) > fabs(i.y))
    {
        *j = (float3)(-i.z, 0.f, i.x);
        *j = normalize(*j);
    }
    else
    {
        *j = (float3)(0.f, i.z, -i.y);
        *j = normalize(*j);
    }
    *k = cross(i, *j);
}

float3 sampleDirection(float3 i, float e1, float e2)
{
    float3 sample = cosineSampleHemisphere(e1, e2);

    float3 j, k;

    coordinateSystem(i, &j, &k);

    float3 direction;

    direction.x = dot((float3) (j.x, i.x, k.x), sample);
    direction.y = dot((float3) (j.y, i.y, k.y), sample);
    direction.z = dot((float3) (j.z, i.z, k.z), sample);

    return direction;
}

bool intersectTriangle(const ray_t* ray,
                       global const triangle_t* triangle,
                       float* tHit,
                       float2* tUV,
                       float3* tN)
{
    float3 e1 = triangle->pb - triangle->pa;
    float3 e2 = triangle->pc - triangle->pa;
    float3 s1 = cross(ray->direction, e2);
    float divisor = dot(s1, e1);

    if (divisor == 0.0f)
    {
        return false;
    }

    float invDivisor = 1.0f / divisor;

    float3 s = ray->origin - triangle->pa;
    float b1 = dot(s, s1) * invDivisor;

    if (b1 < 0.0f || b1 > 1.0f)
    {
        return false;
    }

    float3 s2 = cross(s, e1);
    float b2 = dot(ray->direction, s2) * invDivisor;

    if (b2 < 0.0f || b1 + b2 > 1.0f)
    {
        return false;
    }

    float t = dot(e2, s2) * invDivisor;

    if (t < tMinRay || t > tMaxRay)
    {
        return false;
    }

    *tHit = t;

    float b0 = 1.0f - b1 - b2;

    float2 uv;

    uv.x = b0 * triangle->ua.x + b1 * triangle->ub.x + b2 * triangle->uc.x;
    uv.y = b0 * triangle->ua.y + b1 * triangle->ub.y + b2 * triangle->uc.y;

    *tUV = uv;

    float3 normal;

    normal.x = b0 * triangle->na.x + b1 * triangle->nb.x + b2 * triangle->nc.x;
    normal.y = b0 * triangle->na.y + b1 * triangle->nb.y + b2 * triangle->nc.y;
    normal.z = b0 * triangle->na.z + b1 * triangle->nb.z + b2 * triangle->nc.z;

    *tN = normal;

    return true;
}

bool intersectBBox(const ray_t* ray,
                   const bbox_t* bounds,
                   float3 invDir,
                   uint dirIsNeg[3])
{
    float tmin = (bounds->corners[dirIsNeg[0]].x - ray->origin.x) * invDir.x;
    float tmax = (bounds->corners[1 - dirIsNeg[0]].x - ray->origin.x) * invDir.x;
    float tymin = (bounds->corners[dirIsNeg[1]].y - ray->origin.y) * invDir.y;
    float tymax = (bounds->corners[1 - dirIsNeg[1]].y - ray->origin.y) * invDir.y;

    if ((tmin > tymax) || (tymin > tmax)) return false;
    if (tymin > tmin) tmin = tymin;
    if (tymax < tmax) tmax = tymax;

    float tzmin = (bounds->corners[dirIsNeg[2]].z - ray->origin.z) * invDir.z;
    float tzmax = (bounds->corners[1 - dirIsNeg[2]].z - ray->origin.z) * invDir.z;

    if ((tmin > tzmax) || (tzmin > tmax)) return false;
    if (tzmin > tmin) tmin = tzmin;
    if (tzmax < tmax) tmax = tzmax;

    return (tmin < tMaxRay) && (tmax > tMinRay);
}

bool intersect(const ray_t* ray,
               global const triangle_t* triangles,
               global const material_t* materials,
               global const bvh_node_t* bvhNodes,
               ray_hit_t* hitPoint)
{
    float3 invDir = 1.0f / ray->direction;
    uint dirIsNeg[3];
    dirIsNeg[0] = (invDir.x < 0.0f);
    dirIsNeg[1] = (invDir.y < 0.0f);
    dirIsNeg[2] = (invDir.z < 0.0f);

    uint nodeIndex = 0;
    uint todoOffset = 0;
    uint todo[64];

    float minDistance = HUGE_VALF;
    uint closestTriangle;
    float2 closestUV;
    float3 closestNormal;
    bool hit = false;

    while (true)
    {
        bbox_t bounds = bvhNodes[nodeIndex].bounds;
        uint offset = bvhNodes[nodeIndex].offset;
        uint primitiveCount = bvhNodes[nodeIndex].primitiveCount;
        uint axis = bvhNodes[nodeIndex].axis;

        if (intersectBBox(ray, &bounds, invDir, dirIsNeg))
        {
            if (bvhNodes[nodeIndex].primitiveCount > 0)
            {
                for (uint i = 0; i < primitiveCount; i++)
                {
                    uint triangleOffset = offset + i;

                    float tHit;
                    float2 tUV;
                    float3 tN;

                    if (intersectTriangle(ray, &triangles[triangleOffset], &tHit, &tUV, &tN))
                    {
                        hit = true;

                        if (tHit < minDistance)
                        {
                            minDistance = tHit;
                            closestTriangle = triangleOffset;
                            closestUV = tUV;
                            closestNormal = tN;
                        }
                    }
                }

                if (todoOffset == 0)
                {
                    break;
                }

                nodeIndex = todo[--todoOffset];
            }
            else
            {
                if (dirIsNeg[axis] == 1)
                {
                    todo[todoOffset++] = nodeIndex + 1;
                    nodeIndex = offset;
                }
                else
                {
                    todo[todoOffset++] = offset;
                    nodeIndex = nodeIndex + 1;
                }
            }
        }
        else
        {
            if (todoOffset == 0)
            {
                break;
            }

            nodeIndex = todo[--todoOffset];
        }
    }

    if (hit)
    {
        hitPoint->position = ray->origin + minDistance * ray->direction;
        hitPoint->normal = closestNormal;
        hitPoint->materialId = triangles[closestTriangle].materialId;
        hitPoint->uv = closestUV;
    }

    return hit;
}

float randf(uint* seed)
{
    *seed = (214013 * (*seed) + 2531011);
    return (float) (((*seed) >> 16) & 0x7FFF) / 32768.0f;
}

float3 reflect(float3 l, float3 n)
{
    return l - 2.0f * dot(l, n) * n;
}

float3 refract(float3 l, float3 n, float ior)
{
    float3 refract;

    float d = dot(n, l);
    float k = 1.0f - ior * ior * (1.0f - d * d);

    if (k > 0.0f)
    {
        refract = ior * l - (ior * d + sqrt(k)) * n;
    }

    return refract;
}

float3 readTextureColor(global const texture_t* textures,
                        global const uchar* textureBuffer,
                        uint textureId,
                        float2 uv)
{
    int x = (int) floor(uv.x * textures[textureId].width);
    int y = (int) floor(uv.y * textures[textureId].height);

    float3 color;

    int pixel = textures[textureId].offset + 4 * (y * textures[textureId].width + x);

    uchar r = textureBuffer[pixel + 0];
    uchar g = textureBuffer[pixel + 1];
    uchar b = textureBuffer[pixel + 2];

    color.x = (float) (r);
    color.y = (float) (g);
    color.z = (float) (b);

    color = color / 255.0f;

    return color;
}

kernel void launchKernel(global float* pixelBuffer,
                         global const triangle_t* triangles,
                         global const material_t* materials,
                         global const ray_t* rays,
                         global const bvh_node_t* bvhNodes,
                         global uint* seeds,
                         global const texture_t* textures,
                         global const uchar* textureBuffer,
                         global const texture_t* skyTexture,
                         global const uchar* skyTextureBuffer)
{
    int gid = get_global_id(0);

    uint seed = seeds[gid];

    ray_t ray = rays[gid];

    float3 finalColor = (float3)(1.0f);

    ray_hit_t hit;

    for (int depth = 0; depth < 6; depth++)
    {
        if (intersect(&ray, triangles, materials, bvhNodes, &hit))
        {
            global const material_t* hitMaterial = materials + hit.materialId;

            ray.origin = hit.position;

            /////////////////////////////////////////////////////
            // COLOR
            /////////////////////////////////////////////////////

            float3 materialColor = hitMaterial->color;

            if (hitMaterial->textureType == TEXTURE_IMAGE)
            {
                materialColor = readTextureColor(textures, textureBuffer, hitMaterial->textureId, hit.uv);
            }
            else if (hitMaterial->textureType == TEXTURE_CHECKERBOARD)
            {
                int px = (int) floor(hit.position.x * 0.25f);
                int py = (int) floor(hit.position.z * 0.25f);
                materialColor *= ((px + py) % 2 == 0) ? 0.9f : 0.7f;
            }

            finalColor *= materialColor;

            /////////////////////////////////////////////////////
            // SURFACE
            /////////////////////////////////////////////////////

            float e1 = randf(&seed);
            float e2 = randf(&seed);

            SurfaceType surfaceType = (SurfaceType) hitMaterial->surfaceType;

            if (surfaceType & SURFACE_DIFFUSE)
            {
                ray.direction = sampleDirection(hit.normal, e1, e2);
                finalColor *= fabs(dot(hit.normal, ray.direction));
            }
            else if (surfaceType & SURFACE_GLASS)
            {
                bool inside = false;
                if (dot(ray.direction, hit.normal) > 0)
                {
                    hit.normal = -hit.normal;
                    inside = true;
                }

                e1 = (e1 - 0.5f) * hitMaterial->roughness + 0.5f;
                e2 = (e2 - 0.5f) * hitMaterial->roughness + 0.5f;

                float facingRatio = -dot(ray.direction, hit.normal);
                float fresnelEffect = mix(pow(1.0f - facingRatio, 4.0f), 1.0f, 0.02f);
                float ior = (inside) ? hitMaterial->ior : 1.0f / hitMaterial->ior;

                if (randf(&seed) < fresnelEffect)
                {
                    ray.direction = sampleDirection(reflect(ray.direction, hit.normal), e1, e2);
                }
                else
                {
                    float3 tentativeDirection = refract(ray.direction, hit.normal, ior);

                    if (length(tentativeDirection) > 0.0f)
                    {
                        ray.direction = sampleDirection(tentativeDirection, e1, e2);
                    }
                }
            }
            else if (surfaceType & SURFACE_GLOSSY)
            {
                e1 = (e1 - 0.5f) * hitMaterial->roughness + 0.5f;
                e2 = (e2 - 0.5f) * hitMaterial->roughness + 0.5f;

                ray.direction = sampleDirection(reflect(ray.direction, hit.normal), e1, e2);
                finalColor *= fabs(dot(hit.normal, ray.direction));
            }
            else if (surfaceType & SURFACE_EMISSION)
            {
                finalColor *= hitMaterial->emission;
                break;
            }

            ray.origin += ray.direction * epsilon;
        }
        else
        {
            float phi = atan2(ray.direction.x, ray.direction.z);

            if (phi < 0.0f)
            {
                phi = fmod(phi + twopi, twopi);
            }

            phi /= twopi;

            float2 uv;
            uv.x = phi;
            // NOTE(vinht): Magic constant 0.001f was chosen to hide skydomes
            // starting height slightly below the horizon.
            uv.y = acos(ray.direction.y + 0.001f) / pi;
            uv = clamp(uv, (float2) (0.0f + epsilon), (float2) (1.0f - epsilon));

            float3 skyColor = readTextureColor(skyTexture, skyTextureBuffer, 0, uv);
            finalColor *= skyColor;

            // NOTE(vinht): Can't apply brightness factor if ray goes directly to sky
            // This will overbrighten the sky. We only want indirect effects.
            if (depth > 0)
            {
                finalColor *= 2.0f;
            }

            break;
        }
    }

    pixelBuffer[4 * gid + 0] = finalColor.x;
    pixelBuffer[4 * gid + 1] = finalColor.y;
    pixelBuffer[4 * gid + 2] = finalColor.z;

    seeds[gid] = seed;
}