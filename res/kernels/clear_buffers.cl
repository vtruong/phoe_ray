/*
* The MIT License (MIT)
*
* Copyright (c) 2015 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

kernel void launchKernel(global uchar* dstPixelBuffer,
                         global float* sumPixelBuffer,
                         global float* srcPixelBuffer)
{
    int gid = get_global_id(0);

    uint r = 4 * gid + 0;
    uint g = 4 * gid + 1;
    uint b = 4 * gid + 2;
    uint a = 4 * gid + 3;

    dstPixelBuffer[r] = 0;
    dstPixelBuffer[g] = 0;
    dstPixelBuffer[b] = 0;
    dstPixelBuffer[a] = 0;

    sumPixelBuffer[r] = 0.0f;
    sumPixelBuffer[g] = 0.0f;
    sumPixelBuffer[b] = 0.0f;
    sumPixelBuffer[a] = 0.0f;

    srcPixelBuffer[r] = 0.0f;
    srcPixelBuffer[g] = 0.0f;
    srcPixelBuffer[b] = 0.0f;
    srcPixelBuffer[a] = 0.0f;
}