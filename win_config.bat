mkdir bin
cd bin
mkdir debug
mkdir release

cd Debug
mklink /J "res" "../../res"

cd ..

cd Release
mklink /J "res" "../../res"

cd ..
cd ..

copy /Y "scripts\launch_phoe_ray.bat" "bin\debug"
copy /Y "scripts\launch_phoe_ray.bat" "bin\release"
