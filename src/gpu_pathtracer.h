/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"
#include "compute.h"
#include "scene.h"
#include "gpu_texture.h"
#include "core.h"

struct GPUPathTracer
{
    Core* core;

    u32* seeds;

    float gammaValue;
    float invSamplesRan;

    int samplesRan;

    ComputeProgram pathTracerProgram;
    ComputeProgram compositorProgram;
    ComputeProgram rayGeneratorProgram;
    ComputeProgram clearBuffersProgram;

    std::mutex* mutex;
    std::thread* thread;
    u32 recentlyFinishedJobs;

    struct MemObjects
    {
        cl_mem triangles;
        cl_mem materials;
        cl_mem bvhNodes;
        cl_mem rays;
        cl_mem seeds;
        cl_mem dstPixelBuffer;
        cl_mem sumPixelBuffer;
        cl_mem srcPixelBuffer;
        cl_mem gamma;
        cl_mem invSamples;
        cl_mem screenWidth;
        cl_mem screenHeight;
        cl_mem aspectRatio;
        cl_mem tanFov;
        cl_mem cameraOrigin;
        cl_mem cameraToWorld;

        cl_mem textures;
        cl_mem textureBuffer;

        cl_mem skyTexture;
        cl_mem skyTextureBuffer;
    } memObjects;
};

static bool createGPUPathTracerKernels(GPUPathTracer* pathTracer, ComputeContext* computeContext)
{
    if (!createComputeProgram(computeContext,
                              "res/kernels/pathtracer.cl",
                              &pathTracer->pathTracerProgram))
    {
        printf("Failed to create compute program.\n");
        return false;
    }

    if (!createComputeProgram(computeContext,
                              "res/kernels/compositor.cl",
                              &pathTracer->compositorProgram))
    {
        printf("Failed to create compute program.\n");
        return false;
    }

    if (!createComputeProgram(computeContext,
                              "res/kernels/ray_generator.cl",
                              &pathTracer->rayGeneratorProgram))
    {
        printf("Failed to create compute program.\n");
        return false;
    }

    if (!createComputeProgram(computeContext,
                              "res/kernels/clear_buffers.cl",
                              &pathTracer->clearBuffersProgram))
    {
        printf("Failed to create compute program.\n");
        return false;
    }

    size_t kernelWorkGroupSize;

    clGetKernelWorkGroupInfo(pathTracer->pathTracerProgram.kernel,
                             computeContext->device,
                             CL_KERNEL_WORK_GROUP_SIZE,
                             sizeof(kernelWorkGroupSize),
                             &kernelWorkGroupSize,
                             NULL);

    printf("CL_KERNEL_WORK_GROUP_SIZE: %u\n", kernelWorkGroupSize);

    size_t kernelPreferredWorkGroupSizeMultiple;

    clGetKernelWorkGroupInfo(pathTracer->pathTracerProgram.kernel,
                             computeContext->device,
                             CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,
                             sizeof(kernelPreferredWorkGroupSizeMultiple),
                             &kernelPreferredWorkGroupSizeMultiple,
                             NULL);

    printf("CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE: %u\n", kernelPreferredWorkGroupSizeMultiple);

    return true;
}

static void initGPUPathTracer(GPUPathTracer* pathTracer, ComputeContext* computeContext, Core* core)
{
    Screen* screen = &core->screen;
    Scene* scene = &core->scene;
    Camera* camera = &core->camera;

    pathTracer->core = core;

    pathTracer->mutex = new std::mutex;

    u32 pixelCount = screen->width * screen->height;

    u32 rayBufferSize = pixelCount * sizeof(GPURay);
    u32 seedBufferSize = pixelCount * sizeof(u32);
    u32 pixelBufferSize = pixelCount * 4;
    u32 backBufferSize = pixelCount * 4 * sizeof(float);

    pathTracer->invSamplesRan = 1.0f;

    pathTracer->seeds = (u32*) malloc(seedBufferSize);

    for (u32 randomId = 0; randomId < pixelCount; randomId++)
    {
        pathTracer->seeds[randomId] = (u32) (randf() * 2531011.0f);
    }

    GPUPathTracer::MemObjects* memObjects = &pathTracer->memObjects;

    memObjects->triangles =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->alignedTrianglesSize, scene->alignedTriangles, NULL);
    memObjects->materials =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->alignedMaterialsSize, scene->alignedMaterials, NULL);
    memObjects->bvhNodes =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->alignedBvhNodesSize, scene->alignedBvhNodes, NULL);
    memObjects->rays =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_WRITE,
                       rayBufferSize, NULL, NULL);
    memObjects->seeds =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                       seedBufferSize, pathTracer->seeds, NULL);
    memObjects->dstPixelBuffer =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_WRITE,
                       pixelBufferSize, NULL, NULL);
    memObjects->sumPixelBuffer =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_WRITE,
                       backBufferSize, NULL, NULL);
    memObjects->srcPixelBuffer =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_WRITE,
                       backBufferSize, NULL, NULL);
    memObjects->gamma =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY,
                       sizeof(float), NULL, NULL);
    memObjects->invSamples =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY,
                       sizeof(float), NULL, NULL);
    memObjects->screenWidth =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(int), &screen->width, NULL);
    memObjects->screenHeight =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(int), &screen->height, NULL);
    memObjects->aspectRatio =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(float), &screen->aspectRatio, NULL);
    memObjects->tanFov =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(float), &camera->tanFov, NULL);
    memObjects->cameraOrigin =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(vec4), &camera->origin, NULL);
    memObjects->cameraToWorld =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(mat4), &camera->cameraToWorld, NULL);
    memObjects->textures =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->textureCount * sizeof(Texture), scene->textures, NULL);
    memObjects->textureBuffer =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->textureBufferSize, scene->textureBuffer, NULL);
    memObjects->skyTexture =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(Texture), &scene->skyTexture, NULL);
    memObjects->skyTextureBuffer =
        clCreateBuffer(computeContext->context,
                       CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       scene->skyTextureBufferSize, scene->skyTextureBuffer, NULL);

    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 0, sizeof(cl_mem), &memObjects->srcPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 1, sizeof(cl_mem), &memObjects->triangles));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 2, sizeof(cl_mem), &memObjects->materials));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 3, sizeof(cl_mem), &memObjects->rays));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 4, sizeof(cl_mem), &memObjects->bvhNodes));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 5, sizeof(cl_mem), &memObjects->seeds));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 6, sizeof(cl_mem), &memObjects->textures));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 7, sizeof(cl_mem), &memObjects->textureBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 8, sizeof(cl_mem), &memObjects->skyTexture));
    CL_CHECK(clSetKernelArg(pathTracer->pathTracerProgram.kernel, 9, sizeof(cl_mem), &memObjects->skyTextureBuffer));

    CL_CHECK(clSetKernelArg(pathTracer->compositorProgram.kernel, 0, sizeof(cl_mem), &memObjects->dstPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->compositorProgram.kernel, 1, sizeof(cl_mem), &memObjects->sumPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->compositorProgram.kernel, 2, sizeof(cl_mem), &memObjects->srcPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->compositorProgram.kernel, 3, sizeof(cl_mem), &memObjects->gamma));
    CL_CHECK(clSetKernelArg(pathTracer->compositorProgram.kernel, 4, sizeof(cl_mem), &memObjects->invSamples));

    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 0, sizeof(cl_mem), &memObjects->screenWidth));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 1, sizeof(cl_mem), &memObjects->screenHeight));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 2, sizeof(cl_mem), &memObjects->aspectRatio));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 3, sizeof(cl_mem), &memObjects->tanFov));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 4, sizeof(cl_mem), &memObjects->cameraOrigin));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 5, sizeof(cl_mem), &memObjects->cameraToWorld));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 6, sizeof(cl_mem), &memObjects->rays));
    CL_CHECK(clSetKernelArg(pathTracer->rayGeneratorProgram.kernel, 7, sizeof(cl_mem), &memObjects->seeds));

    CL_CHECK(clSetKernelArg(pathTracer->clearBuffersProgram.kernel, 0, sizeof(cl_mem), &memObjects->dstPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->clearBuffersProgram.kernel, 1, sizeof(cl_mem), &memObjects->sumPixelBuffer));
    CL_CHECK(clSetKernelArg(pathTracer->clearBuffersProgram.kernel, 2, sizeof(cl_mem), &memObjects->srcPixelBuffer));
}

static void gpuWorkerThread(GPUPathTracer* pathTracer, cl_command_queue commandQueue)
{
    Core* core = pathTracer->core;
    Screen* screen = &core->screen;

    u32 rayCount = screen->width * screen->height;

    const u32 workBatchSize = 8192;
    const u32 localWorkSize = 128;

    int samplesPerPixel = core->samplesPerPixel;

    while (pathTracer->samplesRan < samplesPerPixel)
    {
        u32 workSize = workBatchSize;
        u32 offset = 0;
        u32 workBatchCount = (rayCount / workBatchSize) + 1;

        for (u32 workBatchIndex = 0; workBatchIndex < workBatchCount; workBatchIndex++)
        {
            CL_CHECK
            (
                clEnqueueNDRangeKernel(commandQueue,
                                       pathTracer->rayGeneratorProgram.kernel,
                                       1, &offset, &workSize, &localWorkSize,
                                       0, NULL, NULL)
            );

            CL_CHECK
            (
                clEnqueueNDRangeKernel(commandQueue,
                                       pathTracer->pathTracerProgram.kernel,
                                       1, &offset, &workSize, &localWorkSize,
                                       0, NULL, NULL)
            );

            CL_CHECK
            (
                clEnqueueNDRangeKernel(commandQueue,
                                       pathTracer->compositorProgram.kernel,
                                       1, &offset, &workSize, &localWorkSize,
                                       0, NULL, NULL)
            );

            clFinish(commandQueue);

            offset += workBatchSize;

            if (offset + workBatchSize > rayCount)
            {
                u32 delta = (offset + workBatchSize) - rayCount;
                workSize -= delta;
            }

            if (core->stopRunning ||
                core->renderState == RENDER_STATE_HALTING ||
                core->renderState == RENDER_STATE_RESTARTING)
            {
                goto exit;
            }
        }

        u32 pixelBufferSize = core->pixelBufferSize;
        u8* pixelBuffer = core->pixelBuffer;

        CL_CHECK
        (
            clEnqueueReadBuffer(commandQueue,
                                pathTracer->memObjects.dstPixelBuffer, CL_TRUE, 0,
                                pixelBufferSize, pixelBuffer,
                                0, NULL, NULL)
        );

        pathTracer->samplesRan++;
        pathTracer->invSamplesRan = 1.0f / (float) pathTracer->samplesRan;

        CL_CHECK
        (
            clEnqueueWriteBuffer(commandQueue,
                                 pathTracer->memObjects.invSamples, CL_FALSE, 0,
                                 sizeof(float), &pathTracer->invSamplesRan,
                                 0, NULL, NULL)
        );

        clFinish(commandQueue);

        pathTracer->mutex->lock();
        pathTracer->recentlyFinishedJobs++;
        pathTracer->mutex->unlock();
    }

exit:
    return;
}

static void prepareGPUPathTracer(GPUPathTracer* pathTracer, ComputeContext* computeContext)
{
    Screen* screen = &pathTracer->core->screen;
    Camera* camera = &pathTracer->core->camera;
    bool gammaCorrection = pathTracer->core->gammaCorrection;
    Scene* scene = &pathTracer->core->scene;

    u32 rayCount = screen->width * screen->height;

    size_t globalWorkSize[1] = { rayCount };
    size_t localWorkSize[1] = { 128 };

    CL_CHECK
    (
        clEnqueueNDRangeKernel(computeContext->commandQueue,
                               pathTracer->clearBuffersProgram.kernel,
                               1, NULL, globalWorkSize, localWorkSize,
                               0, NULL, NULL)
    );

    float gamma = (gammaCorrection) ? 1.0f / 2.2f : 1.0f;

    CL_CHECK
    (
        clEnqueueWriteBuffer(computeContext->commandQueue,
                             pathTracer->memObjects.gamma, CL_FALSE, 0,
                             sizeof(float), &gamma,
                             0, NULL, NULL)
    );

    pathTracer->samplesRan = 0;
    pathTracer->invSamplesRan = 1.0f;

    CL_CHECK
    (
        clEnqueueWriteBuffer(computeContext->commandQueue,
                             pathTracer->memObjects.invSamples, CL_FALSE, 0,
                             sizeof(float), &pathTracer->invSamplesRan,
                             0, NULL, NULL)
    );

    CL_CHECK
    (
        clEnqueueWriteBuffer(computeContext->commandQueue,
                             pathTracer->memObjects.cameraOrigin, CL_FALSE, 0,
                             sizeof(vec4), &camera->origin,
                             0, NULL, NULL)
    );

    CL_CHECK
    (
        clEnqueueWriteBuffer(computeContext->commandQueue,
                             pathTracer->memObjects.cameraToWorld, CL_FALSE, 0,
                             sizeof(mat4), &camera->cameraToWorld,
                             0, NULL, NULL)
    );

    CL_CHECK
    (
        clEnqueueWriteBuffer(computeContext->commandQueue,
                             pathTracer->memObjects.materials, CL_FALSE, 0,
                             scene->alignedMaterialsSize, scene->alignedMaterials,
                             0, NULL, NULL)
    );

    pathTracer->recentlyFinishedJobs = 0;
    pathTracer->thread = new std::thread(gpuWorkerThread, pathTracer, computeContext->commandQueue);
}

static void freeGPUPathTracer(GPUPathTracer* pathTracer)
{
    if (pathTracer->thread != NULL)
    {
        if (pathTracer->thread->joinable())
        {
            pathTracer->thread->join();
        }
    }

    delete pathTracer->thread;
    delete pathTracer->mutex;

    freeComputeProgram(&pathTracer->pathTracerProgram);
    freeComputeProgram(&pathTracer->compositorProgram);
    freeComputeProgram(&pathTracer->rayGeneratorProgram);
    freeComputeProgram(&pathTracer->clearBuffersProgram);

    clReleaseMemObject(pathTracer->memObjects.triangles);
    clReleaseMemObject(pathTracer->memObjects.materials);
    clReleaseMemObject(pathTracer->memObjects.bvhNodes);
    clReleaseMemObject(pathTracer->memObjects.rays);
    clReleaseMemObject(pathTracer->memObjects.seeds);
    clReleaseMemObject(pathTracer->memObjects.dstPixelBuffer);
    clReleaseMemObject(pathTracer->memObjects.sumPixelBuffer);
    clReleaseMemObject(pathTracer->memObjects.srcPixelBuffer);
    clReleaseMemObject(pathTracer->memObjects.gamma);
    clReleaseMemObject(pathTracer->memObjects.invSamples);
    clReleaseMemObject(pathTracer->memObjects.screenWidth);
    clReleaseMemObject(pathTracer->memObjects.screenHeight);
    clReleaseMemObject(pathTracer->memObjects.aspectRatio);
    clReleaseMemObject(pathTracer->memObjects.tanFov);
    clReleaseMemObject(pathTracer->memObjects.cameraOrigin);
    clReleaseMemObject(pathTracer->memObjects.cameraToWorld);
    clReleaseMemObject(pathTracer->memObjects.textures);
    clReleaseMemObject(pathTracer->memObjects.textureBuffer);
    clReleaseMemObject(pathTracer->memObjects.skyTexture);
    clReleaseMemObject(pathTracer->memObjects.skyTextureBuffer);

    free(pathTracer->seeds);
}