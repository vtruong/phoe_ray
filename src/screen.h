/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"

struct Screen
{
    int width;
    int height;
    float aspectRatio;
};

static void captureScreen(Screen* screen, u8* pixelBuffer, u32 pixelBufferSize)
{
    u8* tempBuffer = (u8*) malloc(pixelBufferSize * sizeof(u8));

    glReadPixels(0, 0, screen->width, screen->height, GL_RGBA, GL_UNSIGNED_BYTE, tempBuffer);

    for (int screenY = 0; screenY < screen->height; screenY++)
    {
        for (int screenX = 0; screenX < screen->width; screenX++)
        {
            u32 srcPixelIndex = 4 * (screenY * screen->width + screenX);

            int dstScreenY = screen->height - screenY - 1;
            u32 dstPixelIndex = 4 * (dstScreenY * screen->width + screenX);

            assert(srcPixelIndex < pixelBufferSize);
            assert(dstPixelIndex < pixelBufferSize);

            pixelBuffer[dstPixelIndex + 0] = tempBuffer[srcPixelIndex + 0];
            pixelBuffer[dstPixelIndex + 1] = tempBuffer[srcPixelIndex + 1];
            pixelBuffer[dstPixelIndex + 2] = tempBuffer[srcPixelIndex + 2];
            pixelBuffer[dstPixelIndex + 3] = tempBuffer[srcPixelIndex + 3];
        }
    }

    free(tempBuffer);
}

static void saveScreenToDisk(Screen* screen, u8* pixelBuffer)
{
    char chosenFileName[64] = {};

    int fileNumber = 0;

    while (true)
    {
        char fileName[64] = {};

        sprintf(fileName, "render%d.png", fileNumber);

        FILE* f = fopen(fileName, "rb");

        if (f == NULL)
        {
            memcpy(chosenFileName, fileName, sizeof(chosenFileName));
            break;
        }
        else
        {
            fclose(f);
            fileNumber++;
        }
    }

    printf("Saving into %s..\n", chosenFileName);

    int strideInBytes = 4 * screen->width;

    int errorCode = stbi_write_png(chosenFileName, screen->width, screen->height, 4, pixelBuffer, strideInBytes);

    if (errorCode != 0)
    {
        printf("Rendered image is saved to disk.\n");
    }
    else
    {
        printf("Failed to save rendered image.\n");
    }
}