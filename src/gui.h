/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"
#include "utility.h"
#include "core.h"

struct GUI
{
    GUIShader guiShader;

    u32 vao, vbo, ibo;
    u32 fontTexture;

    bool mousePressed[2];
    float mouseWheel;
    bool anyWindowHovered;

    int materialId;
    int surfaceId;
    int textureTypeId;
    int textureId;
    bool initializeMenu;
};

static void imGuiRenderDrawLists(ImDrawData* draw_data)
{
    ImGuiIO& io = ImGui::GetIO();
    GUI* gui = (GUI*)io.UserData;
    GUIShader* guiShader = &gui->guiShader;

    // Backup GL state
    GLint last_program; glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
    GLint last_texture; glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    GLint last_array_buffer; glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    GLint last_element_array_buffer; glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
    GLint last_vertex_array; glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
    GLint last_blend_src; glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
    GLint last_blend_dst; glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
    GLint last_blend_equation_rgb; glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
    GLint last_blend_equation_alpha; glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
    GLint last_viewport[4]; glGetIntegerv(GL_VIEWPORT, last_viewport);
    GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);

    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glActiveTexture(GL_TEXTURE0);

    // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
    int fb_width = (int)(io.DisplaySize.x * io.DisplayFramebufferScale.x);
    int fb_height = (int)(io.DisplaySize.y * io.DisplayFramebufferScale.y);
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    // Setup orthographic projection matrix
    glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const float ortho_projection[4][4] =
    {
        { 2.0f / io.DisplaySize.x, 0.0f,                     0.0f, 0.0f },
        { 0.0f ,                   2.0f / -io.DisplaySize.y, 0.0f, 0.0f },
        { 0.0f ,                   0.0f,                    -1.0f, 0.0f },
        { -1.0f,                   1.0f,                     0.0f, 1.0f },
    };

    glUseProgram(guiShader->program);
    glUniform1i(guiShader->texture, 0);
    glUniformMatrix4fv(guiShader->projection, 1, GL_FALSE, &ortho_projection[0][0]);
    glBindVertexArray(gui->vao);

    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;

        glBindBuffer(GL_ARRAY_BUFFER, gui->vbo);
        glBufferData(GL_ARRAY_BUFFER,
                     (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert),
                     (GLvoid*)&cmd_list->VtxBuffer.front(),
                     GL_STREAM_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui->ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx),
                     (GLvoid*)&cmd_list->IdxBuffer.front(),
                     GL_STREAM_DRAW);

        for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin();
             pcmd != cmd_list->CmdBuffer.end();
             pcmd++)
        {
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                glScissor((int)pcmd->ClipRect.x,
                          (int)(fb_height - pcmd->ClipRect.w),
                          (int)(pcmd->ClipRect.z - pcmd->ClipRect.x),
                          (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES,
                               (GLsizei)pcmd->ElemCount,
                               sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT,
                               idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }

    // Restore modified GL state
    glUseProgram(last_program);
    glBindTexture(GL_TEXTURE_2D, last_texture);
    glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
    glBindVertexArray(last_vertex_array);
    glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    glBlendFunc(last_blend_src, last_blend_dst);
    if (last_enable_blend) glEnable(GL_BLEND);
    else glDisable(GL_BLEND);
    if (last_enable_cull_face) glEnable(GL_CULL_FACE);
    else glDisable(GL_CULL_FACE);
    if (last_enable_depth_test) glEnable(GL_DEPTH_TEST);
    else glDisable(GL_DEPTH_TEST);
    if (last_enable_scissor_test) glEnable(GL_SCISSOR_TEST);
    else glDisable(GL_SCISSOR_TEST);
    glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
}

static void prePollEventGUI(GUI* gui)
{
    ImGuiIO& io = ImGui::GetIO();
    io.MouseWheel = 0;

    gui->mousePressed[0] = false;
    gui->mousePressed[1] = false;
}

static const char* surfaceTypeComboBoxLabels = "diffuse\0glass\0glossy\0emission\0";
static const char* textureTypeComboBoxLabels = "color\0checkerboard\0";
static const char* textureTypeComboBoxAllLabels = "color\0checkerboard\0image\0";

static void selectMaterialGUI(GUI* gui, Material* material, u32 materialId)
{
    gui->materialId = materialId;
    gui->surfaceId = -1;

    // NOTE(vinht): Find out which id surface type corresponds to for the combo menu
    int surfaceType = (int) material->surfaceType;
    while (surfaceType != 0)
    {
        surfaceType = surfaceType >> 1;
        gui->surfaceId++;
    }

    gui->textureTypeId = material->textureType;
    gui->textureId = material->textureId;
}

static bool updateMaterialEditGUI(GUI* gui, Scene* scene)
{
    bool userInteracted = false;

    Material* material = &scene->materials[gui->materialId];

    if (gui->initializeMenu ||
        ImGui::Combo("materials", &gui->materialId, scene->materialNamesStringified, 12))
    {
        material = &scene->materials[gui->materialId];

        selectMaterialGUI(gui, material, gui->materialId);

        gui->initializeMenu = false;
    }

    if (ImGui::ColorEdit3("color", &material->color[0]))
    {
        userInteracted = true;
    }


    if (ImGui::Combo("surface", &gui->surfaceId, surfaceTypeComboBoxLabels, 4))
    {
        material->surfaceType = (SurfaceType) (1 << gui->surfaceId);

        userInteracted = true;
    }

    if (scene->textureCount > 0)
    {
        if (ImGui::Combo("texture type", &gui->textureTypeId, textureTypeComboBoxAllLabels, 3))
        {
            material->textureType = (TextureType) gui->textureTypeId;

            userInteracted = true;
        }

        if (material->textureType == TEXTURE_IMAGE)
        {
            if (ImGui::Combo("image", &gui->textureId, scene->textureNamesStringified, 12))
            {
                material->textureId = gui->textureId;

                userInteracted = true;
            }
        }
    }
    else
    {
        if (ImGui::Combo("texture", &gui->textureTypeId, textureTypeComboBoxLabels, 2))
        {
            material->textureType = (TextureType) gui->textureTypeId;

            userInteracted = true;
        }
    }

    if (material->surfaceType == SURFACE_GLASS || material->surfaceType == SURFACE_GLOSSY)
    {
        if (ImGui::SliderFloat("roughness", &material->roughness, 0.0f, 1.0f))
        {
            userInteracted = true;
        }
    }

    if (material->surfaceType == SURFACE_EMISSION)
    {
        if (ImGui::SliderFloat("emission", &material->emission, 0.0f, 20.0f))
        {
            userInteracted = true;
        }
    }

    if (material->surfaceType == SURFACE_GLASS)
    {
        if (ImGui::SliderFloat("ior", &material->ior, 0.0f, 5.0f))
        {
            userInteracted = true;
        }
    }

    return userInteracted;
}

static void updateRenderInfoGUI(RenderInfo* renderInfo)
{
    ImGui::Text("Sample: %u/%u (%.2f%%)",
                renderInfo->samplesFinished,
                renderInfo->samplesPerPixel,
                renderInfo->completionPercent);
    ImGui::Text("Elapsed: %.2f sec", renderInfo->elapsedTime);
    ImGui::Text("Estimate: %.2f sec", renderInfo->estimatedTime);
}

static bool updateLiveRenderingToggleGUI(RenderMode* renderMode)
{
    bool userInteracted = false;

    bool isLiveRendering = (*renderMode == RENDER_MODE_LIVE);

    if (ImGui::Checkbox("live rendering", &isLiveRendering))
    {
        userInteracted = true;

        *renderMode = (isLiveRendering) ? RENDER_MODE_LIVE : RENDER_MODE_NORMAL;
    }

    return userInteracted;
}

static void createGUI(GUI* gui, Screen* screen)
{
    ImGuiIO& io = ImGui::GetIO();
    io.UserData = (void*) gui;

    io.DisplaySize.x = (float) screen->width;
    io.DisplaySize.y = (float) screen->height;
    io.DeltaTime = 1.0f / 60.0f;
    io.RenderDrawListsFn = imGuiRenderDrawLists;

    gui->guiShader = createGUIShader();
    gui->initializeMenu = true;

    /////////////////////////////////////////////////////
    // BUFFERS
    /////////////////////////////////////////////////////

    glGenBuffers(1, &gui->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, gui->vbo);

    glGenBuffers(1, &gui->ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui->ibo);

    glGenVertexArrays(1, &gui->vao);
    glBindVertexArray(gui->vao);
    glBindBuffer(GL_ARRAY_BUFFER, gui->vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui->ibo);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*) 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*) sizeof(ImVec2));
    glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*) (2 * sizeof(ImVec2)));

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    /////////////////////////////////////////////////////
    // FONT TEXTURE
    /////////////////////////////////////////////////////

    unsigned char* pixels;
    int width, height;
    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    glGenTextures(1, &gui->fontTexture);
    glBindTexture(GL_TEXTURE_2D, gui->fontTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

    // Store our identifier
    io.Fonts->TexID = (void*)(intptr_t)gui->fontTexture;

    /////////////////////////////////////////////////////
    // INPUTS
    /////////////////////////////////////////////////////

    io.KeyMap[ImGuiKey_Tab] = SDLK_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
    io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = SDL_SCANCODE_PAGEUP;
    io.KeyMap[ImGuiKey_PageDown] = SDL_SCANCODE_PAGEDOWN;
    io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
    io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
    io.KeyMap[ImGuiKey_Delete] = SDLK_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = SDLK_BACKSPACE;
    io.KeyMap[ImGuiKey_Enter] = SDLK_RETURN;
    io.KeyMap[ImGuiKey_Escape] = SDLK_ESCAPE;
    io.KeyMap[ImGuiKey_A] = SDLK_a;
    io.KeyMap[ImGuiKey_C] = SDLK_c;
    io.KeyMap[ImGuiKey_V] = SDLK_v;
    io.KeyMap[ImGuiKey_X] = SDLK_x;
    io.KeyMap[ImGuiKey_Y] = SDLK_y;
    io.KeyMap[ImGuiKey_Z] = SDLK_z;
}

static void freeGUI(GUI* gui)
{
    glDeleteTextures(1, &gui->fontTexture);
    glDeleteBuffers(1, &gui->vbo);
    glDeleteVertexArrays(1, &gui->vao);
    freeShaderProgram(gui->guiShader.program);

    ImGui::Shutdown();
}
