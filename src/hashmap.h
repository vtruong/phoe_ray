/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"

static const u32 maxHashMapSize = 64;

// NOTE(vinht): Very basic fixed size hash map for material array indices.
// Uses linear probing for resolving collisions. Lookups are very rare and only
// during scene initialization, so performance is not an issue.
struct HashMap
{
    u8 keys[maxHashMapSize][64];
    u32 data[maxHashMapSize];
    u8 occupancy[maxHashMapSize];
    u32 size;
};

static u32 hash(const char* str)
{
    // NOTE(vinht): djb2 from http://www.cse.yorku.ca/~oz/hash.html
    unsigned long hash = 5381;
    unsigned char c = *str;

    do
    {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
        c = *str++;
    }
    while (c != 0);

    return hash;
}

static void hashMapInit(HashMap* hashMap)
{
    memset(hashMap, 0, sizeof(HashMap));
}

static void hashMapInsert(HashMap* hashMap, const char* key, u32 data)
{
    assert(hashMap->size < maxHashMapSize);

    u32 hashKeyIndex = hash(key) % maxHashMapSize;

    while (true)
    {
        if (hashMap->occupancy[hashKeyIndex] == 0)
        {
            memcpy(hashMap->keys[hashKeyIndex], key, strlen(key));
            hashMap->data[hashKeyIndex] = data;
            hashMap->occupancy[hashKeyIndex] = 1;
            hashMap->size++;
            break;
        }
        else
        {
            if (strcmp((char*) hashMap->keys[hashKeyIndex], key) == 0)
            {
                break;
            }

            hashKeyIndex++;
            hashKeyIndex = hashKeyIndex % maxHashMapSize;
        }
    }
}

static bool hashMapLookup(HashMap* hashMap, const char* key, u32* data)
{
    u32 hashKeyIndex = hash(key) % maxHashMapSize;

    u32 attempts = 0;

    while (true)
    {
        if (attempts == maxHashMapSize)
        {
            return false;
        }

        if (hashMap->occupancy[hashKeyIndex] == 1)
        {
            if (strcmp((char*) hashMap->keys[hashKeyIndex], key) == 0)
            {
                *data = hashMap->data[hashKeyIndex];
                break;
            }
            else
            {
                hashKeyIndex++;
                hashKeyIndex = hashKeyIndex % maxHashMapSize;
                attempts++;
            }
        }
        else
        {
            hashKeyIndex++;
            hashKeyIndex = hashKeyIndex % maxHashMapSize;
            attempts++;
        }
    }

    return true;
}
