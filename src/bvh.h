/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "scene.h"

// NOTE(vinht): Most of the BVH implementation is from From the book:
// Physically Based Rendering, 2nd edition. I did some modifications in terms
// of not using classes or STL.

struct BVHPrimitiveInfo
{
    int primitiveId;
    vec3 centroid;
    BBox bounds;
};

static void initBHVPrimitiveInfo(BVHPrimitiveInfo* info, int primitiveId, BBox bounds)
{
    info->primitiveId = primitiveId;
    info->bounds = bounds;
    info->centroid = 0.5f * bounds.min + 0.5f * bounds.max;
}

struct BVHBuildNode
{
    BBox bounds;
    BVHBuildNode* children[2];
    u32 splitAxis;
    u32 firstPrimitiveOffset;
    u32 primitiveCount;
};

static void initLeafNode(BVHBuildNode* node, u32 first, u32 n, BBox b)
{
    node->firstPrimitiveOffset = first;
    node->primitiveCount = n;
    node->bounds = b;
}

static void initInteriorNode(BVHBuildNode* node, u32 axis, BVHBuildNode* c0, BVHBuildNode* c1)
{
    node->children[0] = c0;
    node->children[1] = c1;
    node->bounds = unionBBox(&c0->bounds, &c1->bounds);
    node->splitAxis = axis;
    node->primitiveCount = 0;
}

static u32 currentSplitDimension = 0;

static int comparePoints(const void* a, const void* b)
{
    return ((BVHPrimitiveInfo*) a)->centroid[currentSplitDimension] <
           ((BVHPrimitiveInfo*) b)->centroid[currentSplitDimension];
}

static bool bucketPartitionComparator(BVHPrimitiveInfo* node,
                                      int minCostIndex, int bucketCount,
                                      u32 splitDimension, BBox centroidBounds)
{
    float num = node->centroid[splitDimension] - centroidBounds.min[splitDimension];
    float denom = centroidBounds.max[splitDimension] - centroidBounds.min[splitDimension];
    int bucketIndex = (int)(bucketCount * (num / denom));

    if (bucketIndex == bucketCount)
    {
        bucketIndex = bucketCount - 1;
    }

    return bucketIndex <= minCostIndex;
}

static u32 bucketPartition(
    BVHPrimitiveInfo* buildNodes,
    u32 start, u32 end,
    int minCostIndex, int bucketCount,
    u32 splitDimension, BBox centroidBounds)
{
    assert(end > start);

    u32 range = end - start;
    u32 tempBufferSize = range * sizeof(BVHPrimitiveInfo);
    BVHPrimitiveInfo* tempBuffer = (BVHPrimitiveInfo*) malloc(tempBufferSize);

    u32 startIndex = 0;
    u32 endIndex = range - 1;

    for (u32 i = start; i < end; i++)
    {
        BVHPrimitiveInfo* node = &buildNodes[i];

        if (bucketPartitionComparator(node,
                                      minCostIndex, bucketCount,
                                      splitDimension, centroidBounds))
        {
            *(tempBuffer + startIndex) = *node;
            startIndex++;
        }
        else
        {
            *(tempBuffer + endIndex) = *node;
            endIndex--;
        }
    }

    memcpy(buildNodes + start, tempBuffer, tempBufferSize);

    free(tempBuffer);

    u32 firstFalse = start + startIndex;

    return firstFalse;
}

struct LinearBVHNode
{
    BBox bounds;

    union
    {
        u32 primitivesOffset; // leaf
        u32 secondChildOffset; // interior
    };

    u8 primitiveCount; // 0 -> interior node
    u8 axis; // interior node: xyz
    u8 pad[2]; // ensure 32 byte total size
};

static u32 flattenBVHTree(BVHBuildNode* buildNode, LinearBVHNode* linearNodes, u32* offset)
{
    LinearBVHNode* linearNode = &linearNodes[*offset];
    linearNode->bounds = buildNode->bounds;
    u32 myOffset = (*offset)++;

    if (buildNode->primitiveCount > 0)
    {
        assert(!buildNode->children[0] && !buildNode->children[1]);
        linearNode->primitivesOffset = buildNode->firstPrimitiveOffset;
        linearNode->primitiveCount = (u8) buildNode->primitiveCount;
    }
    else
    {
        linearNode->axis = (u8) buildNode->splitAxis;
        linearNode->primitiveCount = 0;
        flattenBVHTree(buildNode->children[0], linearNodes, offset);
        linearNode->secondChildOffset = flattenBVHTree(buildNode->children[1], linearNodes, offset);
    }

    return myOffset;
}

static BVHBuildNode* recursiveBuildBVH(BVHBuildNode* nodes, u32* nodeIndex,
                                       BVHPrimitiveInfo* buildData,
                                       u32 start, u32 end,
                                       u32* orderedPrimitives, u32* orderedPrimitivesIndex)
{
    assert(start != end);

    BVHBuildNode* node = nodes + (*nodeIndex);
    (*nodeIndex)++;

    BBox bbox;

    for (u32 i = start; i < end; i++)
    {
        bbox = unionBBox(&bbox, &buildData[i].bounds);
    }

    u32 primitiveCount = end - start;

    if (primitiveCount == 1)
    {
        // create leaf BVHbuildnode
        u32 firstPrimitiveOffset = *orderedPrimitivesIndex;
        for (u32 i = start; i < end; i++)
        {
            u32 primitiveId = buildData[i].primitiveId;
            orderedPrimitives[*orderedPrimitivesIndex] = primitiveId;
            (*orderedPrimitivesIndex)++;
        }

        initLeafNode(node, firstPrimitiveOffset, primitiveCount, bbox);
    }
    else
    {
        BBox centroidBounds;

        for (u32 i = start; i < end; i++)
        {
            centroidBounds = unionBBox(&centroidBounds, buildData[i].centroid);
        }

        u32 splitDimension = maximumExtentBBox(&centroidBounds);

        u32 mid = (start + end) / 2;

        if (centroidBounds.max[splitDimension] == centroidBounds.min[splitDimension])
        {
            // create leaf BVHbuildnode
            u32 firstPrimitiveOffset = *orderedPrimitivesIndex;
            for (u32 i = start; i < end; i++)
            {
                u32 primitiveId = buildData[i].primitiveId;
                orderedPrimitives[*orderedPrimitivesIndex] = primitiveId;
                (*orderedPrimitivesIndex)++;
            }

            initLeafNode(node, firstPrimitiveOffset, primitiveCount, bbox);

            return node;
        }

        // NOTE(vinht): Surface area heuristic partitioning essentially tries to find
        // most optimum split index by brute-forcing every potential split location
        // and finding one that has smallest surface area.
        if (primitiveCount <= 4)
        {
            // partition primitives into equally-sized subsets
            currentSplitDimension = splitDimension;
            qsort(buildData + start, end - start, sizeof(BVHPrimitiveInfo), comparePoints);
        }
        else
        {
            // allocate bucketinfo for SAH partition buckets
            const int bucketCount = 12;
            struct BucketInfo
            {
                BucketInfo()
                {
                    count = 0;
                }

                int count;
                BBox bounds;
            };

            BucketInfo buckets[bucketCount];

            // initialize bucketinfo for SAH partition buckets
            for (u32 i = start; i < end; i++)
            {
                float num = buildData[i].centroid[splitDimension] - centroidBounds.min[splitDimension];
                float denom = centroidBounds.max[splitDimension] - centroidBounds.min[splitDimension];
                int bucket = (int)(bucketCount * (num / denom));

                if (bucket == bucketCount)
                {
                    bucket = bucketCount - 1;
                }

                buckets[bucket].count++;
                buckets[bucket].bounds = unionBBox(&buckets[bucket].bounds, &buildData[i].bounds);
            }

            // compute costs for splitting after each bucket
            float cost[bucketCount - 1];

            for (u32 i = 0; i < bucketCount - 1; i++)
            {
                BBox b0, b1;
                u32 count0 = 0, count1 = 0;

                for (u32 j = 0; j <= i; j++)
                {
                    b0 = unionBBox(&b0, &buckets[j].bounds);
                    count0 += buckets[j].count;
                }

                for (u32 j = i + 1; j < bucketCount; j++)
                {
                    b1 = unionBBox(&b1, &buckets[j].bounds);
                    count1 += buckets[j].count;
                }

                float b0SurfaceArea = surfaceAreaBBox(&b0);
                float b1SurfaceArea = surfaceAreaBBox(&b1);
                float bboxSurfaceArea = surfaceAreaBBox(&bbox);

                // NOTE(vinht): In practice 1/8 seems to be good enough estimate.
                cost[i] = 0.125f * (count0 * b0SurfaceArea + count1 * b1SurfaceArea) / bboxSurfaceArea;
            }

            // find bucket to split at that minimizes SAH metric
            float minCost = cost[0];
            u32 minCostIndex = 0;

            for (u32 i = 1; i < bucketCount - 1; i++)
            {
                if (cost[i] < minCost)
                {
                    minCost = cost[i];
                    minCostIndex = i;
                }
            }

            // NOTE(vinht): In practice this number does not seem to make any difference.
            const int maxPrimitivesInNode = 255;

            if (primitiveCount > maxPrimitivesInNode || minCost < primitiveCount)
            {
                mid = bucketPartition(buildData,
                                      start, end,
                                      minCostIndex, bucketCount,
                                      splitDimension, centroidBounds);
            }
            else
            {
                u32 firstPrimitiveOffset = *orderedPrimitivesIndex;
                for (u32 i = start; i < end; i++)
                {
                    u32 primitiveId = buildData[i].primitiveId;
                    orderedPrimitives[*orderedPrimitivesIndex] = primitiveId;
                    (*orderedPrimitivesIndex)++;
                }

                initLeafNode(node, firstPrimitiveOffset, primitiveCount, bbox);

                return node;
            }
        }

        initInteriorNode(node, splitDimension,
                         recursiveBuildBVH(nodes, nodeIndex,
                                           buildData, start, mid,
                                           orderedPrimitives, orderedPrimitivesIndex),
                         recursiveBuildBVH(nodes, nodeIndex,
                                           buildData, mid, end,
                                           orderedPrimitives, orderedPrimitivesIndex));
    }

    return node;
}

static inline bool intersectBBox(BBox bounds, Ray* ray,
                                 vec3 invDir, u32 dirIsNeg[3])
{
    float tmin = (bounds[dirIsNeg[0]].x - ray->origin.x) * invDir.x;
    float tmax = (bounds[1 - dirIsNeg[0]].x - ray->origin.x) * invDir.x;
    float tymin = (bounds[dirIsNeg[1]].y - ray->origin.y) * invDir.y;
    float tymax = (bounds[1 - dirIsNeg[1]].y - ray->origin.y) * invDir.y;

    if ((tmin > tymax) || (tymin > tmax)) return false;
    if (tymin > tmin) tmin = tymin;
    if (tymax < tmax) tmax = tymax;

    float tzmin = (bounds[dirIsNeg[2]].z - ray->origin.z) * invDir.z;
    float tzmax = (bounds[1 - dirIsNeg[2]].z - ray->origin.z) * invDir.z;

    if ((tmin > tzmax) || (tzmin > tmax)) return false;
    if (tzmin > tmin) tmin = tzmin;
    if (tzmax < tmax) tmax = tzmax;

    return (tmin < tMaxRay) && (tmax > tMinRay);
}