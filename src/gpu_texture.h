/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"
#include "compute.h"
#include "texture.h"

static cl_mem uploadTextureToGPU(RawTexture* texture, cl_context context)
{
    cl_image_format imageFormat;
    imageFormat.image_channel_order = CL_RGBA;
    imageFormat.image_channel_data_type = CL_UNORM_INT8;

    cl_int errorCode;

    cl_mem image;

    image = clCreateImage2D(context,
                            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                            &imageFormat,
                            texture->width,
                            texture->height,
                            0,
                            texture->data,
                            &errorCode);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to create cl image object.\n");
        return NULL;
    }

    return image;
}

static cl_sampler createGPUTextureSampler(cl_context context)
{
    cl_int errorCode;

    cl_sampler sampler;

    sampler = clCreateSampler(context,
                              GL_TRUE,
                              CL_ADDRESS_CLAMP_TO_EDGE,
                              CL_FILTER_NEAREST,
                              &errorCode);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to create cl texture sampler.\n");
        return NULL;
    }

    return sampler;
}