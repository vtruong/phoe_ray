/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "triangle.h"
#include "sphere.h"
#include "bbox.h"

struct Ray
{
    vec3 origin;
    vec3 direction;
};

static const float tMinRay = 1e-4f;
static const float tMaxRay = 1e+4f;

// NOTE(vinht):
// M�ller and Trumbore ray-triangle intersection test from the book: Physically Based Rendering, 2nd edition
static bool intersectTriangle(Ray* ray, Triangle* triangle, float* tHit, vec2* tUV, vec3* tN)
{
    // compute s1
    vec3 e1 = triangle->pb - triangle->pa;
    vec3 e2 = triangle->pc - triangle->pa;
    vec3 s1 = glm::cross(ray->direction, e2);
    float divisor = glm::dot(s1, e1);

    if (divisor == 0.0f)
    {
        return false;
    }

    float invDivisor = 1.0f / divisor;

    // compute first barycentric coordinate
    vec3 s = ray->origin - triangle->pa;
    float b1 = glm::dot(s, s1) * invDivisor;

    if (b1 < 0.0f || b1 > 1.0f)
    {
        return false;
    }

    // compute second barycentric coordinate
    vec3 s2 = glm::cross(s, e1);
    float b2 = glm::dot(ray->direction, s2) * invDivisor;

    if (b2 < 0.0f || b1 + b2 > 1.0f)
    {
        return false;
    }

    // compute t to intersection point
    float t = glm::dot(e2, s2) * invDivisor;

    if (t < tMinRay || t > tMaxRay)
    {
        return false;
    }

    *tHit = t;

    float b0 = 1.0f - b1 - b2;

    tUV->s = b0 * triangle->ua.s + b1 * triangle->ub.s + b2 * triangle->uc.s;
    tUV->t = b0 * triangle->ua.t + b1 * triangle->ub.t + b2 * triangle->uc.t;

    tN->x = b0 * triangle->na.x + b1 * triangle->nb.x + b2 * triangle->nc.x;
    tN->y = b0 * triangle->na.y + b1 * triangle->nb.y + b2 * triangle->nc.y;
    tN->z = b0 * triangle->na.z + b1 * triangle->nb.z + b2 * triangle->nc.z;

    return true;
}

// NOTE(vinht):
// This sphere intersection test is from the book: Physically Based Rendering, 2nd edition
static bool solveQuadratic(float a, float b, float c, float* t0, float* t1)
{
    float discriminant = b * b - 4.0f * a * c;
    if (discriminant <= 0.0f)
    {
        return false;
    }

    float rootDiscriminant = sqrtf(discriminant);

    float q;

    if (b < 0.0f)
    {
        q = -0.5f * (b - rootDiscriminant);
    }
    else
    {
        q = -0.5f * (b + rootDiscriminant);
    }

    *t0 = q / a;
    *t1 = c / q;

    if (*t0 > *t1)
    {
        float temp = *t1;
        *t1 = *t0;
        *t0 = temp;
    }

    return true;
}

static bool intersectSphere(Ray* ray, Sphere* sphere, float* tHit)
{
    float a = glm::dot(ray->direction, ray->direction);
    float b = 2.0f * glm::dot(ray->direction, ray->origin);
    float c = glm::dot(ray->origin, ray->origin) - (sphere->radius * sphere->radius);

    float t0, t1;
    if (!solveQuadratic(a, b, c, &t0, &t1))
    {
        return false;
    }

    if (t0 > tMaxRay || t1 < tMinRay)
    {
        return false;
    }

    *tHit = t0;

    if (t0 < tMinRay)
    {
        *tHit = t1;

        if (*tHit > tMaxRay)
        {
            return false;
        }
    }

    return true;
}

// NOTE(vinht): Ray-bbox intersection test is from the book: Physically Based Rendering, 2nd edition
static bool intersectBBox(Ray* ray, BBox* bbox, float* tHit0, float* tHit1)
{
    float t0 = tMinRay;
    float t1 = tMaxRay;

    for (int i = 0; i < 3; ++i)
    {
        // NOTE(vinht): invRayDir can be zero so resulting divides can have -inf or inf.
        // However the algorithm still works correctly.
        float invRayDir = 1.f / ray->direction[i];

        // NOTE(vinht):
        // There is also small thing to note when the ray is exactly at bbox boundary.
        // In that case, we actually get 0/0 which according to IEEE754 results in NaN
        // and logical comparisons to NaN always results in false.
        float tNear = (bbox->min[i] - ray->origin[i]) * invRayDir;
        float tFar = (bbox->max[i] - ray->origin[i]) * invRayDir;

        if (tNear > tFar)
        {
            float temp = tNear;
            tNear = tFar;
            tFar = temp;
        }

        t0 = tNear > t0 ? tNear : t0;
        t1 = tFar < t1 ? tFar : t1;

        if (t0 > t1)
        {
            return false;
        }
    }

    if (tHit0)
    {
        *tHit0 = t0;
    }

    if (tHit1)
    {
        *tHit1 = t1;
    }

    return true;
}
