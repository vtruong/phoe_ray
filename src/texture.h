/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"

struct RawTexture
{
    int width;
    int height;
    u8* data;
};

struct Texture
{
    int width;
    int height;
    int offset;
};

struct TextureName
{
    char buffer[64];
};

static bool loadTextureFromFile(const char* path, RawTexture* texture)
{
    bool success = true;

    FILE* f = fopen(path, "rb");

    int components = 0;

    if (f)
    {
        texture->data = stbi_load_from_file(f, &texture->width, &texture->height, &components, 0);

        printf("Load %s\n", path);
        printf("  width: %d\n", texture->width);
        printf("  height: %d\n", texture->height);
        printf("  components: %d\n", components);

        fclose(f);
    }
    else
    {
        printf("Failed to load texture: %s\n", path);

        success = false;
    }

    if (components != 4)
    {
        printf("Error: Texture has to have four color components. (%s)\n", path);

        success = false;
    }

    return success;
}

static void unloadTexture(RawTexture* texture)
{
    stbi_image_free(texture->data);
}

static vec3 readTextureColor(Texture* texture, u8* textureBuffer, vec2 uv)
{
    int x = (int) glm::floor(uv.x * texture->width);
    int y = (int) glm::floor(uv.y * texture->height);

    vec3 color;

    int pixel = texture->offset + 4 * (y * texture->width + x);

    color.x = textureBuffer[pixel + 0] / 255.0f;
    color.y = textureBuffer[pixel + 1] / 255.0f;
    color.z = textureBuffer[pixel + 2] / 255.0f;

    return color;
}