/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"

static u32 createProgram(const char* vert, const char* frag)
{
    u32 vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vert, NULL);
    glCompileShader(vertexShader);

    int status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        char buffer[2048];
        glGetShaderInfoLog(vertexShader, 2048, NULL, buffer);
        printf("Shader compilation error (Vertex):\n%s\n", buffer);
        assert(1 == 0);
    }

    u32 fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &frag, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE)
    {
        char buffer[2048];
        glGetShaderInfoLog(fragmentShader, 2048, NULL, buffer);
        printf("Shader compilation error (Fragment):\n%s\n", buffer);
        assert(1 == 0);
    }

    u32 program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &status);
    if (status != GL_TRUE)
    {
        char buffer[2048];
        glGetProgramInfoLog(program, 2048, NULL, buffer);
        printf("Shader linking error:\n%s\n", buffer);
        assert(1 == 0);
    }

    glDeleteShader(fragmentShader);
    glDeleteShader(vertexShader);

    return program;
}

struct ScreenQuadShader
{
    u32 program;
};

static ScreenQuadShader createScreenQuadShader()
{
    const char* vertexSource =
        "#version 330\n"

        "layout(location = 0) in vec2 inPosition;"
        "layout(location = 1) in vec2 inTexcoord;"
        "out vec2 Texcoord;"

        "void main()"
        "{"
        "   Texcoord = inTexcoord;"
        "   gl_Position = vec4(inPosition, 0.0, 1.0);"
        "}";

    const char* fragmentSource =
        "#version 330\n"

        "in vec2 Texcoord;"
        "out vec4 outColor;"

        "uniform sampler2D pixelBuffer;"

        "void main()"
        "{"
        "   outColor = vec4(texture(pixelBuffer, Texcoord).rgb, 1.0);"
        "}";

    ScreenQuadShader shader;

    shader.program = createProgram(vertexSource, fragmentSource);
    glUniform1i(glGetUniformLocation(shader.program, "pixelBuffer"), 0);

    return shader;
}

struct BasicShader
{
    u32 program;

    u32 projection;
    u32 view;
    u32 model;
    u32 color;
    u32 directionToLight;
    u32 gamma;
    u32 alpha;
    u32 emitting;
};

static BasicShader createBasicShader()
{
    const char* vertexSource =
        "#version 330\n"

        "layout(location = 0) in vec3 inPosition;"
        "layout(location = 1) in vec3 inNormal;"
        "layout(location = 2) in vec2 inUV;"
        "out vec3 Normal;"
        "out vec2 UV;"

        "uniform mat4 projection;"
        "uniform mat4 view;"
        "uniform mat4 model;"

        "void main()"
        "{"
        "   Normal = mat3(model) * inNormal;"
        "   UV = inUV;"
        "   vec4 position = vec4(inPosition, 1.0);"
        "   gl_Position = projection * view * model * position;"
        "}";

    const char* fragmentSource =
        "#version 330\n"

        "in vec3 Normal;"
        "in vec2 UV;"
        "out vec4 outColor;"

        "uniform vec3 color;"
        "uniform vec3 directionToLight;"
        "uniform float gamma;"
        "uniform float alpha;"
        "uniform float emitting;"

        "void main()"
        "{"
        "   vec3 normal = normalize(Normal);"
        "   float diffuse = max(emitting, dot(normal, directionToLight));"
        "   outColor = vec4(pow(color, vec3(gamma)) * diffuse, alpha);"
        "}";

    BasicShader shader;

    shader.program = createProgram(vertexSource, fragmentSource);

    shader.projection = glGetUniformLocation(shader.program, "projection");
    shader.view = glGetUniformLocation(shader.program, "view");
    shader.model = glGetUniformLocation(shader.program, "model");
    shader.color = glGetUniformLocation(shader.program, "color");
    shader.directionToLight = glGetUniformLocation(shader.program, "directionToLight");
    shader.gamma = glGetUniformLocation(shader.program, "gamma");
    shader.alpha = glGetUniformLocation(shader.program, "alpha");
    shader.emitting = glGetUniformLocation(shader.program, "emitting");

    return shader;
}

struct LineShader
{
    u32 program;

    u32 projection;
    u32 view;
};

static LineShader createLineShader()
{
    const char* vertexSource =
        "#version 330\n"

        "layout(location = 0) in vec3 inPosition;"
        "layout(location = 1) in vec4 inColor;"

        "out vec4 Color;"

        "uniform mat4 projection;"
        "uniform mat4 view;"

        "void main()"
        "{"
        "   Color = inColor;"
        "   vec4 position = vec4(inPosition, 1.0);"
        "   gl_Position = projection * view * position;"
        "}";

    const char* fragmentSource =
        "#version 330\n"

        "in vec4 Color;"
        "out vec4 outColor;"

        "void main()"
        "{"
        "   outColor = Color;"
        "}";

    LineShader shader;

    shader.program = createProgram(vertexSource, fragmentSource);

    shader.projection = glGetUniformLocation(shader.program, "projection");
    shader.view = glGetUniformLocation(shader.program, "view");

    return shader;
}

struct GUIShader
{
    u32 program;

    u32 projection;
    u32 texture;
};

static GUIShader createGUIShader()
{
    const char* vertexSource =
        "#version 330\n"

        "layout(location = 0) in vec2 inPosition;"
        "layout(location = 1) in vec2 inUV;"
        "layout(location = 2) in vec4 inColor;"

        "out vec2 UV;"
        "out vec4 Color;"

        "uniform mat4 projection;"

        "void main()"
        "{"
        "	UV = inUV;"
        "	Color = inColor;"
        "	gl_Position = projection * vec4(inPosition.xy, 0,1);"
        "}";

    const char* fragmentSource =
        "#version 330\n"

        "uniform sampler2D Texture;"

        "in vec2 UV;"
        "in vec4 Color;"

        "out vec4 outColor;"

        "void main()"
        "{"
        "	outColor = Color * texture(Texture, UV.st);"
        "}";

    GUIShader shader;

    shader.program = createProgram(vertexSource, fragmentSource);

    shader.projection = glGetUniformLocation(shader.program, "projection");
    shader.texture = glGetUniformLocation(shader.program, "Texture");

    return shader;
}

static void freeShaderProgram(u32 program)
{
    glDeleteProgram(program);
}