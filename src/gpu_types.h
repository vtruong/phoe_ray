/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"

// NOTE(vinht): These types are precisely aligned for the GPU

struct GPUTriangle
{
    // 16 16 16
    cl_float3 pa, pb, pc;

    // 16 16 16
    cl_float3 na, nb, nc;

    // 8 8 8
    cl_float2 ua, ub, uc;

    // 4 4
    cl_uint materialId;
    cl_uint pad;
};

struct GPUMaterial
{
    // 16
    cl_float3 color;

    // 4 4 4
    cl_uint textureId;
    cl_uint surfaceType;
    cl_uint textureType;

    // 4 4 4
    cl_float roughness;
    cl_float emission;
    cl_float ior;

    cl_uint pad[2];
};

struct GPURay
{
    // 16 x 2
    cl_float3 origin, direction;
};

struct GPUBBox
{
    // 16 x 2
    cl_float3 corners[2];
};

struct GPUBVHNode
{
    // 16 x 2
    GPUBBox bounds;

    // 16
    cl_uint offset;
    cl_uint primitiveCount;
    cl_uint axis;
    cl_uint pad;
};