/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"
#include "core.h"

struct ThreadJob
{
    int x, y;
    int w, h;
};

struct JobQueue
{
    ThreadJob* jobs;
    u32 jobCount;

    u32 jobsAvailable;
    u32 finishedJobs;
    u32 recentlyFinishedJobs;
    u32 threadsFinished;
    u32 samplesFinished;

    std::mutex* mutex;
    std::condition_variable* cv;
};

struct Thread
{
    std::thread* thread;
};

struct CPUPathTracer
{
    JobQueue jobQueue;
    Thread threads[threadCount];

    Core* core;
};

static void partitionScreenIntoJobs(int screenWidth, int screenHeight, int partitionWidth, JobQueue* jobQueue)
{
    int horizontalBlockCount = screenWidth / partitionWidth;
    int horizontalLeftOver = screenWidth % partitionWidth;

    int verticalBlockCount = screenHeight / partitionWidth;
    int verticalLeftOver = screenHeight % partitionWidth;

    int horizontalDegenerateArea = horizontalBlockCount * verticalLeftOver * partitionWidth;
    int verticalDegenerateArea = verticalBlockCount * horizontalLeftOver * partitionWidth;

    int totalBlockCount = horizontalBlockCount * verticalBlockCount;

    if (horizontalDegenerateArea > 0)
    {
        totalBlockCount += horizontalBlockCount;
    }

    if (verticalDegenerateArea > 0)
    {
        totalBlockCount += verticalBlockCount;
    }

    if (horizontalLeftOver > 0 && verticalBlockCount > 0)
    {
        totalBlockCount += 1;
    }

    free(jobQueue->jobs);
    jobQueue->jobs = (ThreadJob*) malloc(totalBlockCount * sizeof(ThreadJob));
    jobQueue->jobCount = totalBlockCount;
    int blockIndex = 0;

    for (int y = 0; y < verticalBlockCount; y++)
    {
        for (int x = 0; x < horizontalBlockCount; x++)
        {
            int blockWidth = partitionWidth;
            int blockHeight = partitionWidth;

            ThreadJob* block = &jobQueue->jobs[blockIndex++];
            block->x = partitionWidth * x;
            block->y = partitionWidth * y;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (horizontalDegenerateArea > 0)
    {
        for (int x = 0; x < horizontalBlockCount; x++)
        {
            int blockWidth = partitionWidth;
            int blockHeight = verticalLeftOver;

            ThreadJob* block = &jobQueue->jobs[blockIndex++];
            block->x = partitionWidth * x;
            block->y = partitionWidth * verticalBlockCount;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (verticalDegenerateArea > 0)
    {
        for (int y = 0; y < verticalBlockCount; y++)
        {
            int blockWidth = horizontalLeftOver;
            int blockHeight = partitionWidth;

            ThreadJob* block = &jobQueue->jobs[blockIndex++];
            block->x = partitionWidth * horizontalBlockCount;
            block->y = partitionWidth * y;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (horizontalDegenerateArea > 0 && verticalDegenerateArea > 0)
    {
        int blockWidth = horizontalLeftOver;
        int blockHeight = verticalLeftOver;

        ThreadJob* block = &jobQueue->jobs[blockIndex++];
        block->x = partitionWidth * horizontalBlockCount;
        block->y = partitionWidth * verticalBlockCount;
        block->w = blockWidth;
        block->h = blockHeight;
    }

    // NOTE(vinht): Fisher & Yates -shuffle
    for (int blockId = 0; blockId < totalBlockCount - 1; blockId++)
    {
        int swapId = (int) (randf() * (float) totalBlockCount);

        ThreadJob temp = jobQueue->jobs[blockId];
        jobQueue->jobs[blockId] = jobQueue->jobs[swapId];
        jobQueue->jobs[swapId] = temp;
    }
}

static vec3 radiance(Ray* ray, Scene* scene)
{
    vec3 finalColor = vec3(1.0f);

    RayHit hit;

    for (int depth = 0; depth < 6; depth++)
    {
        if (bvhIntersect(ray, scene, &hit))
        {
            Material* hitMaterial = &scene->materials[hit.materialId];

            ray->origin = hit.position;

            /////////////////////////////////////////////////////
            // COLOR
            /////////////////////////////////////////////////////

            vec3 materialColor = hitMaterial->color;

            if (hitMaterial->textureType == TEXTURE_IMAGE)
            {
                materialColor = readTextureColor(&scene->textures[hitMaterial->textureId],
                                                 scene->textureBuffer,
                                                 hit.uv);
            }
            else if (hitMaterial->textureType == TEXTURE_CHECKERBOARD)
            {
                int px = (int) glm::floor(hit.position.x * 0.25f);
                int py = (int) glm::floor(hit.position.z * 0.25f);
                materialColor *= ((px + py) % 2 == 0) ? 0.9f : 0.7f;
            }

            finalColor *= materialColor;

            /////////////////////////////////////////////////////
            // SURFACE
            /////////////////////////////////////////////////////

            SurfaceType surfaceType = hitMaterial->surfaceType;

            float e1 = randf();
            float e2 = randf();

            if (surfaceType & SURFACE_DIFFUSE)
            {
                ray->direction = sampleDirection(hit.normal, e1, e2);
                finalColor *= glm::abs(glm::dot(hit.normal, ray->direction));
            }
            else if (surfaceType & SURFACE_GLASS)
            {
                bool inside = false;
                if (glm::dot(ray->direction, hit.normal) > 0)
                {
                    hit.normal = -hit.normal;
                    inside = true;
                }

                e1 = (e1 - 0.5f) * hitMaterial->roughness + 0.5f;
                e2 = (e2 - 0.5f) * hitMaterial->roughness + 0.5f;

                float facingRatio = -glm::dot(ray->direction, hit.normal);
                float fresnelEffect = glm::mix(glm::pow(1.0f - facingRatio, 4.0f), 1.0f, 0.02f);
                float ior = (inside) ? hitMaterial->ior : 1.0f / hitMaterial->ior;

                if (randf() < fresnelEffect)
                {
                    ray->direction = sampleDirection(glm::reflect(ray->direction, hit.normal), e1, e2);
                }
                else
                {
                    vec3 tentativeDirection = glm::refract(ray->direction, hit.normal, ior);

                    if (glm::length(tentativeDirection) > 0.0f)
                    {
                        ray->direction = sampleDirection(tentativeDirection, e1, e2);
                    }
                }
            }
            else if (surfaceType & SURFACE_GLOSSY)
            {
                e1 = (e1 - 0.5f) * hitMaterial->roughness + 0.5f;
                e2 = (e2 - 0.5f) * hitMaterial->roughness + 0.5f;

                ray->direction = sampleDirection(glm::reflect(ray->direction, hit.normal), e1, e2);
                finalColor *= glm::abs(glm::dot(hit.normal, ray->direction));
            }
            else if (surfaceType & SURFACE_EMISSION)
            {
                finalColor *= hitMaterial->emission;
                break;
            }

            ray->origin += ray->direction * epsilon;
        }
        else
        {
            float phi = glm::atan(ray->direction.x, ray->direction.z);

            if (phi < 0.0f)
            {
                phi = glm::mod(phi + twopi, twopi);
            }

            phi /= twopi;

            vec2 uv;
            uv.x = phi;
            // NOTE(vinht): Magic constant 0.001f was chosen to hide skydomes
            // starting height slightly below the horizon.
            uv.y = glm::acos(ray->direction.y + 0.001f) / pi;
            uv = glm::clamp(uv, vec2(0.0f + epsilon), vec2(1.0f - epsilon));

            vec3 skyColor = readTextureColor(&scene->skyTexture, scene->skyTextureBuffer, uv);
            finalColor *= skyColor;

            // NOTE(vinht): Can't apply brightness factor if ray goes directly to sky
            // This will overbrighten the sky. We only want indirect effects.
            if (depth > 0)
            {
                finalColor *= 2.0f;
            }

            break;
        }
    }

    return finalColor;
}

static void gather(ThreadJob job, float* backBuffer, Camera* camera,
                   Screen* screen, Scene* scene, vec2 jitter,
                   bool* stopSignal, RenderState* renderState,
                   bool phaseOne)
{
    if (phaseOne)
    {
        int screenX = job.x + job.w / 2;
        int screenY = job.y + job.h / 2;

        Ray ray;
        ray = screenToCameraRay(screenX, screenY, camera, screen);

        vec3 finalColor = radiance(&ray, scene);

        for (int screenY = job.y; screenY < job.y + job.h; screenY++)
        {
            for (int screenX = job.x; screenX < job.x + job.w; screenX++)
            {
                u32 pixelIndex = 4 * (screenY * screen->width + screenX);

                backBuffer[pixelIndex + 0] += finalColor.r;
                backBuffer[pixelIndex + 1] += finalColor.g;
                backBuffer[pixelIndex + 2] += finalColor.b;
            }
        }
    }
    else
    {
        for (int screenY = job.y; screenY < job.y + job.h; screenY++)
        {
            for (int screenX = job.x; screenX < job.x + job.w; screenX++)
            {
                Ray ray;
                ray = screenToCameraRay(screenX, screenY, camera, screen, jitter);

                vec3 finalColor = radiance(&ray, scene);

                u32 pixelIndex = 4 * (screenY * screen->width + screenX);

                backBuffer[pixelIndex + 0] += finalColor.r;
                backBuffer[pixelIndex + 1] += finalColor.g;
                backBuffer[pixelIndex + 2] += finalColor.b;

                if (*stopSignal ||
                    *renderState == RENDER_STATE_HALTING ||
                    *renderState == RENDER_STATE_RESTARTING)
                {
                    return;
                }
            }
        }
    }
}

static void combine(ThreadJob job, u8* pixelBuffer, float* backBuffer,
                    Screen* screen, int samplesDone, bool gammaCorrection,
                    bool* stopSignal, RenderState* renderState,
                    bool phaseOne)
{
    vec3 gamma = vec3((gammaCorrection) ? 1.0f / 2.2f : 1.0f);

    float invSamples = 1.0f / (float) (samplesDone + 1);

    for (int screenY = job.y; screenY < job.y + job.h; screenY++)
    {
        for (int screenX = job.x; screenX < job.x + job.w; screenX++)
        {
            u32 pixelIndex = 4 * (screenY * screen->width + screenX);

            vec3 finalColor;

            finalColor.x = backBuffer[pixelIndex + 0];
            finalColor.y = backBuffer[pixelIndex + 1];
            finalColor.z = backBuffer[pixelIndex + 2];

            if (phaseOne)
            {
                backBuffer[pixelIndex + 0] = 0;
                backBuffer[pixelIndex + 1] = 0;
                backBuffer[pixelIndex + 2] = 0;
            }

            finalColor *= invSamples;
            finalColor = glm::clamp(finalColor, vec3(0.0f), vec3(1.0f));
            finalColor = glm::pow(finalColor, gamma);
            finalColor *= 255.0f;

            pixelBuffer[pixelIndex + 0] = (u8) finalColor.r;
            pixelBuffer[pixelIndex + 1] = (u8) finalColor.g;
            pixelBuffer[pixelIndex + 2] = (u8) finalColor.b;
            pixelBuffer[pixelIndex + 3] = (u8) 255;

            if (*stopSignal ||
                *renderState == RENDER_STATE_HALTING ||
                *renderState == RENDER_STATE_RESTARTING)
            {
                return;
            }
        }
    }
}

static void workerThread(CPUPathTracer* pathTracer, u32 threadId)
{
    Thread* thread = &pathTracer->threads[threadId];

    JobQueue* jobQueue = &pathTracer->jobQueue;

    Core* core = pathTracer->core;

    std::unique_lock<std::mutex> jobQueueLock(*jobQueue->mutex, std::defer_lock);

    while (true)
    {
        jobQueueLock.lock();

        jobQueue->cv->wait(jobQueueLock, [core, pathTracer, thread]()
        {
            return core->stopRunning || (pathTracer->jobQueue.jobsAvailable > 0);
        });

        if (core->stopRunning)
        {
            return;
        }

        int jobsPerThread = jobQueue->jobCount / threadCount;
        int jobsLeftOver = jobQueue->jobCount - (threadCount * jobsPerThread);
        int jobStartId = (int) (threadId * jobsPerThread);

        // NOTE(vinht): Left over jobs are handed over to final thread.
        if (threadCount > 1 && threadId == threadCount - 1)
        {
            jobsPerThread += jobsLeftOver;
        }

        pathTracer->jobQueue.jobsAvailable--;

        jobQueueLock.unlock();

        u32 backBufferSize = core->screen.width * core->screen.height * 4 * sizeof(float);
        float* backBuffer = (float*) malloc(backBufferSize);
        memset(backBuffer, 0, backBufferSize);

        int sampleCount = (int) core->samplesPerPixel;
        int samplesDone = 0;

        for (int sampleId = 0; sampleId < sampleCount; sampleId++)
        {
            bool phaseOne = (sampleId == 0);

            // NOTE(vinht): Simple jittering inside pixels is enough for now
            vec2 jitter;
            jitter.x = randf() - 0.5f;
            jitter.y = randf() - 0.5f;

            for (int jobId = jobStartId; jobId < jobStartId + jobsPerThread; jobId++)
            {
                ThreadJob* currentJob = jobQueue->jobs + jobId;

                gather(*currentJob, backBuffer, &core->camera,
                       &core->screen, &core->scene, jitter,
                       &core->stopRunning, &core->renderState,
                       phaseOne);

                combine(*currentJob, core->pixelBuffer, backBuffer,
                        &core->screen, samplesDone, core->gammaCorrection,
                        &core->stopRunning, &core->renderState,
                        phaseOne);

                jobQueueLock.lock();
                jobQueue->finishedJobs++;
                jobQueue->recentlyFinishedJobs++;
                jobQueueLock.unlock();

                if (core->stopRunning ||
                    core->renderState == RENDER_STATE_HALTING ||
                    core->renderState == RENDER_STATE_RESTARTING)
                {
                    goto exit;
                }
            }

            samplesDone++;

            if (core->stopRunning ||
                core->renderState == RENDER_STATE_HALTING ||
                core->renderState == RENDER_STATE_RESTARTING)
            {
                goto exit;
            }
        }

exit:
        jobQueueLock.lock();
        jobQueue->threadsFinished++;
        jobQueueLock.unlock();

        free(backBuffer);
    }
}

static void initCPUPathTracer(CPUPathTracer* pathTracer, Core* core)
{
    pathTracer->core = core;
    pathTracer->jobQueue.mutex = new std::mutex;
    pathTracer->jobQueue.cv = new std::condition_variable;

    for (u32 threadId = 0; threadId < threadCount; threadId++)
    {
        pathTracer->threads[threadId].thread = new std::thread(workerThread, pathTracer, threadId);
    }

    Screen* screen = &pathTracer->core->screen;

    partitionScreenIntoJobs(screen->width, screen->height, 8, &pathTracer->jobQueue);
}

static void prepareCPUPathTracer(CPUPathTracer* pathTracer)
{
    pathTracer->jobQueue.jobsAvailable = threadCount;
    pathTracer->jobQueue.finishedJobs = 0;
    pathTracer->jobQueue.recentlyFinishedJobs = 0;
    pathTracer->jobQueue.threadsFinished = 0;
    pathTracer->jobQueue.samplesFinished = 0;
    pathTracer->jobQueue.cv->notify_all();
}

static void freeCPUPathTracer(CPUPathTracer* pathTracer)
{
    pathTracer->jobQueue.cv->notify_all();

    for (u32 threadId = 0; threadId < threadCount; threadId++)
    {
        pathTracer->threads[threadId].thread->join();
        delete pathTracer->threads[threadId].thread;
    }

    delete pathTracer->jobQueue.mutex;
    delete pathTracer->jobQueue.cv;

    free(pathTracer->jobQueue.jobs);
}
