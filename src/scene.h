/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "objparser.h"
#include "hashmap.h"
#include "bvh.h"
#include "texture.h"

#ifdef GPU_PATH_TRACE
    #include "gpu_types.h"
#endif

struct Scene
{
    vec3 directionToLight;

    char filePath[512];

    Mesh* meshes;
    u32 meshCount;

    TriangleMesh* triangleMeshes;
    u32 triangleMeshCount;

    u32* renderOrderBuffer;

    Triangle* triangles;
    u32 triangleCount;

    Material* materials;
    u32 materialCount;

    BBox* bboxes;
    u32 bboxCount;

    Texture* textures;
    u32 textureCount;
    u8* textureBuffer;
    u32 textureBufferSize;

    Texture skyTexture;
    u8* skyTextureBuffer;
    u32 skyTextureBufferSize;

    LinearBVHNode* bvhNodes;
    u32 bvhNodeCount;

    char* materialNamesStringified;
    char* textureNamesStringified;

#ifdef GPU_PATH_TRACE
    GPUTriangle* alignedTriangles;
    u32 alignedTrianglesSize;
    GPUMaterial* alignedMaterials;
    u32 alignedMaterialsSize;
    GPUBVHNode* alignedBvhNodes;
    u32 alignedBvhNodesSize;
#endif
};

static bool loadTextures(HashMap* textureMap, RawTexture* rawTextures)
{
    char texturePath[2048] = {};

    for (u32 hashIndex = 0; hashIndex < maxHashMapSize; hashIndex++)
    {
        if (textureMap->occupancy[hashIndex] == 0)
        {
            continue;
        }

        u32 data = textureMap->data[hashIndex];

        const char* textureName = (const char*) textureMap->keys[hashIndex];

        strcat(texturePath, "res/textures/");
        strcat(texturePath, textureName);

        if (!loadTextureFromFile(texturePath, &rawTextures[data]))
        {
            printf("Failed to load texture: %s\n", texturePath);
            return false;
        }

        memset(texturePath, 0, sizeof(texturePath));
    }

    return true;
}

static bool buildScene(Scene* scene, const char* objPath)
{
    scene->directionToLight = vec3(-0.602595f, 0.753244f, -0.263635f);

    u32 meshCount = getOBJMeshCount(objPath);

    scene->meshCount = meshCount;
    scene->meshes = (Mesh*) calloc(scene->meshCount, sizeof(Mesh));

    scene->triangleMeshCount = meshCount;
    scene->triangleMeshes = (TriangleMesh*) calloc(scene->triangleMeshCount, sizeof(TriangleMesh));

    /////////////////////////////////////////////////////
    // MATERIALS
    /////////////////////////////////////////////////////

    char mtlPath[512];

    if (!readOBJMTLName(objPath, mtlPath))
    {
        // NOTE(vinht): Technically this should not prevent loading, but it would
        // require lot more work to setup cases where there are zero materials.
        // It is way easier to skip material-less cases.
        printf("Scene is missing material file\n.");
        return false;
    }
    else
    {
        char temp[512] = {};
        strcat(temp, "res/meshes/");
        strcat(temp, mtlPath);
        strcpy(mtlPath, temp);
    }

    u32 materialCount = getOBJMaterialCount(mtlPath);

    scene->materialCount = materialCount;
    scene->materials = (Material*) calloc(scene->materialCount, sizeof(Material));

    HashMap materialMap, textureMap;
    memset(&materialMap, 0, sizeof(materialMap));
    memset(&textureMap, 0, sizeof(textureMap));

    readOBJMaterials(mtlPath, scene->materials, &materialMap, &textureMap);

    // Gather and stringify material names for GUI
    MaterialName* materialNames = (MaterialName*) calloc(scene->materialCount, sizeof(MaterialName));

    u32 materialNameTotalLength = 0;

    for (u32 hashIndex = 0; hashIndex < maxHashMapSize; hashIndex++)
    {
        if (materialMap.occupancy[hashIndex] == 1)
        {
            u32 data = materialMap.data[hashIndex];
            strcpy(materialNames[data].buffer, (char*) materialMap.keys[hashIndex]);

            materialNameTotalLength += strlen((char*) materialMap.keys[hashIndex]);
            materialNameTotalLength++; // NOTE(vinht): Null character.
        }
    }
    materialNameTotalLength++; // NOTE(vinht): Null character.

    scene->materialNamesStringified = (char*) calloc(materialNameTotalLength, 1);

    u32 materialNameOffset = 0;

    for (u32 materialId = 0; materialId < scene->materialCount; materialId++)
    {
        strcat(scene->materialNamesStringified + materialNameOffset,
               materialNames[materialId].buffer);
        materialNameOffset += strlen(materialNames[materialId].buffer);
        materialNameOffset++; // NOTE(vinht): Null character.
    }

    free(materialNames);

    /////////////////////////////////////////////////////
    // TEXTURES
    /////////////////////////////////////////////////////

    scene->textureCount = textureMap.size;

    RawTexture* rawTextures = (RawTexture*) calloc(scene->textureCount, sizeof(RawTexture));

    if (!loadTextures(&textureMap, rawTextures))
    {
        printf("Failed to load textures.\n");
        return false;
    }

    u32 textureBufferSize = 0;

    for (u32 textureId = 0; textureId < scene->textureCount; textureId++)
    {
        RawTexture* rawTexture = rawTextures + textureId;

        textureBufferSize += 4 * rawTexture->width * rawTexture->height;
    }

    scene->textures = (Texture*) calloc(scene->textureCount, sizeof(Texture));
    scene->textureBuffer = (u8*) malloc(textureBufferSize);
    scene->textureBufferSize = textureBufferSize;

    u32 textureBufferOffset = 0;

    for (u32 textureId = 0; textureId < scene->textureCount; textureId++)
    {
        RawTexture* rawTexture = rawTextures + textureId;
        Texture* texture = scene->textures + textureId;

        texture->width = rawTexture->width;
        texture->height = rawTexture->height;
        texture->offset = textureBufferOffset;

        u32 textureSize = 4 * texture->width * texture->height;

        memcpy(scene->textureBuffer + textureBufferOffset, rawTexture->data, textureSize);

        textureBufferOffset += textureSize;

        unloadTexture(rawTexture);
    }

    free(rawTextures);

    // Gather and stringify texture names for GUI
    TextureName* textureNames = (TextureName*) calloc(scene->textureCount, sizeof(TextureName));

    u32 textureNameTotalLength = 0;

    for (u32 hashIndex = 0; hashIndex < maxHashMapSize; hashIndex++)
    {
        if (textureMap.occupancy[hashIndex] == 1)
        {
            u32 data = textureMap.data[hashIndex];
            strcpy(textureNames[data].buffer, (char*) textureMap.keys[hashIndex]);

            textureNameTotalLength += strlen((char*) textureMap.keys[hashIndex]);
            textureNameTotalLength++; // NOTE(vinht): Null character.
        }
    }
    textureNameTotalLength++; // NOTE(vinht): Null character.

    scene->textureNamesStringified = (char*) calloc(textureNameTotalLength, 1);

    u32 textureNameOffset = 0;

    for (u32 textureId = 0; textureId < scene->textureCount; textureId++)
    {
        strcat(scene->textureNamesStringified + textureNameOffset,
               textureNames[textureId].buffer);
        textureNameOffset += strlen(textureNames[textureId].buffer);
        textureNameOffset++; // NOTE(vinht): Null character.
    }

    free(textureNames);

    // Sky texture
    RawTexture rawSkyTexture;

    if (!loadTextureFromFile("res/textures/sky.png", &rawSkyTexture))
    {
        printf("Failed to load sky texture.\n");
        return false;
    }

    scene->skyTexture.width = rawSkyTexture.width;
    scene->skyTexture.height = rawSkyTexture.height;
    u32 rawSkyTextureSize = 4 * rawSkyTexture.width * rawSkyTexture.height;
    scene->skyTextureBuffer = (u8*) malloc(rawSkyTextureSize);
    scene->skyTextureBufferSize = rawSkyTextureSize;
    memcpy(scene->skyTextureBuffer, rawSkyTexture.data, rawSkyTextureSize);

    unloadTexture(&rawSkyTexture);

    /////////////////////////////////////////////////////
    // MESHES
    /////////////////////////////////////////////////////

    readOBJFile(objPath, scene->meshes, &materialMap);

    scene->renderOrderBuffer = (u32*) calloc(scene->meshCount, sizeof(u32));

    for (u32 meshId = 0; meshId < scene->meshCount; meshId++)
    {
        Mesh* mesh = scene->meshes + meshId;
        TriangleMesh* triangleMesh = scene->triangleMeshes + meshId;

        triangleMesh->mesh = mesh;
        triangleMesh->objectToWorld = mat4(1.0f);
        triangleMesh->materialId = mesh->materialId;

        mesh->centroid = mesh->bbox.min + 0.5f * (mesh->bbox.max - mesh->bbox.min);

        scene->renderOrderBuffer[meshId] = meshId;
    }

    return true;
}

static void triangulateScene(Scene* scene)
{
    for (u32 meshId = 0; meshId < scene->triangleMeshCount; meshId++)
    {
        scene->triangleCount += scene->triangleMeshes[meshId].mesh->vertexCount / 3;
    }

    scene->triangles = (Triangle*) malloc(scene->triangleCount * sizeof(Triangle));

    for (u32 meshId = 0, triangleId = 0; meshId < scene->triangleMeshCount; meshId++)
    {
        TriangleMesh* triangleMesh = &scene->triangleMeshes[meshId];
        Mesh* mesh = triangleMesh->mesh;
        mat4 objectToWorld = triangleMesh->objectToWorld;

        for (u32 vertexId = 0;
             vertexId < mesh->vertexCount;
             vertexId += 3, triangleId++)
        {
            Triangle* triangle = scene->triangles + triangleId;
            triangle->pa = mesh->vertices[vertexId + 0].position;
            triangle->pb = mesh->vertices[vertexId + 1].position;
            triangle->pc = mesh->vertices[vertexId + 2].position;

            triangle->pa = vec3(objectToWorld * vec4(triangle->pa, 1.0f));
            triangle->pb = vec3(objectToWorld * vec4(triangle->pb, 1.0f));
            triangle->pc = vec3(objectToWorld * vec4(triangle->pc, 1.0f));

            triangle->na = glm::normalize(mat3(objectToWorld) * mesh->vertices[vertexId + 0].normal);
            triangle->nb = glm::normalize(mat3(objectToWorld) * mesh->vertices[vertexId + 1].normal);
            triangle->nc = glm::normalize(mat3(objectToWorld) * mesh->vertices[vertexId + 2].normal);

            triangle->ua = mesh->vertices[vertexId + 0].uv;
            triangle->ub = mesh->vertices[vertexId + 1].uv;
            triangle->uc = mesh->vertices[vertexId + 2].uv;

            triangle->parent = triangleMesh;
        }
    }
}

#ifdef GPU_PATH_TRACE
static void copyAlignV2(cl_float2* dst, vec2* src)
{
    for (u32 i = 0; i < 2; i++)
    {
        (*dst).s[i] = (*src)[i];
    }
}

static void copyAlignV3(cl_float3* dst, vec3* src)
{
    for (u32 i = 0; i < 3; i++)
    {
        (*dst).s[i] = (*src)[i];
    }
}

static void alignSceneForCompute(Scene* scene)
{
    u32 triangleBufferSize = scene->triangleCount * sizeof(GPUTriangle);
    u32 materialBufferSize = scene->materialCount * sizeof(GPUMaterial);
    u32 bvhNodeBufferSize = scene->bvhNodeCount * sizeof(GPUBVHNode);

    scene->alignedTrianglesSize = triangleBufferSize;
    scene->alignedMaterialsSize = materialBufferSize;
    scene->alignedBvhNodesSize = bvhNodeBufferSize;

    scene->alignedTriangles = (GPUTriangle*) malloc(triangleBufferSize);
    scene->alignedMaterials = (GPUMaterial*) malloc(materialBufferSize);
    scene->alignedBvhNodes = (GPUBVHNode*) malloc(bvhNodeBufferSize);

    for (u32 triangleId = 0; triangleId < scene->triangleCount; triangleId++)
    {
        Triangle* triangle = scene->triangles + triangleId;
        GPUTriangle* alignedTriangle = scene->alignedTriangles + triangleId;

        copyAlignV3(&alignedTriangle->pa, &triangle->pa);
        copyAlignV3(&alignedTriangle->pb, &triangle->pb);
        copyAlignV3(&alignedTriangle->pc, &triangle->pc);

        copyAlignV3(&alignedTriangle->na, &triangle->na);
        copyAlignV3(&alignedTriangle->nb, &triangle->nb);
        copyAlignV3(&alignedTriangle->nc, &triangle->nc);

        copyAlignV2(&alignedTriangle->ua, &triangle->ua);
        copyAlignV2(&alignedTriangle->ub, &triangle->ub);
        copyAlignV2(&alignedTriangle->uc, &triangle->uc);

        alignedTriangle->materialId = 0;
        alignedTriangle->materialId = triangle->parent->materialId;
    }

    for (u32 materialId = 0; materialId < scene->materialCount; materialId++)
    {
        Material* material = scene->materials + materialId;
        GPUMaterial* alignedMaterial = scene->alignedMaterials + materialId;

        copyAlignV3(&alignedMaterial->color, &material->color);

        alignedMaterial->textureId = material->textureId;
        alignedMaterial->surfaceType = material->surfaceType;
        alignedMaterial->textureType = material->textureType;
        alignedMaterial->roughness = material->roughness;
        alignedMaterial->emission = material->emission;
        alignedMaterial->ior = material->ior;
    }

    for (u32 bvhNodeId = 0; bvhNodeId < scene->bvhNodeCount; bvhNodeId++)
    {
        LinearBVHNode* bvhNode = scene->bvhNodes + bvhNodeId;
        GPUBVHNode* alignedBvhNode = scene->alignedBvhNodes + bvhNodeId;

        copyAlignV3(&alignedBvhNode->bounds.corners[0], &bvhNode->bounds.min);
        copyAlignV3(&alignedBvhNode->bounds.corners[1], &bvhNode->bounds.max);

        alignedBvhNode->offset = bvhNode->primitivesOffset;
        alignedBvhNode->primitiveCount = bvhNode->primitiveCount;
        alignedBvhNode->axis = bvhNode->axis;
    }
}

static void updateMaterialsForCompute(Scene* scene)
{
    for (u32 materialId = 0; materialId < scene->materialCount; materialId++)
    {
        Material* material = scene->materials + materialId;
        GPUMaterial* alignedMaterial = scene->alignedMaterials + materialId;

        copyAlignV3(&alignedMaterial->color, &material->color);

        alignedMaterial->textureId = material->textureId;
        alignedMaterial->surfaceType = material->surfaceType;
        alignedMaterial->textureType = material->textureType;
        alignedMaterial->roughness = material->roughness;
        alignedMaterial->emission = material->emission;
        alignedMaterial->ior = material->ior;
    }
}
#endif

static void bboxScene(Scene* scene)
{
    scene->bboxes = (BBox*) calloc(scene->triangleCount, sizeof(BBox));
    scene->bboxCount = scene->triangleCount;

    for (u32 triangleId = 0, bboxId = 0; triangleId < scene->triangleCount; triangleId++)
    {
        BBox* bbox = scene->bboxes + bboxId;

        Triangle* triangle = scene->triangles + triangleId;

        bbox->min = bbox->max = triangle->pa;

        *bbox = unionBBox(bbox, triangle->pb);
        *bbox = unionBBox(bbox, triangle->pc);

        bboxId++;
    }
}

static void freeScene(Scene* scene)
{
    for (u32 meshId = 0; meshId < scene->meshCount; meshId++)
    {
        freeMesh(&scene->meshes[meshId]);
    }

    free(scene->meshes);
    free(scene->triangleMeshes);
    free(scene->renderOrderBuffer);
    free(scene->triangles);
    free(scene->materials);
    free(scene->bboxes);
    free(scene->textures);
    free(scene->textureBuffer);
    free(scene->skyTextureBuffer);
    free(scene->bvhNodes);
    free(scene->materialNamesStringified);
    free(scene->textureNamesStringified);

#ifdef GPU_PATH_TRACE
    free(scene->alignedTriangles);
    free(scene->alignedMaterials);
    free(scene->alignedBvhNodes);
#endif
}

static void buildBVH(Scene* scene)
{
    // NOTE(vinht): Gather every triangle, copy their bounding boxes and calculate centroids
    BVHPrimitiveInfo* buildData;
    buildData = (BVHPrimitiveInfo*) malloc(scene->triangleCount * sizeof(BVHPrimitiveInfo));

    for (u32 triangleId = 0; triangleId < scene->triangleCount; triangleId++)
    {
        BVHPrimitiveInfo* data = buildData + triangleId;
        initBHVPrimitiveInfo(data, triangleId, scene->bboxes[triangleId]);
    }

    // NOTE(vinht): Since predicted number nodes in entire BVH will be 2n - 1 at most,
    // we can pre-allocate all memory we need for BVHBuildNodes.
    BVHBuildNode* buildNodes = (BVHBuildNode*) calloc(scene->triangleCount * 2 - 1, sizeof(BVHBuildNode));
    u32 buildNodeIndex = 0;

    // NOTE(vinht): This array gets filled correct primitive ordering for linear traversal
    u32* orderedPrimitives;
    orderedPrimitives = (u32*) malloc(scene->triangleCount * sizeof(u32));
    u32 orderedPrimitivesIndex = 0;

    BVHBuildNode* root;
    root = recursiveBuildBVH(buildNodes, &buildNodeIndex,
                             buildData, 0, scene->triangleCount,
                             orderedPrimitives, &orderedPrimitivesIndex);
    free(buildData);

    // NOTE(vinht): Re-order our original triangles
    Triangle* tempTriangleArray = (Triangle*) malloc(scene->triangleCount * sizeof(Triangle));

    for (u32 triangleId = 0; triangleId < scene->triangleCount; triangleId++)
    {
        tempTriangleArray[triangleId] = scene->triangles[orderedPrimitives[triangleId]];
    }

    memcpy(scene->triangles, tempTriangleArray, scene->triangleCount * sizeof(Triangle));

    free(tempTriangleArray);
    free(orderedPrimitives);

    // NOTE(vinht): Now we will flatten pointer version of BVH into pointerless, linear
    // and more compact version of BVH.
    scene->bvhNodes = (LinearBVHNode*) calloc(buildNodeIndex, sizeof(LinearBVHNode));

    u32 flattenOffset = 0;
    flattenBVHTree(root, scene->bvhNodes, &flattenOffset);

    scene->bvhNodeCount = buildNodeIndex;

    free(buildNodes);
}

struct RayHit
{
    vec3 position;
    vec3 normal;
    vec2 uv;
    u32 materialId;
};

static bool badIntersect(Ray* ray, Scene* scene, RayHit* outHit = nullptr)
{
    // TODO(vinht): Better way to distinguish between geometric objects?
    Triangle* closestTriangle = nullptr;

    float minDistance = infinity;
    for (u32 triangleIndex = 0; triangleIndex < scene->triangleCount; triangleIndex++)
    {
        Triangle* triangle = scene->triangles + triangleIndex;

        float tHit;
        vec2 tUV;
        vec3 tN;

        if (intersectTriangle(ray, triangle, &tHit, &tUV, &tN))
        {
            if (tHit < minDistance)
            {
                closestTriangle = triangle;
                minDistance = tHit;
            }
        }
    }

    if (closestTriangle != nullptr)
    {
        if (outHit != nullptr)
        {
            outHit->position = ray->origin + minDistance * ray->direction;

            if (closestTriangle)
            {
                outHit->normal = closestTriangle->na;
                outHit->materialId = closestTriangle->parent->materialId;
            }
        }

        return true;
    }

    return false;
}

static bool bvhIntersect(Ray* ray, Scene* scene, RayHit* outHit = nullptr)
{
    vec3 invDir = 1.0f / ray->direction;
    u32 dirIsNeg[3] = { invDir.x < 0.0f, invDir.y < 0.0f, invDir.z < 0.0f };

    u32 nodeIndex = 0;
    u32 todoOffset = 0;
    u32 todo[64];

    float minDistance = infinity;
    Triangle* closestTriangle = nullptr;
    vec2 closestUV;
    vec3 closestNormal;
    bool hit = false;

    while (true)
    {
        LinearBVHNode* node = scene->bvhNodes + nodeIndex;

        if (intersectBBox(node->bounds, ray, invDir, dirIsNeg))
        {
            if (node->primitiveCount > 0)
            {
                for (u32 primitiveIndex = 0;
                     primitiveIndex < node->primitiveCount;
                     primitiveIndex++)
                {
                    Triangle* triangle = &scene->triangles[node->primitivesOffset + primitiveIndex];

                    float tHit;
                    vec2 tUV;
                    vec3 tN;

                    if (intersectTriangle(ray, triangle, &tHit, &tUV, &tN))
                    {
                        hit = true;

                        if (tHit < minDistance)
                        {
                            minDistance = tHit;
                            closestTriangle = triangle;
                            closestUV = tUV;
                            closestNormal = tN;
                        }
                    }
                }

                if (todoOffset == 0)
                {
                    break;
                }

                nodeIndex = todo[--todoOffset];
            }
            else
            {
                if (dirIsNeg[node->axis])
                {
                    todo[todoOffset++] = nodeIndex + 1;
                    nodeIndex = node->secondChildOffset;
                }
                else
                {
                    todo[todoOffset++] = node->secondChildOffset;
                    nodeIndex = nodeIndex + 1;
                }
            }
        }
        else
        {
            if (todoOffset == 0)
            {
                break;
            }

            nodeIndex = todo[--todoOffset];
        }
    }

    if (hit && closestTriangle && outHit)
    {
        outHit->position = ray->origin + minDistance * ray->direction;
        outHit->normal = closestNormal;
        outHit->materialId = closestTriangle->parent->materialId;
        outHit->uv = closestUV;
    }

    return hit;
}

static void sortMeshesForRendering(Scene* scene, Camera* camera)
{
    u32* tempBuffer = (u32*) calloc(scene->meshCount, sizeof(u32));

    vec3 c = camera->origin;

    // NOTE(vinht): Bottom-up merge sort implementation.
    // Used wikipedia's implementation for reference.

    // NOTE(vinht): Sort meshes back to front from cameras perpective

    for (u32 width = 1; width < scene->meshCount; width *= 2)
    {
        for (u32 left = 0; left < scene->meshCount; left += 2 * width)
        {
            u32 right = glm::min(left + width, scene->meshCount);
            u32 end = glm::min(left + 2 * width, scene->meshCount);

            u32 leftHead = left;
            u32 rightHead = right;

            for (u32 element = left; element < end; element++)
            {
                u32 selected = 0;

                if (leftHead < right)
                {
                    if (rightHead >= end)
                    {
                        selected = scene->renderOrderBuffer[leftHead++];
                    }
                    else
                    {
                        u32 aIndex = scene->renderOrderBuffer[leftHead];
                        u32 bIndex = scene->renderOrderBuffer[rightHead];

                        Mesh* aMesh = &scene->meshes[aIndex];
                        Mesh* bMesh = &scene->meshes[bIndex];

                        vec3 a = aMesh->centroid;
                        vec3 b = bMesh->centroid;

                        float aDistToCam = glm::distance(a, c);
                        float bDistToCam = glm::distance(b, c);

                        if (bDistToCam <= aDistToCam)
                        {
                            selected = scene->renderOrderBuffer[leftHead++];
                        }
                        else
                        {
                            selected = scene->renderOrderBuffer[rightHead++];
                        }
                    }
                }
                else
                {
                    selected = scene->renderOrderBuffer[rightHead++];
                }

                tempBuffer[element] = selected;
            }
        }

        memcpy(scene->renderOrderBuffer, tempBuffer, sizeof(u32) * scene->meshCount);
    }

    // NOTE(vinht): Place opaque objects first and transparent object last in the array

    u32 offset = 0;

    for (u32 index = 0; index < scene->meshCount; index++)
    {
        u32 meshIndex = scene->renderOrderBuffer[index];
        TriangleMesh* triangleMesh = scene->triangleMeshes + meshIndex;
        Material* material = scene->materials + triangleMesh->materialId;

        if (material->surfaceType != SURFACE_GLASS)
        {
            tempBuffer[offset++] = meshIndex;
        }
    }

    for (u32 index = 0; index < scene->meshCount; index++)
    {
        u32 meshIndex = scene->renderOrderBuffer[index];
        TriangleMesh* triangleMesh = scene->triangleMeshes + meshIndex;
        Material* material = scene->materials + triangleMesh->materialId;

        if (material->surfaceType == SURFACE_GLASS)
        {
            tempBuffer[offset++] = meshIndex;
        }
    }

    memcpy(scene->renderOrderBuffer, tempBuffer, sizeof(u32) * scene->meshCount);

    free(tempBuffer);
}
