/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "vertex.h"
#include "mesh.h"
#include "hashmap.h"

static bool readOBJMTLName(const char* inObjPath, char* outMtlPath)
{
    char buffer[512];

    FILE* f = fopen(inObjPath, "r");

    assert(f);

    bool foundMtlFile = false;

    while (fgets(buffer, sizeof(buffer), f))
    {
        if (buffer[0] == '#')
        {
            continue;
        }

        if (strstr(buffer, "mtllib"))
        {
            sscanf(buffer, "mtllib %511s", outMtlPath);

            foundMtlFile = true;
            break;
        }
    }

    fclose(f);

    return foundMtlFile;
}

static u32 getOBJMeshCount(const char* path)
{
    char buffer[512];

    FILE* f = fopen(path, "r");

    assert(f);

    u32 meshCount = 0;

    while (fgets(buffer, sizeof(buffer), f))
    {
        if (buffer[0] == '#')
        {
            continue;
        }

        if (strstr(buffer, "usemtl"))
        {
            meshCount++;
        }
    }

    fclose(f);

    return meshCount;
}

static u32 getOBJMaterialCount(const char* path)
{
    char buffer[512];

    FILE* f = fopen(path, "r");

    assert(f);

    fgets(buffer, sizeof(buffer), f);
    fgets(buffer, sizeof(buffer), f);

    u32 materialCount;
    sscanf(buffer, "# Material Count: %u", &materialCount);

    fclose(f);

    return materialCount;
}

static void readOBJMaterials(const char* path, Material* materialArrays, HashMap* materialMap, HashMap* textureMap)
{
    char buffer[512];

    FILE* f = fopen(path, "r");

    assert(f);

    int materialIndex = 0;
    int materialsLoaded = 0;

    int textureIndex = 0;
    int texturesLoaded = 0;

    while (fgets(buffer, sizeof(buffer), f))
    {
        // NOTE(vinht): line beginning # is a comment
        if (buffer[0] == '#')
        {
            continue;
        }

        if (strstr(buffer, "map_Kd"))
        {
            assert(materialIndex > 0);
            Material* currentMaterial = materialArrays + materialIndex - 1;

            char textureName[512];
            sscanf(buffer, "map_Kd %511s", textureName);

            u32 existingTextureIndex;

            if (hashMapLookup(textureMap, textureName, &existingTextureIndex))
            {
                currentMaterial->textureId = existingTextureIndex;
            }
            else
            {
                hashMapInsert(textureMap, textureName, textureIndex);
                currentMaterial->textureId = textureIndex;
                textureIndex++;
                texturesLoaded++;
            }
        }
        else if (strstr(buffer, "Kd"))
        {
            assert(materialIndex > 0);
            Material* currentMaterial = materialArrays + materialIndex - 1;

            sscanf(buffer, "Kd %f %f %f",
                   &currentMaterial->color.r,
                   &currentMaterial->color.g,
                   &currentMaterial->color.b);
        }
        else if (strstr(buffer, "newmtl"))
        {
            char materialName[512];
            sscanf(buffer, "newmtl %511s", materialName);

            hashMapInsert(materialMap, materialName, materialIndex);
            materialIndex++;

            Material* currentMaterial = materialArrays + materialIndex - 1;

            // TODO(vinht): This is getting increasingly stupid. Need better 3d object format.

            currentMaterial->textureType = TEXTURE_FLAT_COLOR;

            currentMaterial->surfaceType = SURFACE_DIFFUSE;
            currentMaterial->roughness = 0.0f;
            currentMaterial->emission = 1.0f;
            currentMaterial->ior = 1.5f;

            switch (materialName[0])
            {
                case '+':
                    currentMaterial->surfaceType = SURFACE_DIFFUSE;
                    currentMaterial->roughness = 0.1f;
                    break;
                case '&':
                    currentMaterial->surfaceType = SURFACE_GLOSSY;
                    currentMaterial->roughness = 0.0f;
                    break;
                case '"':
                    currentMaterial->surfaceType = SURFACE_GLOSSY;
                    currentMaterial->roughness = 0.1f;
                    break;
                case '$':
                    currentMaterial->surfaceType = SURFACE_GLASS;
                    currentMaterial->roughness = 0.0f;
                    break;
                case '?':
                    currentMaterial->surfaceType = SURFACE_GLASS;
                    currentMaterial->roughness = 0.1f;
                    break;
                case '#':
                    currentMaterial->surfaceType = SURFACE_EMISSION;
                    currentMaterial->emission = 17.0f;
                    break;
                case '!':
                    currentMaterial->textureType = TEXTURE_IMAGE;
                    break;
                case '%':
                    currentMaterial->textureType = TEXTURE_CHECKERBOARD;
                    break;
            }

            materialsLoaded++;
        }
    }

    fclose(f);

    printf("Load %s\n", path);
    printf("  %d materials\n", materialsLoaded);
    printf("  %d textures\n", texturesLoaded);
}

static void initializeMesh(Mesh* mesh, std::vector<Vertex>* vertices)
{
    mesh->vertexCount = vertices->size();
    u32 verticesSize = mesh->vertexCount * sizeof(Vertex);
    mesh->vertices = (Vertex*) malloc(verticesSize);
    memcpy(mesh->vertices, &(*vertices)[0], verticesSize);

    glGenVertexArrays(1, &mesh->vao);
    glBindVertexArray(mesh->vao);

    glGenBuffers(1, &mesh->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo);
    glBufferData(GL_ARRAY_BUFFER, mesh->vertexCount * sizeof(Vertex), mesh->vertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (sizeof(vec3)));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) (2 * sizeof(vec3)));
}

struct TriangleIndices
{
    u32 positionIndices[3];
    u32 uvIndices[3];
};

static vec3 addV3IfWontProduceZero(vec3 a, vec3 b)
{
    vec3 result = a + b;

    if (glm::length(result) == 0.0f)
        return a;

    return result;
}

static void buildMeshVertices(std::vector<TriangleIndices>* triangleIndices,
                              std::vector<vec3>* tentativePositions,
                              std::vector<vec2>* tentativeUVs,
                              bool smoothing,
                              std::vector<Vertex>* outVertices)
{
    // TODO(vinht): Bad robustness, inputs can easily lead to NaNs.

    std::vector<vec3> tentativeNormals;
    tentativeNormals.resize(tentativePositions->size());

    if (smoothing)
    {
        for (size_t i = 0; i < triangleIndices->size(); i++)
        {
            TriangleIndices triangle = (*triangleIndices)[i];

            int a = triangle.positionIndices[0];
            int b = triangle.positionIndices[1];
            int c = triangle.positionIndices[2];

            vec3 pa = (*tentativePositions)[a];
            vec3 pb = (*tentativePositions)[b];
            vec3 pc = (*tentativePositions)[c];

            vec3 pba = pb - pa;
            vec3 pca = pc - pa;

            vec3 n = glm::normalize(glm::cross(pba, pca));

            tentativeNormals[a] = addV3IfWontProduceZero(tentativeNormals[a], n);
            tentativeNormals[b] = addV3IfWontProduceZero(tentativeNormals[b], n);
            tentativeNormals[c] = addV3IfWontProduceZero(tentativeNormals[c], n);
        }

        for (size_t i = 0; i < tentativeNormals.size(); i++)
        {
            tentativeNormals[i] = glm::normalize(tentativeNormals[i]);
        }

        for (size_t i = 0; i < triangleIndices->size(); i++)
        {
            TriangleIndices triangle = (*triangleIndices)[i];

            for (u32 vertexId = 0; vertexId < 3; vertexId++)
            {
                u32 positionIndex = triangle.positionIndices[vertexId];

                Vertex vertex;

                vertex.position = (*tentativePositions)[positionIndex];
                vertex.normal = tentativeNormals[positionIndex];

                if (tentativeUVs->size() > 0)
                {
                    u32 uvIndex = triangle.uvIndices[vertexId];

                    vertex.uv = (*tentativeUVs)[uvIndex];
                }

                outVertices->push_back(vertex);
            }
        }
    }
    else
    {
        for (const auto& triangle : *triangleIndices)
        {
            vec3 positions[3];
            positions[0] = (*tentativePositions)[triangle.positionIndices[0]];
            positions[1] = (*tentativePositions)[triangle.positionIndices[1]];
            positions[2] = (*tentativePositions)[triangle.positionIndices[2]];

            vec3 pba = positions[1] - positions[0];
            vec3 pca = positions[2] - positions[0];

            vec3 n = glm::normalize(glm::cross(pba, pca));

            for (u32 vertexIndex = 0; vertexIndex < 3; vertexIndex++)
            {
                Vertex vertex;

                vertex.position = positions[vertexIndex];
                vertex.normal = n;

                if (tentativeUVs->size() > 0)
                {
                    vertex.uv = (*tentativeUVs)[triangle.uvIndices[vertexIndex]];
                }

                outVertices->push_back(vertex);
            }
        }
    }
}

static void readOBJFile(const char* path, Mesh* meshArray, HashMap* materialMap)
{
    char buffer[512];

    FILE* f = fopen(path, "r");

    // NOTE(vinht): Treat missing files as fatal errors for now.
    assert(f);

    std::vector<vec3> tentativePositions;
    std::vector<vec2> tentativeUVs;
    std::vector<Vertex> tentativeVertices;

    Mesh* currentMesh = meshArray;
    currentMesh->bbox = BBox();

    char lastChar = ' ';

    u32 positionsRead = 0;
    u32 uvsRead = 0;
    u32 facesRead = 0;

    bool smoothingEnabled = false;

    std::vector<TriangleIndices> triangleIndices;

    while (fgets(buffer, sizeof(buffer), f))
    {
        // NOTE(vinht): line beginning # is a comment
        if (buffer[0] == '#')
        {
            continue;
        }

        if ((lastChar == 'f' && buffer[0] != 'f'))
        {
            buildMeshVertices(&triangleIndices, &tentativePositions,
                              &tentativeUVs, smoothingEnabled,
                              &tentativeVertices);
            initializeMesh(currentMesh, &tentativeVertices);

            positionsRead += tentativePositions.size();
            uvsRead += tentativeUVs.size();

            tentativePositions.clear();
            tentativeUVs.clear();
            tentativeVertices.clear();
            triangleIndices.clear();

            smoothingEnabled = false;

            currentMesh++;
            currentMesh->bbox = BBox();
        }

        lastChar = buffer[0];

        // NOTE(vinht): v = vertex position, vn = vertex normal, f = triangle face
        if (buffer[0] == 'v' && buffer[1] == 'n')
        {
            // NOTE(vinht): No need to import normals at all.
            // We still need this case such that we don't fall through
            // to the third case, where it would pass incorrectly.
        }
        else if (buffer[0] == 'v' && buffer[1] == 't')
        {
            vec2 uv;
            sscanf(buffer, "vt %f %f", &uv.x, &uv.y);

            tentativeUVs.push_back(uv);
        }
        else if (buffer[0] == 'v')
        {
            vec3 position;
            sscanf(buffer, "v %f %f %f", &position.x, &position.y, &position.z);

            currentMesh->bbox = unionBBox(&currentMesh->bbox, position);

            tentativePositions.push_back(position);
        }
        else if (buffer[0] == 'f')
        {
            TriangleIndices triIndex;

            // NOTE(vinht): Simplify format for sscanf.
            char* c = buffer;

            while (*c != '\0')
            {
                if (*c == '/')
                {
                    *c = ' ';
                }

                c++;
            }

            if (tentativeUVs.size() > 0)
            {
                sscanf(buffer, "f %u %u %u %u %u %u",
                       &triIndex.positionIndices[0], &triIndex.uvIndices[0],
                       &triIndex.positionIndices[1], &triIndex.uvIndices[1],
                       &triIndex.positionIndices[2], &triIndex.uvIndices[2]);
            }
            else
            {
                sscanf(buffer, "f %u %u %u",
                       &triIndex.positionIndices[0],
                       &triIndex.positionIndices[1],
                       &triIndex.positionIndices[2]);
            }

            for (u32 vertexIndex = 0; vertexIndex < 3; vertexIndex++)
            {
                triIndex.positionIndices[vertexIndex] =
                    triIndex.positionIndices[vertexIndex] - positionsRead - 1;
                triIndex.uvIndices[vertexIndex] =
                    triIndex.uvIndices[vertexIndex] - uvsRead - 1;
            }

            triangleIndices.push_back(triIndex);

            facesRead++;
        }
        else if (strstr(buffer, "usemtl"))
        {
            char materialName[512];
            sscanf(buffer, "usemtl %511s", materialName);

            u32 materialId;

            if (hashMapLookup(materialMap, materialName, &materialId))
            {
                currentMesh->materialId = materialId;
            }
        }
        else if (buffer[0] == 's')
        {
            char smoothing[8];
            sscanf(buffer, "s %7s", smoothing);

            if (strcmp(smoothing, "1") == 0)
            {
                smoothingEnabled = true;
            }
        }
    }

    if (triangleIndices.size() > 0)
    {
        buildMeshVertices(&triangleIndices, &tentativePositions,
                          &tentativeUVs, smoothingEnabled,
                          &tentativeVertices);
        initializeMesh(currentMesh, &tentativeVertices);
    }

    fclose(f);

    printf("Load %s\n", path);
    printf("  %u positions\n", positionsRead);
    printf("  %u uvs\n", uvsRead);
    printf("  %u faces\n", facesRead);
}
