/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "includes.h"
#include "random.h"
#include "screen.h"
#include "shaders.h"
#include "screenquad.h"
#include "vertex.h"
#include "ray.h"
#include "sphere.h"
#include "triangle.h"
#include "camera.h"
#include "bbox.h"
#include "scene.h"
#include "sampling.h"
#include "core.h"
#include "debug.h"
#include "gui.h"
#include "utility.h"

#ifdef CPU_PATH_TRACE
    #include "cpu_pathtracer.h"
#endif

#ifdef GPU_PATH_TRACE
    #include "compute.h"
    #include "gpu_pathtracer.h"
#endif

static void validateScreenResolution(Screen* screen, char* widthBuffer, char* heightBuffer)
{
    const int defaultScreenWidth = 1024;
    const int defaultScreenHeight = 576;
    const int screenSizeMultiple = 16;

    int screenWidth = atoi(widthBuffer);
    int screenHeight = atoi(heightBuffer);

    if (screenWidth > 0 && screenHeight > 0)
    {
        int dWidth = screenWidth % screenSizeMultiple;
        int dHeight = screenHeight % screenSizeMultiple;

        screenWidth -= dWidth;
        screenHeight -= dHeight;

        if (dWidth > 0 || dHeight > 0)
        {
            printf("Warning: screen dimensions are not multiple of 16. Rounding up.\n");

            if (dWidth > 0)
            {
                screenWidth += screenSizeMultiple;
            }

            if (dHeight > 0)
            {
                screenHeight += screenSizeMultiple;
            }
        }
    }
    else
    {
        printf("Warning: one of screen dimensions is missing. Using default resolution.\n");

        screenWidth = defaultScreenWidth;
        screenHeight = defaultScreenHeight;
    }

    printf("Screen resolution: %d x %d, %d pixels\n", screenWidth, screenHeight, screenWidth * screenHeight);

    screen->width = screenWidth;
    screen->height = screenHeight;
    screen->aspectRatio = (float) screen->width / (float) screen->height;
}

static void validateSamplesPerPixel(int* samplesPerPixel, char* sppBuffer)
{
    const int defaultSamplesPerPixel = 16;

    *samplesPerPixel = atoi(sppBuffer);

    if (*samplesPerPixel < 4)
    {
        *samplesPerPixel = defaultSamplesPerPixel;
    }

    printf("Samples per pixel: %d\n", *samplesPerPixel);
}

static bool parseCommandLineArgs(Core* core, int argc, char** argv)
{
    char widthBuffer[32] = {};
    char heightBuffer[32] = {};
    char sppBuffer[32] = {};
    char sceneFileNameBuffer[512] = {};

    char currentMode = 0;
    int argIndex = 1;

    for (; argIndex < argc; argIndex++)
    {
        if (currentMode == 0)
        {
            if (strlen(argv[argIndex]) != 2)
            {
                goto error_invalid_argument;
            }

            if (argv[argIndex][0] != '-')
            {
                goto error_invalid_argument;
            }
            else
            {
                currentMode = argv[argIndex][1];
            }
        }
        else
        {
            switch (currentMode)
            {
                case 'w':
                    strcpy(widthBuffer, argv[argIndex]);
                    break;
                case 'h':
                    strcpy(heightBuffer, argv[argIndex]);
                    break;
                case 's':
                    strcpy(sppBuffer, argv[argIndex]);
                    break;
                case 'f':
                    strcpy(sceneFileNameBuffer, argv[argIndex]);
                    break;
                default:
                    goto error_invalid_mode;
            }

            currentMode = 0;
        }
    }

    validateScreenResolution(&core->screen, widthBuffer, heightBuffer);
    validateSamplesPerPixel(&core->samplesPerPixel, sppBuffer);

    if (strlen(sceneFileNameBuffer) == 0)
    {
        goto error_missing_scene;
    }
    else if (!checkFileExistence(sceneFileNameBuffer))
    {
        goto error_missing_scene;
    }
    else
    {
        strcpy(core->scene.filePath, sceneFileNameBuffer);
    }

    goto success;

error_missing_scene:
    printf("Error: Missing scene file.\n");
    return false;

error_invalid_argument:
    printf("Error: Invalid argument: %s\n", argv[argIndex]);
    return false;

error_invalid_mode:
    printf("Error: Invalid mode: %c\n", currentMode);
    return false;

success:
    return true;
}

static void mouseButtonCallback(Core* core, SDL_Event* event)
{
    Camera* camera = &core->camera;
    CameraControls* cameraControls = &core->cameraControls;
    GUI* gui = &core->gui;
    Screen* screen = &core->screen;
    Scene* scene = &core->scene;

    vec2 cursorPosition = getMousePosition();

    if (!gui->anyWindowHovered)
    {
        if (event->type == SDL_MOUSEBUTTONDOWN)
        {
            if (event->button.button == SDL_BUTTON_LEFT)
            {
                cameraControls->mouseStart = cursorPosition;
                cameraControls->mouseDelta = cursorPosition;

                cameraControls->beginTracking = true;
            }
            else if (event->button.button == SDL_BUTTON_RIGHT)
            {
                Ray ray = screenToCameraRay((int)cursorPosition.x,
                                            (int)cursorPosition.y,
                                            camera,
                                            screen);

                RayHit hit;

                if (bvhIntersect(&ray, scene, &hit))
                {
                    selectMaterialGUI(gui, scene->materials + hit.materialId, hit.materialId);
                }
            }
        }
    }

    if (event->button.button == SDL_BUTTON_LEFT)
    {
        if (event->type == SDL_MOUSEBUTTONUP)
        {
            cameraControls->mouseStart = cursorPosition;

            camera->theta += camera->dTheta;
            camera->dTheta = 0.0f;

            camera->phi += camera->dPhi;
            camera->dPhi = 0.0f;

            cameraControls->beginTracking = false;
        }
    }

    if (event->type == SDL_MOUSEBUTTONDOWN)
    {
        if (event->button.button == SDL_BUTTON_LEFT)
        {
            gui->mousePressed[0] = true;
        }
        else if (event->button.button == SDL_BUTTON_RIGHT)
        {
            gui->mousePressed[1] = true;
        }
    }
}


static bool updateDebugGUI(Core* core)
{
    DebugSuite* ds = &core->debugSuite;
    Screen* screen = &core->screen;
    Camera* camera = &core->camera;
    Scene* scene = &core->scene;
    u8* pixelBuffer = core->pixelBuffer;

    bool userInteracted = false;

    if (ImGui::Checkbox("gamma correction", &core->gammaCorrection))
    {
        userInteracted = true;
    }

    if (ImGui::Checkbox("show fullscreen quad", &core->showScreenQuad))
    {
        userInteracted = true;
    }

    if (ImGui::Checkbox("visualize primary rays", &ds->visibilityTest.render))
    {
        runVisibilityTest(&ds->visibilityTest, screen, camera, scene);
        userInteracted = true;
    }

    if (ImGui::Checkbox("visualize ray bounce", &ds->bounceTest.render))
    {
        runBounceTest(&ds->bounceTest, screen, camera, scene);
        userInteracted = true;
    }

    if (ImGui::Checkbox("visualize hemisphere sampling", &ds->hemisphereSamplingTest.render))
    {
        runHemisphereSamplingTest(&ds->hemisphereSamplingTest, screen, camera, scene);
        userInteracted = true;
    }

    if (ImGui::Checkbox("visualize bbox", &ds->bboxTest.render))
    {
        runBBoxTest(&ds->bboxTest, scene);
        userInteracted = true;
    }

    if (ImGui::Checkbox("visualize bvh", &ds->bvhTest.render))
    {
        runBVHTest(&ds->bvhTest, scene);
        userInteracted = true;
    }

    if (ImGui::Button("test partition algorithm"))
    {
        runPartitionTest();
        userInteracted = true;
    }

    if (ImGui::Button("test bvh accuracy"))
    {
        runRaySpamTest(scene);
        userInteracted = true;
    }

    if (ImGui::Button("test stratified sampling"))
    {
        runSamplingTest();
        userInteracted = true;
    }

    if (ImGui::Button("test screen partition algorithm"))
    {
        runScreenPartitionTest(screen->width, screen->height, 16);
        userInteracted = true;
    }

    if (ImGui::Button("test hash map"))
    {
        runHashMapTest();
        userInteracted = true;
    }

    if (ImGui::Button("test merge sort"))
    {
        runMergeSortTest();
        userInteracted = true;
    }

    if (ImGui::Button("calculate screen variance"))
    {
        runVarianceTest(pixelBuffer, screen);
        userInteracted = true;
    }

    if (ImGui::Button("print camera data"))
    {
        outputCameraData(camera);
        userInteracted = true;
    }

    if (ImGui::Button("dump bvh structure to csv"))
    {
        outputBVHFile(scene);
        userInteracted = true;
    }

    return userInteracted;
}

static bool updateGUI(GUI* gui, Core* core)
{
    MouseState ms = getMouseState();

    ImGuiIO& io = ImGui::GetIO();

    io.MousePos = ImVec2(ms.x, ms.y);
    io.MouseDown[0] = gui->mousePressed[0] || ms.button[0];
    io.MouseDown[1] = gui->mousePressed[1] || ms.button[1];
    io.MouseWheel = gui->mouseWheel;
    gui->mouseWheel = 0;

    gui->anyWindowHovered = ImGui::IsPosHoveringAnyWindow(ImVec2(ms.x, ms.y));

    ImGui::NewFrame();

    bool userInteracted = false;

    if (!ImGui::Begin("menu"))
    {
        ImGui::End();

        return userInteracted;
    }

    /////////////////////////////////////////////////////
    // NORMAL MODE GUI
    /////////////////////////////////////////////////////

    if (core->renderMode == RENDER_MODE_NORMAL)
    {
        if (core->renderState == RENDER_STATE_RUNNING)
        {
            if (ImGui::CollapsingHeader("rendering", NULL, true, true))
            {
                if (ImGui::Button("stop"))
                {
                    core->renderState = RENDER_STATE_HALTING;
                }

                updateRenderInfoGUI(&core->renderInfo);
            }
        }
        else
        {
            if (ImGui::CollapsingHeader("material editor", NULL, true, true))
            {
                userInteracted |= updateMaterialEditGUI(gui, &core->scene);
            }

            if (ImGui::CollapsingHeader("debug", NULL, true, false))
            {
                userInteracted |= updateDebugGUI(core);
            }

            if (ImGui::CollapsingHeader("rendering", NULL, true, true))
            {
                userInteracted |= updateLiveRenderingToggleGUI(&core->renderMode);

                ImGui::InputInt("samples", &core->samplesPerPixel);

                if (ImGui::Button("save"))
                {
                    saveScreenToDisk(&core->screen, core->pixelBuffer);
                }

                ImGui::SameLine();

                if (ImGui::Button("render"))
                {
                    core->renderState = RENDER_STATE_BEGINNING;
                }
            }
        }
    }

    /////////////////////////////////////////////////////
    // LIVE MODE GUI
    /////////////////////////////////////////////////////

    if (core->renderMode == RENDER_MODE_LIVE)
    {
        if (ImGui::CollapsingHeader("material editor", NULL, true, true))
        {
            userInteracted |= updateMaterialEditGUI(gui, &core->scene);
        }

        if (ImGui::CollapsingHeader("rendering", NULL, true, true))
        {
            userInteracted |= updateLiveRenderingToggleGUI(&core->renderMode);

            if (core->renderState == RENDER_STATE_RUNNING)
            {
                updateRenderInfoGUI(&core->renderInfo);
            }
        }
    }

    ImGui::End();

    return userInteracted;
}

int main(int argc, char** argv)
{
    printf("phoe_ray has been born\n");

    srand((unsigned) time(0));

    Core core;

    memset(&core, 0, sizeof(Core));

    if (!parseCommandLineArgs(&core, argc, argv))
    {
        printf("Failed to parse command line arguments\n");
        return 1;
    }
    else
    {
        printf("Successfully parsed command line arguments.\n");
    }

    core.camera.fov = glm::radians(45.0f);
    core.camera.tanFov = tanf(core.camera.fov / 2.0f);
    core.camera.origin = vec3(0.0f, 0.0f, 0.0f);
    core.camera.forward = vec3(0.0f, 0.0f, -1.0f);
    core.camera.up = vec3(0.0f, 1.0f, 0.0f);
    core.camera.right = vec3(1.0f, 0.0f, 0.0f);

    // Home
    //core.camera.origin = vec3(14.892697f, 5.396818f, -11.101116f);
    //core.camera.forward = vec3(-0.293467f, -0.313164f, 0.903219f);
    //core.camera.right = vec3(-0.903219f, 0.000000f, -0.293467f);
    //core.camera.up = vec3(-0.091904f, 0.901928f, 0.282856f);
    //core.camera.theta = 0.549999f;
    //core.camera.phi = 0.601389f;

    // Monkey
    core.camera.origin = vec3(-3.640316f, 2.741734f, 2.549792f);
    core.camera.forward = vec3(0.720267f, -0.445832f, -0.531459f);
    core.camera.right = vec3(0.531459f, -0.000000f, 0.720267f);
    core.camera.up = vec3(0.321118f, 0.801234f, -0.236941f);
    core.camera.theta = 0.148827f;
    core.camera.phi = 0.647092f;

    // Room
    //core.camera.origin = vec3(0.0f, 1.0f, 3.2f);
    //core.camera.forward = vec3(0.0f, 0.0f, -1.0f);
    //core.camera.right = vec3(1.0f, 0.0f, 0.0f);
    //core.camera.up = vec3(0.0f, 1.0f, 0.0f);
    //core.camera.theta = 0.0f;
    //core.camera.phi = 0.5;

    core.camera.projection = glm::perspective(core.camera.fov, core.screen.aspectRatio, 0.1f, 1000.0f);

    core.gammaCorrection = true;

    /////////////////////////////////////////////////////
    // OPENCL
    /////////////////////////////////////////////////////

#ifdef GPU_PATH_TRACE

    ComputeContext computeContext;

    if (!createComputeContext(&computeContext, true))
    {
        printf("Failed to create compute context.\n");
        return 1;
    }

#endif

    /////////////////////////////////////////////////////
    // SDL2
    /////////////////////////////////////////////////////

    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Failed SDL_Init: %s\n", SDL_GetError());
        return 1;
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

#ifdef GPU_PATH_TRACE
    const char* windowName = "phoe_ray - running on GPU";
#elif CPU_PATH_TRACE
    const char* windowName = "phoe_ray - running on CPU";
#endif

    core.window = SDL_CreateWindow(windowName,
                                   SDL_WINDOWPOS_CENTERED,
                                   SDL_WINDOWPOS_CENTERED,
                                   core.screen.width,
                                   core.screen.height,
                                   SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

    if (core.window == NULL)
    {
        SDL_Log("Failed SDL_CreateWindow: %s\n", SDL_GetError());
        return 1;
    }

    core.context = SDL_GL_CreateContext(core.window);

    if (core.context == NULL)
    {
        SDL_Log("Failed SDL_GL_CreateContext: %s\n", SDL_GetError());
        return 1;
    }

    SDL_GL_SetSwapInterval(1);

    /////////////////////////////////////////////////////
    // GLEW
    /////////////////////////////////////////////////////

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (err != GLEW_OK)
    {
        printf("Failed glewInit()\n");
        return 1;
    }

    printf("\nOpenGL Version: %s\n", glGetString(GL_VERSION));
    printf("GLSL Version: %s\n\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /////////////////////////////////////////////////////
    // SHADERS
    /////////////////////////////////////////////////////

    ScreenQuadShader screenQuadShader = createScreenQuadShader();
    BasicShader basicShader = createBasicShader();
    LineShader lineShader = createLineShader();

    /////////////////////////////////////////////////////
    // PIXEL BUFFER
    /////////////////////////////////////////////////////

    // NOTE(vinht): Each color component is 8-bits.
    core.pixelBufferSize = core.screen.width * core.screen.height * 4;
    core.pixelBuffer = (u8*) calloc(core.pixelBufferSize, sizeof(u8));

    core.screenQuad = createScreenQuad(core.screen.width, core.screen.height);

    /////////////////////////////////////////////////////
    // SCENE
    /////////////////////////////////////////////////////

    if (!buildScene(&core.scene, core.scene.filePath))
    {
        printf("Failed to load scene\n");
        return 1;
    }

    triangulateScene(&core.scene);
    bboxScene(&core.scene);
    buildBVH(&core.scene);

#ifdef GPU_PATH_TRACE

    alignSceneForCompute(&core.scene);

#endif

    /////////////////////////////////////////////////////
    // PATH TRACER PREPARATION
    /////////////////////////////////////////////////////

#ifdef GPU_PATH_TRACE

    GPUPathTracer gpuPathTracer;
    memset(&gpuPathTracer, 0, sizeof(gpuPathTracer));

    if (!createGPUPathTracerKernels(&gpuPathTracer, &computeContext))
    {
        printf("Failed to create gpu path tracer kernels\n");
        return 1;
    }

    initGPUPathTracer(&gpuPathTracer, &computeContext, &core);

#elif CPU_PATH_TRACE

    CPUPathTracer cpuPathTracer;
    memset(&cpuPathTracer, 0, sizeof(cpuPathTracer));

    initCPUPathTracer(&cpuPathTracer, &core);

#endif

    /////////////////////////////////////////////////////
    // DEBUG
    /////////////////////////////////////////////////////

    createDebugSuite(&core.debugSuite, &core.screen, &core.scene);

    /////////////////////////////////////////////////////
    // GUI
    /////////////////////////////////////////////////////

    createGUI(&core.gui, &core.screen);

    /////////////////////////////////////////////////////
    // MEMORY STATS
    /////////////////////////////////////////////////////

    {
        printf("\nMemory Stats\n");

        size_t pixelBufferSize = core.pixelBufferSize;

        size_t meshesSize = 0;

        for (u32 i = 0; i < core.scene.meshCount; i++)
        {
            Mesh* mesh = core.scene.meshes + i;
            meshesSize += sizeof(*mesh);
            meshesSize += sizeof(Vertex) * mesh->vertexCount;
        }

        size_t trianglesSize = sizeof(Triangle) * core.scene.triangleCount;

        size_t materialsSize = sizeof(Material) * core.scene.materialCount;

        size_t bboxesSize = sizeof(BBox) * core.scene.bboxCount;

        size_t textureBufferSize = core.scene.textureBufferSize + core.scene.skyTextureBufferSize;

        size_t bvhNodesSize = sizeof(LinearBVHNode) * core.scene.bvhNodeCount;

        size_t debugSize = 0;
        DebugSuite* ds = &core.debugSuite;
        debugSize += sizeof(*ds);
        debugSize += sizeof(LineVertex) * ds->bboxTest.aggregate.vertexCount;
        debugSize += sizeof(LineVertex) * ds->bounceTest.aggregate.vertexCount;
        debugSize += sizeof(LineVertex) * ds->bvhTest.aggregate.vertexCount;
        debugSize += sizeof(LineVertex) * ds->hemisphereSamplingTest.aggregate.vertexCount;
        debugSize += sizeof(LineVertex) * ds->visibilityTest.aggregate.vertexCount;

        printf("Pixel Buffer: %.2f KByte\n", pixelBufferSize / 1000.0f);
        printf("Meshes: %.2f KByte\n", meshesSize / 1000.0f);
        printf("Triangles: %.2f KByte\n", trianglesSize / 1000.0f);
        printf("Materials: %.2f KByte\n", materialsSize / 1000.0f);
        printf("BBoxes: %.2f KByte\n", bboxesSize / 1000.0f);
        printf("Texture Buffer: %.2f KByte\n", textureBufferSize / 1000.0f);
        printf("BVH: %.2f KByte\n", bvhNodesSize / 1000.0f);
        printf("Debug: %.2f KByte\n", debugSize / 1000.0f);

#ifdef CPU_PATH_TRACE

        size_t accumulatorBufferSize = threadCount * pixelBufferSize * sizeof(float);

        printf("CPU Accumulator Buffer(s) (%d threads): %.2f KByte\n", threadCount, accumulatorBufferSize / 1000.0f);

#endif

#ifdef GPU_PATH_TRACE

        size_t alignedSize = 0;

        alignedSize += core.scene.alignedTrianglesSize;
        alignedSize += core.scene.alignedMaterialsSize;
        alignedSize += core.scene.alignedBvhNodesSize;

        printf("GPU Aligned Data: %.2f KByte\n", alignedSize / 1000.0f);

        u32 pixelCount = core.screen.width * core.screen.height;

        size_t rayBufferSize = pixelCount * sizeof(GPURay);
        size_t seedBufferSize = pixelCount * sizeof(u32);
        size_t accumulatorBufferSize = pixelCount * 4 * sizeof(float);

        printf("GPU Ray Buffer: %.2f KByte\n", rayBufferSize / 1000.0f);
        printf("GPU Seed Buffer: %.2f KByte\n", seedBufferSize / 1000.0f);
        printf("GPU Pixel Buffer: %.2f KByte\n", pixelBufferSize / 1000.0f);
        printf("GPU Pixel Accumulator Buffer: %.2f KByte\n", accumulatorBufferSize / 1000.0f);
        printf("GPU Pixel Temporary Buffer: %.2f KByte\n", accumulatorBufferSize / 1000.0f);

#endif
    }

    /////////////////////////////////////////////////////
    // RENDER LOOP
    /////////////////////////////////////////////////////

    float renderStartTime = 0.0f;
    float sampleStartTime = 0.0f;

    float frameStartTime = getTime();
    float frameDeltaTime = 0.0f;

    float timeSinceLastRestart = 0.0f;
    bool shouldRestart = 0.0f;

    while (!core.stopRunning)
    {
        float frameCurrentTime = getTime();
        frameDeltaTime = frameCurrentTime - frameStartTime;
        frameStartTime = frameCurrentTime;
        timeSinceLastRestart += frameDeltaTime;

        prePollEventGUI(&core.gui);

        {
            ImGuiIO& io = ImGui::GetIO();
            SDL_Event event;
            GUI* gui = &core.gui;

            while (SDL_PollEvent(&event))
            {
                switch (event.type)
                {
                    case SDL_QUIT:
                    {
                        core.stopRunning = true;
                    } break;

                    case SDL_MOUSEWHEEL:
                    {
                        if (event.wheel.y > 0)
                            gui->mouseWheel = 1;
                        if (event.wheel.y < 0)
                            gui->mouseWheel = -1;
                    } break;

                    case SDL_MOUSEBUTTONUP:
                    case SDL_MOUSEBUTTONDOWN:
                    {
                        mouseButtonCallback(&core, &event);
                    } break;

                    case SDL_TEXTINPUT:
                    {
                        ImGuiIO& io = ImGui::GetIO();
                        io.AddInputCharactersUTF8(event.text.text);
                    } break;

                    case SDL_KEYDOWN:
                    case SDL_KEYUP:
                    {
                        int key = event.key.keysym.sym & ~SDLK_SCANCODE_MASK;
                        io.KeysDown[key] = (event.type == SDL_KEYDOWN);
                        io.KeyShift = ((SDL_GetModState() & KMOD_SHIFT) != 0);
                        io.KeyCtrl = ((SDL_GetModState() & KMOD_CTRL) != 0);
                        io.KeyAlt = ((SDL_GetModState() & KMOD_ALT) != 0);
                    } break;

                }
            }
        }

        /////////////////////////////////////////////////////
        // GUI
        /////////////////////////////////////////////////////

        if (updateGUI(&core.gui, &core))
        {
            if (core.renderMode == RENDER_MODE_NORMAL)
            {
                if (core.renderState != RENDER_STATE_IDLING)
                {
                    core.renderState = RENDER_STATE_HALTING;
                }
            }
            else if (core.renderMode == RENDER_MODE_LIVE)
            {
                shouldRestart = true;
            }
        }

        /////////////////////////////////////////////////////
        // CAMERA
        /////////////////////////////////////////////////////

        if (core.renderMode == RENDER_MODE_NORMAL && core.renderState == RENDER_STATE_IDLING)
        {
            if (updateCamera(&core.camera, &core.cameraControls, &core.screen))
            {
                core.showScreenQuad = false;
            }
        }
        else if (core.renderMode == RENDER_MODE_LIVE)
        {
            if (updateCamera(&core.camera, &core.cameraControls, &core.screen))
            {
                shouldRestart = true;
            }
        }

        if (core.renderMode == RENDER_MODE_LIVE && shouldRestart)
        {
            if (timeSinceLastRestart > 0.1f)
            {
                if (core.renderState == RENDER_STATE_IDLING)
                {
                    core.renderState = RENDER_STATE_BEGINNING;
                }
                else
                {
                    core.renderState = RENDER_STATE_RESTARTING;
                }

                timeSinceLastRestart = 0.0f;
                shouldRestart = false;
            }
        }

        /////////////////////////////////////////////////////
        // BEGIN TO LAUNCH RAYTRACER
        /////////////////////////////////////////////////////

        if (core.renderState == RENDER_STATE_BEGINNING)
        {
            if (core.renderMode == RENDER_MODE_NORMAL)
            {
                printf("Begin rendering.\n");

                u32 pixelBufferSize = core.pixelBufferSize;
                u8* pixelBuffer = core.pixelBuffer;
                Screen* screen = &core.screen;
                ScreenQuad* screenQuad = &core.screenQuad;

                memset(pixelBuffer, 0, pixelBufferSize);
                updateScreenQuad(screenQuad, screen->width, screen->height, pixelBuffer);
            }

            core.renderState = RENDER_STATE_RUNNING;
            core.showScreenQuad = true;

            renderStartTime = getTime();
            sampleStartTime = renderStartTime;

#ifdef GPU_PATH_TRACE

            updateMaterialsForCompute(&core.scene);
            prepareGPUPathTracer(&gpuPathTracer, &computeContext);

#elif CPU_PATH_TRACE

            prepareCPUPathTracer(&cpuPathTracer);

#endif

        }

        /////////////////////////////////////////////////////
        // RAYTRACING CHECKPOINT
        /////////////////////////////////////////////////////

        bool needToUpdateScreenQuad = false;
        bool renderingIsDone = false;

        if (core.renderState == RENDER_STATE_RUNNING)
        {

#ifdef GPU_PATH_TRACE

            std::unique_lock<std::mutex> jobQueueLock(*gpuPathTracer.mutex);

            if (gpuPathTracer.recentlyFinishedJobs > 0)
            {
                gpuPathTracer.recentlyFinishedJobs = 0;

                needToUpdateScreenQuad = true;

                float sampleTimeElapsed = getTime() - sampleStartTime;
                sampleStartTime = getTime();
                int samplesLeft = core.samplesPerPixel - gpuPathTracer.samplesRan;
                float timeEstimated = (float) samplesLeft * sampleTimeElapsed;

                RenderInfo* renderInfo = &core.renderInfo;

                renderInfo->samplesFinished = gpuPathTracer.samplesRan;
                renderInfo->samplesPerPixel = core.samplesPerPixel;
                renderInfo->completionPercent = 100.0f * (float) gpuPathTracer.samplesRan / (float) core.samplesPerPixel;
                renderInfo->elapsedTime = getTime() - renderStartTime;
                renderInfo->estimatedTime = timeEstimated;

                renderingIsDone = (gpuPathTracer.samplesRan == core.samplesPerPixel);
            }

#elif CPU_PATH_TRACE

            JobQueue* jobQueue = &cpuPathTracer.jobQueue;

            std::unique_lock<std::mutex> jobQueueLock(*jobQueue->mutex);

            if (jobQueue->recentlyFinishedJobs > 0)
            {
                jobQueue->recentlyFinishedJobs = 0;

                needToUpdateScreenQuad = true;

                u32 newSamplesFinished = jobQueue->finishedJobs / jobQueue->jobCount;

                if (newSamplesFinished > jobQueue->samplesFinished)
                {
                    float sampleTimeElapsed = getTime() - sampleStartTime;
                    sampleStartTime = getTime();
                    int samplesLeft = core.samplesPerPixel - newSamplesFinished;
                    float timeEstimated = (float) samplesLeft * sampleTimeElapsed;

                    jobQueue->samplesFinished = newSamplesFinished;

                    RenderInfo* renderInfo = &core.renderInfo;

                    renderInfo->samplesFinished = jobQueue->samplesFinished;
                    renderInfo->samplesPerPixel = core.samplesPerPixel;
                    renderInfo->completionPercent = 100.0f * (float) newSamplesFinished / (float) core.samplesPerPixel;
                    renderInfo->elapsedTime = getTime() - renderStartTime;
                    renderInfo->estimatedTime = timeEstimated;
                }
            }

            renderingIsDone = (jobQueue->threadsFinished == threadCount);

#endif

        }

        /////////////////////////////////////////////////////
        // ATTEMPT TO HALT RAYTRACER
        /////////////////////////////////////////////////////

        if (core.renderState == RENDER_STATE_HALTING)
        {

#ifdef GPU_PATH_TRACE

            if (gpuPathTracer.thread != NULL && gpuPathTracer.thread->joinable())
            {
                gpuPathTracer.thread->join();
            }

            core.renderState = RENDER_STATE_IDLING;

            printf("Rendering halted.\n");

#elif CPU_PATH_TRACE

            JobQueue* jobQueue = &cpuPathTracer.jobQueue;

            std::unique_lock<std::mutex> jobQueueLock(*jobQueue->mutex);

            jobQueue->jobsAvailable = 0;

            if (jobQueue->threadsFinished == threadCount)
            {
                core.renderState = RENDER_STATE_IDLING;

                printf("Rendering halted.\n");
            }

#endif

        }

        /////////////////////////////////////////////////////
        // ATTEMPT TO RESTART RAYTRACER
        /////////////////////////////////////////////////////

        if (core.renderState == RENDER_STATE_RESTARTING)
        {

#ifdef GPU_PATH_TRACE

            if (gpuPathTracer.thread != NULL && gpuPathTracer.thread->joinable())
            {
                gpuPathTracer.thread->join();
            }

            core.renderState = RENDER_STATE_BEGINNING;

#elif CPU_PATH_TRACE

            JobQueue* jobQueue = &cpuPathTracer.jobQueue;

            std::unique_lock<std::mutex> jobQueueLock(*jobQueue->mutex);

            jobQueue->jobsAvailable = 0;

            if (jobQueue->threadsFinished == threadCount)
            {
                core.renderState = RENDER_STATE_BEGINNING;
            }

#endif

        }

        /////////////////////////////////////////////////////
        // RAYTRACING IS DONE
        /////////////////////////////////////////////////////

        if (renderingIsDone)
        {
            core.renderState = RENDER_STATE_IDLING;

            printf("Rendering finished. Took: %f seconds\n", getTime() - renderStartTime);

            if (core.renderMode == RENDER_MODE_NORMAL)
            {
                saveScreenToDisk(&core.screen, core.pixelBuffer);
            }
        }

        if (needToUpdateScreenQuad)
        {
            updateScreenQuad(&core.screenQuad, core.screen.width, core.screen.height, core.pixelBuffer);
        }

        /////////////////////////////////////////////////////
        // OPENGL RENDERING
        /////////////////////////////////////////////////////

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (core.showScreenQuad)
        {
            renderScreenQuad(&core.screenQuad, &screenQuadShader);
        }
        else
        {
            Scene* scene = &core.scene;

            glEnable(GL_CULL_FACE);
            glEnable(GL_DEPTH_TEST);

            glUseProgram(basicShader.program);

            glUniformMatrix4fv(basicShader.projection, 1, GL_FALSE, &core.camera.projection[0][0]);
            glUniformMatrix4fv(basicShader.view, 1, GL_FALSE, &core.camera.worldToCamera[0][0]);
            glUniform3fv(basicShader.directionToLight, 1, &scene->directionToLight[0]);
            glUniform1f(basicShader.gamma, (core.gammaCorrection) ? 1.0f / 2.2f : 1.0f);

            glBindVertexArray(scene->meshes[0].vao);

            sortMeshesForRendering(&core.scene, &core.camera);

            for (u32 index = 0; index < core.scene.triangleMeshCount; index++)
            {
                u32 meshIndex = scene->renderOrderBuffer[index];
                TriangleMesh* triangleMesh = core.scene.triangleMeshes + meshIndex;
                Material* material = core.scene.materials + triangleMesh->materialId;

                glBindVertexArray(triangleMesh->mesh->vao);
                glUniformMatrix4fv(basicShader.model, 1, GL_FALSE, &triangleMesh->objectToWorld[0][0]);
                glUniform3fv(basicShader.color, 1, &material->color[0]);
                glUniform1f(basicShader.alpha, (material->surfaceType & SURFACE_GLASS) ? 0.5f : 1.0f);
                glUniform1f(basicShader.emitting, (material->surfaceType & SURFACE_EMISSION) ? 1.0f : 0.1f);
                glDrawArrays(GL_TRIANGLES, 0, triangleMesh->mesh->vertexCount);
            }

            glDisable(GL_CULL_FACE);
            glDisable(GL_DEPTH_TEST);

            renderDebugSuite(&core.debugSuite, &lineShader, &core.camera);
        }

        ImGui::Render();

        SDL_GL_SwapWindow(core.window);
    }

    /////////////////////////////////////////////////////
    // CLEANUP
    /////////////////////////////////////////////////////

    core.stopRunning = true;

    freeGUI(&core.gui);
    freeScene(&core.scene);
    freeScreenQuad(&core.screenQuad);
    freeShaderProgram(lineShader.program);
    freeShaderProgram(basicShader.program);
    freeShaderProgram(screenQuadShader.program);
    freeDebugSuite(&core.debugSuite);
    free(core.pixelBuffer);

#ifdef GPU_PATH_TRACE
    freeGPUPathTracer(&gpuPathTracer);
    freeComputeContext(&computeContext);
#elif CPU_PATH_TRACE
    freeCPUPathTracer(&cpuPathTracer);
#endif

    return 0;
}
