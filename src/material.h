/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"

enum SurfaceType
{
    SURFACE_DIFFUSE = (1 << 0),
    SURFACE_GLASS = (1 << 1),
    SURFACE_GLOSSY = (1 << 2),
    SURFACE_EMISSION = (1 << 3)
};

enum TextureType
{
    TEXTURE_FLAT_COLOR,
    TEXTURE_CHECKERBOARD,
    TEXTURE_IMAGE
};

struct Material
{
    vec3 color;
    u32 textureId;

    SurfaceType surfaceType;
    TextureType textureType;

    // TODO(vinht): Diffuse material should respect roughness in some way.
    float roughness;
    float emission;
    float ior;
};

struct MaterialName
{
    char buffer[64];
};