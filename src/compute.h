/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"

#ifdef _DEBUG
    #define CL_CHECK(x) do { cl_int e; if ((e = x) != CL_SUCCESS) printf("OpenCL Error %d @ line: %d file: %s\n", e, __LINE__, __FILE__); } while(0)
#else
    #define CL_CHECK(x) do { x; } while(0)
#endif

struct ComputeContext
{
    cl_context context;
    cl_command_queue commandQueue;
    cl_device_id device;
};

struct ComputeProgram
{
    cl_program program;
    cl_kernel kernel;
};

static bool createComputeContext(ComputeContext* computeContext, bool profiling)
{
    cl_int errorCode = 0;

    /////////////////////////////////////////////////////
    // context
    /////////////////////////////////////////////////////

    cl_platform_id firstPlatformId;
    cl_uint numPlatforms;

    errorCode = clGetPlatformIDs(1, &firstPlatformId, &numPlatforms);

    if (errorCode != CL_SUCCESS || numPlatforms == 0)
    {
        printf("Failed to find any OpenCL platforms.\n");
        return false;
    }

    cl_context_properties contextProperties[] =
    {
        CL_CONTEXT_PLATFORM,
        (cl_context_properties) firstPlatformId,
        0
    };

    computeContext->context = clCreateContextFromType(contextProperties,
                              CL_DEVICE_TYPE_GPU, NULL, NULL, &errorCode);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to create GPU context.\n");
        return false;
    }

    const cl_int bufferSize = 1024;
    cl_char buffer[bufferSize];

    errorCode = clGetPlatformInfo(firstPlatformId, CL_PLATFORM_PROFILE, bufferSize, buffer, NULL);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get platform profile.\n");
        return false;
    }

    printf("\nOpenCL Platform Profile: %s\n", buffer);

    errorCode = clGetPlatformInfo(firstPlatformId, CL_PLATFORM_VERSION, bufferSize, buffer, NULL);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get platform version.\n");
        return false;
    }

    printf("OpenCL Platform Version: %s\n", buffer);

    errorCode = clGetPlatformInfo(firstPlatformId, CL_PLATFORM_NAME, bufferSize, buffer, NULL);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get platform name.\n");
        return false;
    }

    printf("OpenCL Platform Name: %s\n", buffer);

    errorCode = clGetPlatformInfo(firstPlatformId, CL_PLATFORM_VENDOR, bufferSize, buffer, NULL);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get platform vendor.\n");
        return false;
    }

    printf("OpenCL Platform Vendor: %s\n", buffer);

    /////////////////////////////////////////////////////
    // device
    /////////////////////////////////////////////////////

    size_t deviceBufferSize = 0;

    errorCode = clGetContextInfo(computeContext->context, CL_CONTEXT_DEVICES,
                                 0, NULL, &deviceBufferSize);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get device count.\n");
        return false;
    }

    if (deviceBufferSize == 0)
    {
        printf("No devices available.\n");
        return false;
    }

    cl_device_id* devices = (cl_device_id*) alloca(deviceBufferSize);

    errorCode = clGetContextInfo(computeContext->context, CL_CONTEXT_DEVICES,
                                 deviceBufferSize, devices, NULL);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get device IDs.\n");
        return false;
    }

    computeContext->device = devices[0];

    size_t deviceExtensionsSize;

    errorCode = clGetDeviceInfo(computeContext->device,
                                CL_DEVICE_EXTENSIONS,
                                0, NULL, &deviceExtensionsSize);

    cl_char* deviceExtensions = (cl_char*) malloc(deviceExtensionsSize);

    errorCode = clGetDeviceInfo(computeContext->device,
                                CL_DEVICE_EXTENSIONS,
                                deviceExtensionsSize, deviceExtensions, NULL);

    printf("OpenCL Device Extensions: %s\n", deviceExtensions);

    free(deviceExtensions);

    cl_bool imageSupport = CL_FALSE;
    clGetDeviceInfo(computeContext->device,
                    CL_DEVICE_IMAGE_SUPPORT,
                    sizeof(cl_bool),
                    &imageSupport,
                    NULL);

    if (imageSupport != CL_TRUE)
    {
        printf("OpenCL device does not support images.\n");
        return false;
    }

    cl_uint maxComputeUnits;

    errorCode = clGetDeviceInfo(computeContext->device,
                                CL_DEVICE_MAX_COMPUTE_UNITS,
                                sizeof(cl_uint),
                                &maxComputeUnits,
                                NULL);

    printf("CL_DEVICE_MAX_COMPUTE_UNITS: %u\n", maxComputeUnits);

    /////////////////////////////////////////////////////
    // command queue
    /////////////////////////////////////////////////////

    computeContext->commandQueue = clCreateCommandQueue(
                                       computeContext->context,
                                       computeContext->device,
                                       (profiling) ? CL_QUEUE_PROFILING_ENABLE : 0,
                                       NULL);

    if (computeContext->commandQueue == NULL)
    {
        printf("Failed to create commandQueue for device 0.\n");
        return false;
    }

    return true;
}

static bool createComputeProgram(ComputeContext* computeContext, const char* path, ComputeProgram* computeProgram)
{
    /////////////////////////////////////////////////////
    // source file
    /////////////////////////////////////////////////////

    char* fileBuffer;

    FILE* f = fopen(path, "rb");

    if (f != NULL)
    {
        fseek(f, 0, SEEK_END);
        long fsize = ftell(f);
        fseek(f, 0, SEEK_SET);

        fileBuffer = (char*) malloc(fsize + 1);
        fread(fileBuffer, fsize, 1, f);
        fclose(f);

        fileBuffer[fsize] = 0;
    }
    else
    {
        printf("Failed to read kernel source file.\n");
        return false;
    }

    computeProgram->program = clCreateProgramWithSource(computeContext->context, 1,
                              (const char**) &fileBuffer, NULL, NULL);

    if (computeProgram->program == NULL)
    {
        printf("Failed to create CL program from source.\n");
        return false;
    }

    cl_int errorCode = 0;

    errorCode = clBuildProgram(computeProgram->program, 0, NULL, "-Werror", NULL, NULL);

    if (errorCode != CL_SUCCESS)
    {
        u32 buildLogSize = 16 * 1024;
        char* buildLog = (char*) calloc(buildLogSize, 1);
        clGetProgramBuildInfo(computeProgram->program, computeContext->device,
                              CL_PROGRAM_BUILD_LOG, buildLogSize, buildLog, NULL);

        printf("Error in kernel (%s):\n%s\n", path, buildLog);

        clReleaseProgram(computeProgram->program);

        free(buildLog);

        return false;
    }

    free(fileBuffer);


    /////////////////////////////////////////////////////
    // kernel
    /////////////////////////////////////////////////////

    computeProgram->kernel = clCreateKernel(computeProgram->program, "launchKernel", NULL);

    if (computeProgram->kernel == NULL)
    {
        printf("Failed to create kernel\n");
        return false;
    }

    return true;
}

static cl_float profileComputeEvent(cl_event event)
{
    cl_int errorCode;

    cl_ulong startTime = 0;
    cl_ulong endTime = 0;

    size_t returnBytes;

    errorCode = clGetEventProfilingInfo(
                    event,
                    CL_PROFILING_COMMAND_QUEUED,
                    sizeof(cl_ulong),
                    &startTime,
                    &returnBytes);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get event profiling info for start time\n");
        return 0.0f;
    }

    errorCode = clGetEventProfilingInfo(
                    event,
                    CL_PROFILING_COMMAND_END,
                    sizeof(cl_ulong),
                    &endTime,
                    &returnBytes);

    if (errorCode != CL_SUCCESS)
    {
        printf("Failed to get event profiling info for end time\n");
        return 0.0f;
    }

    return (cl_float) (endTime - startTime) * 1.0e-9f;
}

static void freeComputeContext(ComputeContext* computeContext)
{
    clReleaseCommandQueue(computeContext->commandQueue);
    clReleaseContext(computeContext->context);
}

static void freeComputeProgram(ComputeProgram* computeProgram)
{
    clReleaseProgram(computeProgram->program);
    clReleaseKernel(computeProgram->kernel);
}