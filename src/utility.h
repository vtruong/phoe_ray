/*
* The MIT License (MIT)
*
* Copyright (c) 2015 - 2016 Vinh Truong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#pragma once

#include "includes.h"

struct MouseState
{
    float x, y;
    bool button[3];
};

static vec2 getMousePosition()
{
    int x, y;
    SDL_GetMouseState(&x, &y);

    return vec2(x, y);
}

static MouseState getMouseState()
{
    int x, y;
    u32 state = SDL_GetMouseState(&x, &y);

    MouseState ms;
    ms.x = (float)x;
    ms.y = (float)y;
    ms.button[0] = (state & SDL_BUTTON(SDL_BUTTON_LEFT)) != 0;
    ms.button[1] = (state & SDL_BUTTON(SDL_BUTTON_RIGHT)) != 0;
    ms.button[2] = (state & SDL_BUTTON(SDL_BUTTON_MIDDLE)) != 0;

    return ms;
}

static inline float getTime()
{
    return SDL_GetTicks() / 1000.0f;
}

static bool checkFileExistence(const char* fileName)
{
    bool foundFile = true;

    FILE* f = fopen(fileName, "r");

    if (f == NULL)
    {
        foundFile = false;
    }
    else
    {
        fclose(f);
    }

    return foundFile;
}
