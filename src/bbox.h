/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"

struct BBox
{
    vec3 min;
    vec3 max;

    BBox()
    {
        min = vec3(infinity);
        max = vec3(-infinity);
    }

    inline vec3 operator[](u32 i)
    {
        return *(&min + i);
    }
};

static BBox unionBBox(BBox* bbox, vec3 point)
{
    BBox newBBox = *bbox;

    newBBox.min.x = glm::min(bbox->min.x, point.x);
    newBBox.min.y = glm::min(bbox->min.y, point.y);
    newBBox.min.z = glm::min(bbox->min.z, point.z);
    newBBox.max.x = glm::max(bbox->max.x, point.x);
    newBBox.max.y = glm::max(bbox->max.y, point.y);
    newBBox.max.z = glm::max(bbox->max.z, point.z);

    return newBBox;
}

static BBox unionBBox(BBox* a, BBox* b)
{
    BBox newBBox = *a;

    newBBox.min = glm::min(a->min, b->min);
    newBBox.max = glm::max(a->max, b->max);

    return newBBox;
}

static u32 maximumExtentBBox(BBox* bbox)
{
    vec3 diagonal = bbox->max - bbox->min;

    if (diagonal.x > diagonal.y && diagonal.x > diagonal.z)
    {
        return 0;
    }
    else if (diagonal.y > diagonal.z)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

static float surfaceAreaBBox(BBox* bbox)
{
    vec3 d = bbox->max - bbox->min;
    return 2.0f * (d.x * d.y + d.x * d.z + d.y * d.z);
}