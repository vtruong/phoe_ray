/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "screen.h"
#include "utility.h"

struct Camera
{
    vec3 origin;

    vec3 forward;
    vec3 up;
    vec3 right;

    float fov;

    // NOTE(vinht): Cached computation of tan(fov / 2.0)
    float tanFov;

    mat4 projection;
    mat4 worldToCamera;
    mat4 cameraToWorld;

    float theta, phi;
    float dTheta, dPhi;
};

struct CameraControls
{
    vec2 mouseStart;
    vec2 mouseDelta;
    bool beginTracking;
};

static Ray screenToCameraRay(int screenX, int screenY, Camera* camera, Screen* screen, vec2 pixelOffset = vec2())
{
    float x = (float) screenX;
    float y = (float) screenY;

    float w = (float) screen->width;
    float h = (float) screen->height;

    // NOTE(vinht): Normalize pixel position using screen dimensions.
    // 0.5 offset ensures rays pass through middle of the pixel.
    float ndcx = (x + 0.5f + pixelOffset.x) / w;
    float ndcy = (y + 0.5f + pixelOffset.y) / h;

    // NOTE(vinht): Remap coordinates from NDC's [0,1] to [-1,1] so that
    // image is properly centered.
    // y is flipped so that positive values go "up" in traditional way.
    float px = (2.0f * ndcx - 1.0f);
    float py = (1.0f - 2.0f * ndcy);

    // NOTE(vinht): Assuming Width >= Height, we have to scale x so
    // image does not appear to be stretched horizontally.
    px *= screen->aspectRatio;

    // NOTE(vinht): Finally we account for field of view by simply multiplying
    // previously calculated tan(fov / 2)
    px *= camera->tanFov;
    py *= camera->tanFov;

    // NOTE(vinht): We need to transform ray from camera space to world space.
    // Note that camera is already expressed in world space, so the origin
    // does not need to be transformed. However ray direction still lives in
    // camera space.
    Ray ray;

    ray.origin = camera->origin;
    vec3 cameraPosition = vec3(camera->cameraToWorld * vec4(px, py, -1.0f, 1.0f));
    ray.direction = cameraPosition - ray.origin;
    ray.direction = glm::normalize(ray.direction);

    return ray;
}

static bool updateCamera(Camera* camera, CameraControls* cameraControls, Screen* screen)
{
    /////////////////////////////////////////////////////
    // MOVEMENT
    /////////////////////////////////////////////////////

    vec3 movement;

    bool userInteracted = false;

    const u8* keys = SDL_GetKeyboardState(NULL);

    if (keys[SDL_SCANCODE_W])
    {
        movement += camera->forward;
    }

    if (keys[SDL_SCANCODE_S])
    {
        movement -= camera->forward;
    }

    if (keys[SDL_SCANCODE_A])
    {
        movement -= camera->right;
    }

    if (keys[SDL_SCANCODE_D])
    {
        movement += camera->right;
    }

    if (keys[SDL_SCANCODE_Q])
    {
        movement += camera->up;
    }

    if (keys[SDL_SCANCODE_E])
    {
        movement -= camera->up;
    }

    if (glm::length(movement) > 0.0f)
    {
        movement = glm::normalize(movement);
        userInteracted = true;
    }

    float speedModifier = 1.0f;

    if (keys[SDL_SCANCODE_LSHIFT])
    {
        speedModifier = 0.2f;
    }

    camera->origin += movement * speedModifier * 0.5f;

    /////////////////////////////////////////////////////
    // ORIENTATION
    /////////////////////////////////////////////////////

    MouseState ms = getMouseState();

    if (cameraControls->beginTracking)
    {
        if (ms.button[0])
        {
            cameraControls->mouseDelta = vec2(ms.x, ms.y) - cameraControls->mouseStart;

            camera->dTheta = cameraControls->mouseDelta.x / (float) screen->width;
            camera->dPhi = cameraControls->mouseDelta.y / (float) screen->height;

            userInteracted = true;
        }

        float horizontalAngle = camera->theta + camera->dTheta;

        if (horizontalAngle < 0.0f)
        {
            horizontalAngle = 1.0f + horizontalAngle;
        }
        else if (horizontalAngle > 1.0f)
        {
            horizontalAngle = -1.0f + horizontalAngle;
        }

        float verticalAngle = camera->phi + camera->dPhi;

        verticalAngle = glm::clamp(verticalAngle, 0.01f, 1.0f - 0.01f);

        camera->forward.x = sinf(-verticalAngle * pi) * sinf(-horizontalAngle * twopi);
        camera->forward.y = cosf(-verticalAngle * pi);
        camera->forward.z = sinf(-verticalAngle * pi) * cosf(-horizontalAngle * twopi);

        camera->right = glm::cross(camera->forward, vec3(0.0f, 1.0f, 0.0f));
        camera->up = glm::cross(camera->right, camera->forward);

    }

    camera->worldToCamera = glm::lookAt(camera->origin, camera->origin + camera->forward, camera->up);
    camera->cameraToWorld = glm::inverse(camera->worldToCamera);

    return userInteracted;
}
