/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "random.h"

// NOTE(vinht): From the book: Physically Based Rendering, 2nd edition
// Had to swap y with z.
static inline vec3 uniformSampleHemisphere(float u1, float u2)
{
    float y = u1;
    float r = sqrtf(glm::max(0.0f, 1.0f - y * y));
    float phi = 2.0f * pi * u2;
    float x = r * cosf(phi);
    float z = r * sinf(phi);
    return vec3(x, y, z);
}

static vec2 concentricSampleDisk(float u1, float u2)
{
    float r, theta;

    float sx = 2.0f * u1 - 1.0f;
    float sy = 2.0f * u2 - 1.0f;

    if (sx == 0.0f && sy == 0.0f)
    {
        return vec2(0.0f);
    }

    if (sx >= -sy)
    {
        if (sx > sy)
        {
            // NOTE(vinht): First region of disk.
            r = sx;
            theta = (sy > 0.0f) ? sy / r : 8.0f + sy / r;
        }
        else
        {
            // NOTE(vinht): Second region of disk.
            r = sy;
            theta = 2.0f - sx / r;
        }
    }
    else
    {
        if (sx <= sy)
        {
            // NOTE(vinht): Third region of disk.
            r = -sx;
            theta = 4.0f - sy / r;
        }
        else
        {
            // NOTE(vinht): Fourth region of disk.
            r = -sy;
            theta = 6.0f + sx / r;
        }
    }

    theta *= pi / 4.0f;

    return r * vec2(cosf(theta), sinf(theta));
}

static inline vec3 cosineSampleHemisphere(float u1, float u2)
{
    vec2 point = concentricSampleDisk(u1, u2);

    vec3 d;

    d.x = point.x;
    d.z = point.y;

    d.y = sqrtf(glm::max(0.0f, 1.0f - d.x * d.x - d.z * d.z));

    return d;
}

// NOTE(vinht): From the book: Physically Based Rendering, 2nd edition
static void coordinateSystem(vec3 i, vec3* j, vec3* k)
{
    if (fabsf(i.x) > fabsf(i.y))
    {
        *j = vec3(-i.z, 0.f, i.x);
        *j = glm::normalize(*j);
    }
    else
    {
        *j = vec3(0.f, i.z, -i.y);
        *j = glm::normalize(*j);
    }
    *k = glm::cross(i, *j);
}

static vec3 sampleDirection(vec3 i, float e1, float e2)
{
    vec3 sample = cosineSampleHemisphere(e1, e2);

    vec3 j, k;

    coordinateSystem(i, &j, &k);

    vec3 direction;

    direction.x = glm::dot(vec3(j.x, i.x, k.x), sample);
    direction.y = glm::dot(vec3(j.y, i.y, k.y), sample);
    direction.z = glm::dot(vec3(j.z, i.z, k.z), sample);

    return direction;
}

static vec2 sampleStratified(int samples, int sampleId)
{
    int sideLength = (int) sqrtf((float) samples);

    vec2 sample;

    sample.x = (float) (sampleId % sideLength);
    sample.y = (float) (sampleId / sideLength);

    sample.x += randf();
    sample.y += randf();

    sample.x /= (float) sideLength;
    sample.y /= (float) sideLength;

    return sample;
}