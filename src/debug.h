/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 - 2016 Vinh Truong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#pragma once

#include "includes.h"
#include "screen.h"

// TODO(vinht): Clean up this entire file, most tests are just visualizing lines.

struct LineVertex
{
    vec3 position;
    vec4 color;
};

struct LineAggregate
{
    u32 vao;
    u32 vbo;

    LineVertex* vertices;
    u32 vertexCount;
};

static void initLineAggregate(LineAggregate* aggregate)
{
    assert(aggregate != nullptr);

    glGenVertexArrays(1, &aggregate->vao);
    glBindVertexArray(aggregate->vao);

    glGenBuffers(1, &aggregate->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, aggregate->vbo);
    glBufferData(GL_ARRAY_BUFFER, aggregate->vertexCount * sizeof(LineVertex), aggregate->vertices, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(LineVertex), (void*) 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(LineVertex), (void*) sizeof(vec3));
}

static void renderLineAggregate(LineAggregate* aggregate, LineShader* shader, Camera* camera)
{
    assert(aggregate != nullptr);
    assert(shader != nullptr);
    assert(camera != nullptr);

    glUseProgram(shader->program);

    glUniformMatrix4fv(shader->projection, 1, GL_FALSE, &camera->projection[0][0]);
    glUniformMatrix4fv(shader->view, 1, GL_FALSE, &camera->worldToCamera[0][0]);

    glBindVertexArray(aggregate->vao);

    glDrawArrays(GL_LINES, 0, aggregate->vertexCount);
}

static void freeLineAggregate(LineAggregate* aggregate)
{
    assert(aggregate != nullptr);

    glDeleteBuffers(1, &aggregate->vbo);
    glDeleteVertexArrays(1, &aggregate->vao);

    free(aggregate->vertices);
}

// NOTE(vinht): Technically this is more like "primary-ray visualization" or "camera to first
// intersection visualization", but visibility sounded better to be honest.
struct VisibilityTest
{
    int screenWidth;
    int screenHeight;
    int sampleNth;
    LineAggregate aggregate;
    bool render;
};

static VisibilityTest createVisibilityTest(int screenWidth, int screenHeight, int sampleNth)
{
    assert(screenWidth > 0);
    assert(screenHeight > 0);
    assert(sampleNth > 0);

    VisibilityTest test;

    test.screenWidth = screenWidth;
    test.screenHeight = screenHeight;
    test.sampleNth = sampleNth;
    test.aggregate.vertexCount = (screenWidth / sampleNth) * (screenHeight / sampleNth) * 2;
    test.aggregate.vertices = (LineVertex*) calloc(test.aggregate.vertexCount, sizeof(LineVertex));
    test.render = false;
    initLineAggregate(&test.aggregate);

    return test;
}

static void runVisibilityTest(VisibilityTest* test, Screen* screen, Camera* camera, Scene* scene)
{
    assert(test != nullptr);

    if (test->screenWidth != screen->width &&
        test->screenHeight != screen->height)
    {
        test->screenWidth = screen->width;
        test->screenHeight = screen->height;

        test->aggregate.vertexCount =
            (test->screenWidth / test->sampleNth) *
            (test->screenHeight / test->sampleNth) * 2;

        free(test->aggregate.vertices);
        test->aggregate.vertices =
            (LineVertex*) calloc(test->aggregate.vertexCount, sizeof(LineVertex));
    }

    u32 lineIndex = 0;

    vec4 visibleColor(0.7f, 1.0f, 0.7f, 1.0f);
    vec4 missColor(0.9f, 0.9f, 0.9f, 0.1f);

    for (int screenY = 0;
         screenY < screen->height / test->sampleNth;
         screenY++)
    {
        for (int screenX = 0;
             screenX < screen->width / test->sampleNth;
             screenX++)
        {
            Ray ray;

            ray = screenToCameraRay(screenX * test->sampleNth,
                                    screenY * test->sampleNth,
                                    camera,
                                    screen);

            RayHit hit;

            LineVertex* vertices = test->aggregate.vertices;

            vertices[lineIndex + 0].position = ray.origin;

            if (bvhIntersect(&ray, scene, &hit))
            {
                vertices[lineIndex + 1].position = hit.position;

                vertices[lineIndex + 0].color = visibleColor;
                vertices[lineIndex + 1].color = visibleColor;
            }
            else
            {
                vertices[lineIndex + 1].position = ray.origin + ray.direction * tMaxRay;

                vertices[lineIndex + 0].color = missColor;
                vertices[lineIndex + 1].color = missColor;
            }

            lineIndex += 2;
        }
    }

    glBindVertexArray(test->aggregate.vao);
    glBindBuffer(GL_ARRAY_BUFFER, test->aggregate.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 test->aggregate.vertexCount * sizeof(LineVertex),
                 test->aggregate.vertices,
                 GL_STATIC_DRAW);
}

struct BounceTest
{
    int maxBounceDepth;
    LineAggregate aggregate;
    bool render;
};

static BounceTest createBounceTest(int maxBounceDepth)
{
    assert(maxBounceDepth > 0);

    BounceTest test;

    test.maxBounceDepth = maxBounceDepth;
    test.aggregate.vertexCount = maxBounceDepth * 2;
    test.aggregate.vertices = (LineVertex*) calloc(test.aggregate.vertexCount, sizeof(LineVertex));
    test.render = false;
    initLineAggregate(&test.aggregate);

    return test;
}

static void runBounceTest(BounceTest* test, Screen* screen, Camera* camera, Scene* scene)
{
    assert(test != nullptr);
    assert(core != nullptr);

    int screenCenterX = screen->width / 2;
    int screenCenterY = screen->height / 2;

    u32 lineIndex = 0;

    int bounceDepth = 0;

    vec4 rayColor(0.0f, 1.0f, 0.0f, 1.0f);

    LineVertex* vertices = test->aggregate.vertices;
    memset(test->aggregate.vertices, 0, test->aggregate.vertexCount * sizeof(LineVertex));

    Ray ray;
    ray = screenToCameraRay(screenCenterX, screenCenterY, camera, screen);

    while (bounceDepth < test->maxBounceDepth)
    {
        RayHit hit;

        if (bvhIntersect(&ray, scene, &hit))
        {
            bool inside = false;
            if (glm::dot(ray.direction, hit.normal) > 0)
            {
                hit.normal = -hit.normal;
                inside = true;
            }

            Material* hitMaterial = &scene->materials[hit.materialId];

            if (hitMaterial->surfaceType & SURFACE_GLOSSY)
            {
                vertices[lineIndex + 0].position = ray.origin;
                vertices[lineIndex + 1].position = hit.position;

                vertices[lineIndex + 0].color = rayColor;
                vertices[lineIndex + 1].color = rayColor;

                ray.direction = glm::reflect(ray.direction, hit.normal);
                ray.origin = hit.position + ray.direction * 0.01f;

                bounceDepth++;

                lineIndex += 2;
            }
            else if (hitMaterial->surfaceType & SURFACE_GLASS)
            {
                vertices[lineIndex + 0].position = ray.origin;
                vertices[lineIndex + 1].position = hit.position;

                vertices[lineIndex + 0].color = rayColor;
                vertices[lineIndex + 1].color = rayColor;

                float refractiveIndex = (inside) ? 1.3f : 1.0f / 1.3f;

                ray.direction = glm::refract(ray.direction, hit.normal, refractiveIndex);
                ray.origin = hit.position + ray.direction * 0.01f;

                bounceDepth++;

                lineIndex += 2;
            }
            else
            {
                vertices[lineIndex + 0].position = ray.origin;
                vertices[lineIndex + 1].position = hit.position;

                vertices[lineIndex + 0].color = rayColor;
                vertices[lineIndex + 1].color = rayColor;
                break;
            }
        }
        else
        {
            vertices[lineIndex + 0].position = ray.origin;
            vertices[lineIndex + 1].position = ray.origin + ray.direction * tMaxRay;

            vertices[lineIndex + 0].color = rayColor;
            vertices[lineIndex + 1].color = rayColor;
            break;
        }
    }

    glBindVertexArray(test->aggregate.vao);
    glBindBuffer(GL_ARRAY_BUFFER, test->aggregate.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 test->aggregate.vertexCount * sizeof(LineVertex),
                 test->aggregate.vertices,
                 GL_STATIC_DRAW);
}

struct HemisphereSamplingTest
{
    int sampleCount;
    LineAggregate aggregate;
    bool render;
};

static HemisphereSamplingTest createHemisphereSamplingTest(int sampleCount)
{
    assert(sampleCount > 0);

    HemisphereSamplingTest test;

    test.sampleCount = sampleCount;
    // NOTE(vinht): Line for primary ray. 3 lines for each basis vectors. Rest for samplers.
    test.aggregate.vertexCount = 2 + 6 + sampleCount * 2;
    test.aggregate.vertices = (LineVertex*) calloc(test.aggregate.vertexCount, sizeof(LineVertex));
    test.render = false;
    initLineAggregate(&test.aggregate);

    return test;
}

static void runHemisphereSamplingTest(HemisphereSamplingTest* test, Screen* screen,
                                      Camera* camera, Scene* scene)
{
    assert(test != nullptr);

    int screenCenterX = screen->width / 2;
    int screenCenterY = screen->height / 2;

    vec4 primaryRayColor(0.5f, 0.5f, 0.5f, 1.0f);
    vec4 uColor(0.0f, 1.0f, 0.0f, 1.0f);
    vec4 vColor(1.0f, 0.0f, 0.0f, 1.0f);
    vec4 wColor(0.0f, 0.0f, 1.0f, 1.0f);
    vec4 samplerRayColor(1.0f, 0.5f, 0.0f, 1.0f);

    float basisVectorLength = 0.25f;

    LineVertex* vertices = test->aggregate.vertices;
    memset(test->aggregate.vertices, 0, test->aggregate.vertexCount * sizeof(LineVertex));

    Ray ray;
    ray = screenToCameraRay(screenCenterX, screenCenterY, camera, screen);

    RayHit hit;

    if (bvhIntersect(&ray, scene, &hit))
    {
        u32 lineIndex = 0;

        vec3 basisVectors[3];
        basisVectors[0] = hit.normal;
        coordinateSystem(basisVectors[0], &basisVectors[1], &basisVectors[2]);

        // debug lines
        vertices[lineIndex + 0].position = ray.origin;
        vertices[lineIndex + 1].position = hit.position;

        vertices[lineIndex + 0].color = primaryRayColor;
        vertices[lineIndex + 1].color = primaryRayColor;

        lineIndex += 2;

        vertices[lineIndex + 0].position = hit.position;
        vertices[lineIndex + 1].position = hit.position + basisVectors[0] * basisVectorLength;

        vertices[lineIndex + 0].color = uColor;
        vertices[lineIndex + 1].color = uColor;

        lineIndex += 2;

        vertices[lineIndex + 0].position = hit.position;
        vertices[lineIndex + 1].position = hit.position + basisVectors[1] * basisVectorLength;

        vertices[lineIndex + 0].color = vColor;
        vertices[lineIndex + 1].color = vColor;

        lineIndex += 2;

        vertices[lineIndex + 0].position = hit.position;
        vertices[lineIndex + 1].position = hit.position + basisVectors[2] * basisVectorLength;

        vertices[lineIndex + 0].color = wColor;
        vertices[lineIndex + 1].color = wColor;

        lineIndex += 2;

        // NOTE(vinht): FOllowing commented code is used to tune
        // coordinate system in order to get proper transforms
        mat3 m;
        m[0] = basisVectors[1];
        m[1] = basisVectors[0];
        m[2] = basisVectors[2];

        //{
        //    vec3 d = vec3(1.0f, 0.0f, 0.0f);
        //    vertices[lineIndex + 0].color = vec4(d, 1.0f);
        //    vertices[lineIndex + 1].color = vec4(d, 1.0f);
        //    d = m * d;
        //    vertices[lineIndex + 0].position = hit.position + d;
        //    vertices[lineIndex + 1].position = hit.position + d * 1.5f;
        //    lineIndex += 2;
        //}

        //{
        //    vec3 d = vec3(0.0f, 1.0f, 0.0f);
        //    vertices[lineIndex + 0].color = vec4(d, 1.0f);
        //    vertices[lineIndex + 1].color = vec4(d, 1.0f);
        //    d = m * d;
        //    vertices[lineIndex + 0].position = hit.position + d;
        //    vertices[lineIndex + 1].position = hit.position + d * 1.5f;
        //    lineIndex += 2;
        //}

        //{
        //    vec3 d = vec3(0.0f, 0.0f, 1.0f);
        //    vertices[lineIndex + 0].color = vec4(d, 1.0f);
        //    vertices[lineIndex + 1].color = vec4(d, 1.0f);
        //    d = m * d;
        //    vertices[lineIndex + 0].position = hit.position + d;
        //    vertices[lineIndex + 1].position = hit.position + d * 1.5f;
        //    lineIndex += 2;
        //}

        // sampler vectors
        float s = 1.0f;

        for (int sampleId = 0; sampleId < test->sampleCount; sampleId++)
        {
            float e1 = (randf() - 0.5f) * s + 0.5f;
            float e2 = (randf() - 0.5f) * s + 0.5f;

            //vec3 d = uniformSampleHemisphere(e1, e2);
            vec3 d = cosineSampleHemisphere(e1, e2);
            d = m * d;

            vertices[lineIndex + 0].position = hit.position + d * 1.4f;
            vertices[lineIndex + 1].position = hit.position + d * 1.5f;

            vertices[lineIndex + 0].color = samplerRayColor;
            vertices[lineIndex + 1].color = samplerRayColor;

            lineIndex += 2;
        }
    }

    glBindVertexArray(test->aggregate.vao);
    glBindBuffer(GL_ARRAY_BUFFER, test->aggregate.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 test->aggregate.vertexCount * sizeof(LineVertex),
                 test->aggregate.vertices,
                 GL_STATIC_DRAW);
}

struct BBoxTest
{
    u32 bboxCount;
    LineAggregate aggregate;
    bool render;
};

static BBoxTest createBBoxTest(u32 bboxCount)
{
    assert(bboxCount > 0);

    BBoxTest test;

    test.bboxCount = bboxCount;
    test.aggregate.vertexCount = bboxCount * 24;
    test.aggregate.vertices = (LineVertex*) calloc(test.aggregate.vertexCount, sizeof(LineVertex));
    test.render = false;
    initLineAggregate(&test.aggregate);

    return test;
}

static void buildWireframeBBox(LineVertex* vertices, u32* vertexId, BBox* bbox, vec4 color)
{
    vec3 c1 = bbox->min;
    vec3 c8 = bbox->max;

    float xWidth = c8.x - c1.x;
    float zWidth = c8.z - c1.z;

    vec3 c2 = c1;
    c2.x += xWidth;

    vec3 c3 = c1;
    c3.x += xWidth;
    c3.z += zWidth;

    vec3 c4 = c1;
    c4.z += zWidth;

    vec3 c5 = c4;
    c5.y = c8.y;

    vec3 c6 = c1;
    c6.y = c8.y;

    vec3 c7 = c2;
    c7.y = c8.y;

    for (u32 bvhNodeId = 0; bvhNodeId < 24; bvhNodeId++)
    {
        vertices[(*vertexId) + bvhNodeId].color = color;
    }

    // (1,2) (2,3) (3,4) (4,1)
    vertices[(*vertexId)++].position = c1;
    vertices[(*vertexId)++].position = c2;

    vertices[(*vertexId)++].position = c2;
    vertices[(*vertexId)++].position = c3;

    vertices[(*vertexId)++].position = c3;
    vertices[(*vertexId)++].position = c4;

    vertices[(*vertexId)++].position = c4;
    vertices[(*vertexId)++].position = c1;

    // (5,6) (6,7) (7,8) (8,5)
    vertices[(*vertexId)++].position = c5;
    vertices[(*vertexId)++].position = c6;

    vertices[(*vertexId)++].position = c6;
    vertices[(*vertexId)++].position = c7;

    vertices[(*vertexId)++].position = c7;
    vertices[(*vertexId)++].position = c8;

    vertices[(*vertexId)++].position = c8;
    vertices[(*vertexId)++].position = c5;

    // (1,6) (2,7) (3,8) (4,5)
    vertices[(*vertexId)++].position = c1;
    vertices[(*vertexId)++].position = c6;

    vertices[(*vertexId)++].position = c2;
    vertices[(*vertexId)++].position = c7;

    vertices[(*vertexId)++].position = c3;
    vertices[(*vertexId)++].position = c8;

    vertices[(*vertexId)++].position = c4;
    vertices[(*vertexId)++].position = c5;
}

static void runBBoxTest(BBoxTest* test, Scene* scene)
{
    assert(test);

    u32 vertexId = 0;

    LineVertex* vertices = test->aggregate.vertices;

    for (u32 bboxId = 0; bboxId < test->bboxCount; bboxId++)
    {
        BBox* bbox = scene->bboxes + bboxId;

        buildWireframeBBox(vertices, &vertexId, bbox, vec4(0.0f, 1.0f, 0.0f, 0.1f));
    }

    glBindVertexArray(test->aggregate.vao);
    glBindBuffer(GL_ARRAY_BUFFER, test->aggregate.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 test->aggregate.vertexCount * sizeof(LineVertex),
                 test->aggregate.vertices,
                 GL_STATIC_DRAW);
}

struct BVHTest
{
    u32 nodeCount;
    LineAggregate aggregate;
    bool render;
};

static BVHTest createBVHTest(u32 nodeCount)
{
    assert(nodeCount > 0);

    BVHTest test;

    test.nodeCount = nodeCount;
    test.aggregate.vertexCount = nodeCount * 24;
    test.aggregate.vertices = (LineVertex*) calloc(test.aggregate.vertexCount, sizeof(LineVertex));
    test.render = false;
    initLineAggregate(&test.aggregate);

    return test;
}

static void runBVHTest(BVHTest* test, Scene* scene)
{
    assert(test);
    assert(core);

    u32 vertexId = 0;

    LineVertex* vertices = test->aggregate.vertices;

    for (u32 bboxId = 0; bboxId < test->nodeCount; bboxId++)
    {
        LinearBVHNode* ln = scene->bvhNodes + bboxId;
        BBox* bbox = &ln->bounds;

        buildWireframeBBox(vertices, &vertexId, bbox, vec4(0.0f, 1.0f, 0.0f, 0.1f));
    }

    glBindVertexArray(test->aggregate.vao);
    glBindBuffer(GL_ARRAY_BUFFER, test->aggregate.vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 test->aggregate.vertexCount * sizeof(LineVertex),
                 test->aggregate.vertices,
                 GL_STATIC_DRAW);
}

struct DebugSuite
{
    VisibilityTest visibilityTest;
    BounceTest bounceTest;
    HemisphereSamplingTest hemisphereSamplingTest;
    BBoxTest bboxTest;
    BVHTest bvhTest;
};

static void createDebugSuite(DebugSuite* debugSuite, Screen* screen, Scene* scene)
{
    debugSuite->visibilityTest = createVisibilityTest(screen->width, screen->height, 32);
    debugSuite->bounceTest = createBounceTest(16);
    debugSuite->hemisphereSamplingTest = createHemisphereSamplingTest(1024);
    debugSuite->bboxTest = createBBoxTest(scene->bboxCount);
    debugSuite->bvhTest = createBVHTest(scene->bvhNodeCount);
}

static void freeDebugSuite(DebugSuite* debugSuite)
{
    freeLineAggregate(&debugSuite->bvhTest.aggregate);
    freeLineAggregate(&debugSuite->bboxTest.aggregate);
    freeLineAggregate(&debugSuite->hemisphereSamplingTest.aggregate);
    freeLineAggregate(&debugSuite->bounceTest.aggregate);
    freeLineAggregate(&debugSuite->visibilityTest.aggregate);
}

static void renderDebugSuite(DebugSuite* debugSuite, LineShader* shader, Camera* camera)
{
    VisibilityTest* visibilityTest = &debugSuite->visibilityTest;
    BounceTest* bounceTest = &debugSuite->bounceTest;
    HemisphereSamplingTest* hemisphereSamplingTest = &debugSuite->hemisphereSamplingTest;
    BBoxTest* bboxTest = &debugSuite->bboxTest;
    BVHTest* bvhTest = &debugSuite->bvhTest;

    if (visibilityTest->render)
    {
        renderLineAggregate(&visibilityTest->aggregate, shader, camera);
    }

    if (bounceTest->render)
    {
        renderLineAggregate(&bounceTest->aggregate, shader, camera);
    }

    if (hemisphereSamplingTest->render)
    {
        renderLineAggregate(&hemisphereSamplingTest->aggregate, shader, camera);
    }

    if (bboxTest->render)
    {
        renderLineAggregate(&bboxTest->aggregate, shader, camera);
    }

    if (bvhTest->render)
    {
        renderLineAggregate(&bvhTest->aggregate, shader, camera);
    }
}

static void runSamplingTest()
{
    int sampleCount = 64;
    int sideCount = (int) sqrtf((float) sampleCount);
    float invSideLength = 1.0f / (float) sideCount;

    printf("Sample count: %d\n", sampleCount);
    printf("Side count: %d\n", sideCount);
    printf("Inv side length: %f\n", invSideLength);

    for (int subY = 0; subY < sideCount; subY++)
    {
        for (int subX = 0; subX < sideCount; subX++)
        {
            vec2 jitter;

            // initial starting point
            jitter.x = ((float) subX) * invSideLength - 0.5f;
            jitter.y = ((float) subY) * invSideLength - 0.5f;

            // jitter pixel
            jitter.x += (randf() * invSideLength);
            jitter.y += (randf() * invSideLength);

            // center pixel
            //jitter.x += 0.5f;
            //jitter.y += 0.5f;

            float xWidth = 1.0f;
            float yWidth = 1.0f;
            jitter *= glm::max(0.f, xWidth - fabsf(jitter.x)) *
                      glm::max(0.f, yWidth - fabsf(jitter.y));

            printf("(%.2f,%.2f)", jitter.x, jitter.y);

        }
        printf("\n");
    }
}

static void runHashMapTest()
{
    HashMap hashMap;

    hashMapInit(&hashMap);

    hashMapInsert(&hashMap, "black", 0);
    hashMapInsert(&hashMap, "white", 1);
    hashMapInsert(&hashMap, "red", 2);
    hashMapInsert(&hashMap, "green", 3);
    hashMapInsert(&hashMap, "blue", 4);
    hashMapInsert(&hashMap, "yellow", 5);
    hashMapInsert(&hashMap, "yellow", 5);
    hashMapInsert(&hashMap, "yellow", 5);
    hashMapInsert(&hashMap, "yellow", 5);
    hashMapInsert(&hashMap, "yellow", 5);
    hashMapInsert(&hashMap, "purple\0trash", 6);

    for (u32 bvhNodeId = 0; bvhNodeId < maxHashMapSize; bvhNodeId++)
    {
        u8 occupancy = hashMap.occupancy[bvhNodeId];
        u8* key = hashMap.keys[bvhNodeId];
        u32 data = hashMap.data[bvhNodeId];
        printf("(%u,%s,%u)", (u32)occupancy, key, data);
    }

    printf("\n");

    u32 data;
    printf("%s is at %d\n", "black", hashMapLookup(&hashMap, "black", &data) ? data : -1);
    printf("%s is at %d\n", "green", hashMapLookup(&hashMap, "green", &data) ? data : -1);
    printf("%s is at %d\n", "purple", hashMapLookup(&hashMap, "purple", &data) ? data : -1);
    printf("%s is at %d\n", "dude", hashMapLookup(&hashMap, "dude", &data) ? data : -1);
}

static void runPartitionTest()
{
    u32 buildData [] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    u32* tempBuffer;
    tempBuffer = (u32*) malloc(sizeof(buildData));

    u32 start = 0;
    u32 end = 10;

    u32 startIndex = start, endIndex = end - 1;

    for (u32 bvhNodeId = start; bvhNodeId < end; bvhNodeId++)
    {
        if (buildData[bvhNodeId] % 2 == 0)
        {
            *(tempBuffer + startIndex) = buildData[bvhNodeId];
            startIndex++;
        }
        else
        {
            *(tempBuffer + endIndex) = buildData[bvhNodeId];
            endIndex--;
        }
    }

    memcpy(buildData, tempBuffer, sizeof(buildData));

    u32* firstFalse = &buildData[endIndex + 1];

    for (u32 bvhNodeId = start; bvhNodeId < end; bvhNodeId++)
    {
        printf("%u ", buildData[bvhNodeId]);
    }
    printf("\n");
    printf("first false: %u\n", *firstFalse);

    free(tempBuffer);
}

static void runRaySpamTest(Scene* scene)
{
    const u32 rayCount = 10000;
    const float spreadRadius = 10.0f;

    printf("Generated %d random rays with spread radius %f\n", rayCount, spreadRadius);

    Ray* randomRays = (Ray*) malloc(rayCount * sizeof(Ray));

    for (u32 bvhNodeId = 0; bvhNodeId < rayCount; bvhNodeId++)
    {
        randomRays[bvhNodeId].origin = vec3(spreadRadius * randf(), spreadRadius * randf(), spreadRadius * randf());
        randomRays[bvhNodeId].direction = vec3(randf(), randf(), randf());
        // NOTE(vinht): Add epsilon if by chance randf() returns zero three times for direction.
        randomRays[bvhNodeId].direction += vec3(epsilon);
        randomRays[bvhNodeId].direction = glm::normalize(randomRays[bvhNodeId].direction);
    }

    struct HitRecord
    {
        bool hitSomething;
        vec3 hitPosition;
    };

    u32 hitCount = 0;

    HitRecord* hitRecords = (HitRecord*) malloc(rayCount * sizeof(HitRecord));

    float startTime = getTime();

    for (u32 bvhNodeId = 0; bvhNodeId < rayCount; bvhNodeId++)
    {
        RayHit hitData;

        bool hit = badIntersect(randomRays + bvhNodeId, scene, &hitData);

        hitRecords[bvhNodeId].hitSomething = hit;

        if (hit)
        {
            hitCount++;
            hitRecords[bvhNodeId].hitPosition = hitData.position;
        }
    }

    float bruteforceElapsedTime = getTime() - startTime;

    printf("Performed %d brute-force intersects:\n", rayCount);
    printf("  Ray intersects: %d\n", hitCount);
    printf("  Took %f seconds\n", bruteforceElapsedTime);

    u32 successHits = 0;

    float totalError = 0.0f;

    startTime = getTime();

    for (u32 bvhNodeId = 0; bvhNodeId < rayCount; bvhNodeId++)
    {
        RayHit hitData;

        bool hit = bvhIntersect(randomRays + bvhNodeId, scene, &hitData);

        if (hit)
        {
            successHits++;

            float error = glm::distance(hitData.position, hitRecords[bvhNodeId].hitPosition);

            totalError += error;
        }
    }

    float bvhElapsedTime = getTime() - startTime;

    printf("Performed %d bvh intersects:\n", rayCount);
    printf("  Success ratio: %d / %d (%.2f%%)\n", successHits, hitCount, 100.0f * (float) successHits / (float) hitCount);
    printf("  Total error: %f\n", totalError);
    printf("  Took %f seconds\n", bvhElapsedTime);

    printf("BVH ran %f times faster\n", bruteforceElapsedTime / bvhElapsedTime);

    free(hitRecords);
    free(randomRays);
}

static void runScreenPartitionTest(int screenWidth, int screenHeight, int partitionWidth)
{
    printf("\nPartitioning screen sized w:%d h:%d\n", screenWidth, screenHeight);

    printf("Block partition width: %d\n", partitionWidth);

    int horizontalBlockCount = screenWidth / partitionWidth;
    int horizontalLeftOver = screenWidth % partitionWidth;

    int verticalBlockCount = screenHeight / partitionWidth;
    int verticalLeftOver = screenHeight % partitionWidth;

    printf("Horizontal block count: %d\n", horizontalBlockCount);
    printf("Horizontal left over: %d\n", horizontalLeftOver);
    printf("Vertical block count: %d\n", verticalBlockCount);
    printf("Vertical left over: %d\n", verticalLeftOver);

    int screenArea = screenWidth * screenHeight;

    printf("Screen area: %d\n", screenArea);

    int completeBlockArea = horizontalBlockCount * verticalBlockCount *
                            partitionWidth * partitionWidth;

    printf("Complete block area: %d\n", completeBlockArea);

    int horizontalDegenerateArea = horizontalBlockCount * verticalLeftOver * partitionWidth;
    int verticalDegenerateArea = verticalBlockCount * horizontalLeftOver * partitionWidth;

    printf("Horizontal degenerate area: %d \n", horizontalDegenerateArea);
    printf("Vertical degenerate area: %d \n", verticalDegenerateArea);

    int degenerateArea = horizontalLeftOver * verticalLeftOver;

    printf("Degenerate area: %d\n", degenerateArea);

    int calculatedBlockArea = completeBlockArea + horizontalDegenerateArea +
                              verticalDegenerateArea + degenerateArea;

    printf("Reconstructed block area: %d\n", calculatedBlockArea);

    int error = screenArea - calculatedBlockArea;

    printf("Area error (screen vs. calculated): %d\n", error);

    struct ScreenBlock
    {
        int x, y;
        int w, h;
    };

    int totalBlockCount = horizontalBlockCount * verticalBlockCount;

    if (horizontalDegenerateArea > 0)
    {
        totalBlockCount += horizontalBlockCount;
    }

    if (verticalDegenerateArea > 0)
    {
        totalBlockCount += verticalBlockCount;
    }

    if (horizontalLeftOver > 0 && verticalBlockCount > 0)
    {
        totalBlockCount += 1;
    }

    printf("Total job count: %d\n", totalBlockCount);

    ScreenBlock* screenBlocks = (ScreenBlock*) malloc(totalBlockCount * sizeof(ScreenBlock));
    int blockIndex = 0;

    int newBlockArea = 0;

    for (int y = 0; y < verticalBlockCount; y++)
    {
        for (int x = 0; x < horizontalBlockCount; x++)
        {
            int blockWidth = partitionWidth;
            int blockHeight = partitionWidth;

            newBlockArea += blockWidth * blockHeight;

            ScreenBlock* block = &screenBlocks[blockIndex++];
            block->x = partitionWidth * x;
            block->y = partitionWidth * y;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (horizontalDegenerateArea > 0)
    {
        for (int x = 0; x < horizontalBlockCount; x++)
        {
            int blockWidth = partitionWidth;
            int blockHeight = verticalLeftOver;

            newBlockArea += blockWidth * blockHeight;

            ScreenBlock* block = &screenBlocks[blockIndex++];
            block->x = partitionWidth * x;
            block->y = partitionWidth * verticalBlockCount;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (verticalDegenerateArea > 0)
    {
        for (int y = 0; y < verticalBlockCount; y++)
        {
            int blockWidth = horizontalLeftOver;
            int blockHeight = partitionWidth;

            newBlockArea += blockWidth * blockHeight;

            ScreenBlock* block = &screenBlocks[blockIndex++];
            block->x = partitionWidth * horizontalBlockCount;
            block->y = partitionWidth * y;
            block->w = blockWidth;
            block->h = blockHeight;
        }
    }

    if (horizontalDegenerateArea > 0 && verticalDegenerateArea > 0)
    {
        int blockWidth = horizontalLeftOver;
        int blockHeight = verticalLeftOver;

        newBlockArea += blockWidth * blockHeight;

        ScreenBlock* block = &screenBlocks[blockIndex++];
        block->x = partitionWidth * horizontalBlockCount;
        block->y = partitionWidth * verticalBlockCount;
        block->w = blockWidth;
        block->h = blockHeight;
    }

    printf("New block area: %d\n", newBlockArea);

    error = screenArea - newBlockArea;

    printf("Area error (screen vs. iterated): %d\n", error);

    printf("\n");

    free(screenBlocks);
}

static void runVarianceTest(u8* pixelBuffer, Screen* screen)
{
    u8* buffer = pixelBuffer;
    int w = screen->width;
    int h = screen->height;

    u64 total = 0;
    u64 count = 0;

    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            u32 pixelIndex = 4 * (x + y * w);

            total += buffer[pixelIndex + 0];
            total += buffer[pixelIndex + 1];
            total += buffer[pixelIndex + 2];

            count += 3;
        }
    }

    f64 mean = (f64) total / (f64) count;

    f64 differenceTotal = 0;

    for (int y = 0; y < h; y++)
    {
        for (int x = 0; x < w; x++)
        {
            u32 pixelIndex = 4 * (x + y * w);

            f64 r = (f64) buffer[pixelIndex + 0];
            f64 g = (f64) buffer[pixelIndex + 1];
            f64 b = (f64) buffer[pixelIndex + 2];

            f64 dr = r - mean;
            dr *= dr;
            differenceTotal += dr;

            f64 dg = g - mean;
            dg *= dg;
            differenceTotal += dg;

            f64 db = b - mean;
            db *= db;
            differenceTotal += db;
        }
    }

    f64 variance = differenceTotal / (f64) count;

    printf("Mean: %f\n", mean);
    printf("Variance: %f\n", variance);
}

static void outputBVHFile(Scene* scene)
{
    FILE* bvhFile = fopen("bvh.csv", "w");

    fprintf(bvhFile, "id,offset,prim_cnt,axis\n");

    for (u32 bvhNodeId = 0; bvhNodeId < scene->bvhNodeCount; bvhNodeId++)
    {
        LinearBVHNode* node = scene->bvhNodes + bvhNodeId;

        fprintf(bvhFile, "%u,%u,%u,%u\n",
                bvhNodeId, node->primitivesOffset,
                node->primitiveCount, node->axis);
    }

    fclose(bvhFile);
}

static void outputCameraData(Camera* camera)
{
    printf("core.camera.origin = vec3(%ff, %ff, %ff);\n", camera->origin.x, camera->origin.y, camera->origin.z);
    printf("core.camera.forward = vec3(%ff, %ff, %ff);\n", camera->forward.x, camera->forward.y, camera->forward.z);
    printf("core.camera.right = vec3(%ff, %ff, %ff);\n", camera->right.x, camera->right.y, camera->right.z);
    printf("core.camera.up = vec3(%ff, %ff, %ff);\n", camera->up.x, camera->up.y, camera->up.z);
    printf("core.camera.theta = %ff;\n", camera->theta);
    printf("core.camera.phi = %ff;\n", camera->phi);
}

static void runMergeSortTest()
{
    const int bufferSize = 48;
    int unsorted[bufferSize];
    int sorted[bufferSize] = {};

    printf("Unsorted: ");

    for (int i = 0; i < bufferSize; i++)
    {
        unsorted[i] = (int) (randf() * 100.0f);
        printf("%u ", unsorted[i]);
    }

    printf("\n");

    for (int width = 1; width < bufferSize; width *= 2)
    {
        for (int left = 0; left < bufferSize; left += 2 * width)
        {
            int right = glm::min(left + width, bufferSize);
            int end = glm::min(left + 2 * width, bufferSize);

            for (int leftHead = left, rightHead = right, element = left; element < end; element++)
            {
                if (leftHead < right)
                {
                    if (rightHead >= end)
                    {
                        sorted[element] = unsorted[leftHead++];
                    }
                    else
                    {
                        if (unsorted[leftHead] <= unsorted[rightHead])
                        {
                            sorted[element] = unsorted[leftHead++];
                        }
                        else
                        {
                            sorted[element] = unsorted[rightHead++];
                        }
                    }
                }
                else
                {
                    sorted[element] = unsorted[rightHead++];
                }
            }
        }

        memcpy(unsorted, sorted, sizeof(int) * bufferSize);
    }

    printf("Sorted: ");

    for (int i = 1; i < bufferSize; i++)
    {
        printf("%u ", sorted[i]);
    }

    printf("\n\n");
}